Introduction
===============

What is the Fire Emblem Conversation Editor?
-----------------------------------------------

The Fire Emblem Conversation Editor is intended as a replacement for the old
method of editing Fire Emblem text for Fire Emblem: Awakening and Fire Emblem:
Fates by hand in their own frustrating syntax.

The Editor achieves this by the use of a simple, human-readable script format.
This format produces functionally equivalent output if run on default game files
in 95% of cases -- that is to say, in the parser's current state I have yet to
encounter any issues despite extensive testing, but I'm sure some that edge
cases still exist.

Many text control codes have functions that remain unknown to me and thus will
not be converted when going from game text to script. They will instead be left
as-is. As and when information about how they work is discovered, the Editor
will be updated with script commands to support them. If you have information
about an unrecognized control code, please [file an issue] describing it.


[file an issue]: https://gitlab.com/secretivecactus/fe-conversation-editor/issues/new

Why Use a Script?
--------------------

The script format's strength is in its simplicity. Rather than force the user to
copy and paste or use an IME to produce the required katakana for a given
character's name or the name of an expression portrait, a simple declarative
syntax can be used instead. While the syntax is in English by default, it is
possible to translate every aspect of the Editor without any knowledge of code
or need to rebuild the jar file, and thus includes the script commands. For more
information about translating the Editor, please see [this page].

That said, if you really want to write in the default Fire Emblem text style, be
my guest! The Editor will accomodate you, though it currently lacks 
highlighting for such an endeavor. That is, however, planned for inclusion in a
later release.


[this page]: https://secretivecactus.gitlab.io/fe-conversation-editor/translation

Launching the Editor
-----------------------

There are four scripts provided for launching the Editor: .sh files for Linux
and Mac and .bat files for Windows. Simply double-click on one of the files (or
run it from the terminal or command line) and the program will launch.

While you could run the jar file directly, this is not recommended. The scripts
contain some optimizations for Java's memory use, which can be omitted with no
ill effect save for greedier RAM hogging.

The scripts ending with "-sw" also contain a directive for JavaFX to use
software rendering rather than hardware accelerated rendering. This is required
if you wish to be able to quickly save long conversations as a single image
file. While the hardware-accelerated version will still be able to save long
conversations, it does so by writing them out in chunks and then sticking them
together in a much slower process. The wait times should not be unbearable, but
the impatient should use the software-rendered scripts.

If you are impatient and still wish to launch the jar directly, you can provide
the -Dprism.order=sw property to Java in order to make use of software
rendering. You are free to ignore this if you do not care about being able to
save conversations quickly.

If you're ready to learn how to use the program, [let's begin!]
------------------------------------------------------------------
[let's begin!]: https://secretivecactus.gitlab.io/fe-conversation-editor/gui-introduction

License
==========

The Fire Emblem Conversation Editor is [licensed][Editor License] under the GNU
General Public License v3.0.


[Editor License]: https://gitlab.com/secretivecactus/fe-conversation-editor/blob/master/LICENSE

Third-Party Licenses
=======================

A major portion of this project would not have been possible without [SciresM]'s
work on [FEITS], on which the simulation panel was based. It is
[licensed][FEITS License] under the GPLv3.

I am similarly thankful for SciresM's [FEAT], from which the ability to directly
edit .bin and .bin.lz files was derived. FEAT is [licensed][FEAT License] under
the GPLv2 with the "any later version" clause.

[Einstein95]'s [DSDecmp] is also present in this project, where it assists the
FEAT-based code in loading the aforementioned archives. DSDecmp has no license.

[Tomas Mikula]'s [RichTextFX] is used for syntax highlighting when editing
scripts. It is [licensed][RichTextFX License] under a BSD 2-clause license.

[iamlixiao]'s fork of [PNGJ] is used to enable the saving of large conversations
as a single image when running in hardware-accelerated mode. The code from the
fork has been included in the project repository, as it is not present in the
Maven Central Repository and in any case uses an outdated Gradle build script
targeting Java 6. It is [licensed][Apache License] under the Apache License
v2.0.

The Apache [Commons Collections 4] is used for the sake of caching via the
ReferenceMap class. This allows cached images to be garbage collected if the
JVM's memory is running low. It is [licensed][Apache License] under the Apache
License v2.0.

A single method from [JFXtras] has been used in order to allow conversion of hex
strings to colors. It is [licensed][JFXtras License] under a 3-clause BSD
license.


[SciresM]: https://github.com/sciresm
[FEITS]: https://github.com/SciresM/FEITS
[FEITS License]: https://github.com/SciresM/FEITS/blob/master/LICENSE.txt
[FEAT]: https://github.com/SciresM/FEAT
[FEAT License]: https://github.com/SciresM/FEAT/blob/master/LICENSE
[Einstein95]: https://github.com/einstein95
[DSDecmp]: https://github.com/einstein95/dsdecmp
[Tomas Mikula]: https://github.com/TomasMikula
[RichTextFX]: https://github.com/TomasMikula/RichTextFX
[RichTextFX License]: https://github.com/TomasMikula/RichTextFX/blob/master/LICENSE
[iamlixiao]: https://github.com/iamlixiao
[PNGJ]: https://github.com/iamlixiao/pngj
[Commons Collections 4]: https://commons.apache.org/proper/commons-collections/
[Apache License]: https://www.apache.org/licenses/LICENSE-2.0
[JFXtras]: https://github.com/JFXtras/jfxtras-labs
[JFXtras License]: https://github.com/JFXtras/jfxtras-labs/blob/8.0/license-template.txt
