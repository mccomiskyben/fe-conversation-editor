if [ -e "target/fe-conversation-editor.jar" ]; then
  java -XX:MaxHeapFreeRatio=10 -XX:MinHeapFreeRatio=5 -Xms64m -Xmx512m -jar target/fe-conversation-editor.jar "$@"
else
  if [ -e "bin/fe-conversation-editor.jar" ]; then
    java -XX:MaxHeapFreeRatio=10 -XX:MinHeapFreeRatio=5 -Xms64m -Xmx512m -jar bin/fe-conversation-editor.jar "$@"
  else
    if [ -e "fe-conversation-editor.jar" ]; then
      mkdir bin
      mv fe-conversation-editor.jar bin/fe-conversation-editor.jar
      java -XX:MaxHeapFreeRatio=10 -XX:MinHeapFreeRatio=5 -Xms64m -Xmx512m -jar bin/fe-conversation-editor.jar "$@"
    fi
  fi
fi
