/*
 * Copyright (C) 2016 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.wrapper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.scene.paint.Color;
import jfxtras.labs.util.ColorUtils;
import sahara.cactus.fireemblem.conversation.manager.ResourceManager;

/**
 * Represents the name of a character and possesses awareness of whether or
 * not it matches the user's current search term.
 */
public class CharacterReference {
  
  /**
   * Is bound to a search box's text property, causing the CharacterReference
   * to be marked as needing to be revalidated by its containing
   * ObservableList whenever the search term is changed. This allows quick
   * automatic filtering.
   */
  protected final StringProperty search;
  
  /**
   * Contains the String name of the character.
   */
  protected final String name;
  
  /**
   * Contains the character's Japanese name for use when selecting portraits.
   */
  protected final String japaneseName;
  
  /**
   * Contains the character's hair color.
   */
  protected Color hairColor;
  
  /**
   * Whether or not the character has a portrait.
   */
  protected boolean hasPortrait;
  
  /**
   * Contains the ID that begins each audio clip belonging to the character.
   */
  protected String soundName;
  
  /**
   * Whether or not the character has sound.
   */
  protected final BooleanProperty sound;
  
  /**
   * Contains a pattern for sorting sounds.
   */
  protected final static Pattern soundPattern
                                             = Pattern.compile("^(.+?)_(\\d+)");
  
  /**
   * Creates a CharacterReference with the specified name.
   * 
   * @param name the translated name of the character
   * @param japaneseName the Japanese name of the character
   */
  public CharacterReference(String name, String japaneseName) {
    this.name = name;
    this.japaneseName = japaneseName;
    search = new SimpleStringProperty("");
    soundName = "_NO_SOUND_";
    sound = new SimpleBooleanProperty(false);
    hairColor = Color.rgb(0x5B, 0x58, 0x55);
  }
  
  /**
   * Compares two CharacterReferences based on their String names.
   *
   * @param other the CharacterReference with which a comparison should be made
   * @return an integer representing the result of the comparison, in the
   *      vein of any compareTo method (-1 for less than, 0 for equal to, and
   *      1 for greater than)
   */
  public int compareTo(CharacterReference other) {
    return name.compareTo(other.toString());
  }
  
  /**
   * Performs cleanup to allow this character reference to be garbage collected.
   */
  public void erase() {
    search.unbind();
  }
  
  /**
   * Returns the list of this character's audio files.
   * 
   * @return the list of this character's audio files
   */
  public SortedList<String> getAudioClips() {
    FilteredList<String> clips = ResourceManager.AVAILABLE_SOUNDS.filtered(
      (String s) -> s.startsWith(soundName)
    );
    if (!sound.getValue()) {
      return clips.sorted((String s1, String s2) -> s1.compareTo(s2));
    }
    SortedList<String> result = clips.sorted((String s1, String s2) -> {
      Matcher match1 = soundPattern.matcher(s1);
      match1.find();
      Matcher match2 = soundPattern.matcher(s2);
      match2.find();
      int value = match1.group(1).compareTo(match2.group(1));
      if (value == 0) {
        value = (new Integer(match1.group(2)))
                                       .compareTo(new Integer(match2.group(2)));
      }
      return value;
    });
    return result;
  }
  
  /**
   * Returns the character's hair color.
   * 
   * @return the character's hair color
   */
  public Color getHairColor() {
    return hairColor;
  }
  
  /**
   * Returns the character's Japanese name.
   * 
   * @return name the character's Japanese name
   */
  public String getJapaneseName() {
    return japaneseName;
  }
  
  /**
   * Returns the character's translated name.
   * 
   * @return name the character's translated name
   */
  public String getName() {
    return name;
  }
  
  /**
   * Returns whether or not this character has sound data.
   * 
   * @return whether or not this character has sound data
   */
  public boolean hasSound() {
    return sound.getValue();
  }
  
  /**
   * Checks whether or not this CharacterReference matches the user's current
   * search term.
   *
   * @return whether or not this CharacterReference matches the user's current
   *      search term
   */
  public boolean matchesSearch() {
    return name.startsWith(search.getValue());
  }
  
  /**
   * Returns the CharacterReference's observable search, which is bound
   * to a text field serving as a search box.
   *
   * @return the observable search of the CharacterReference
   */
  public StringProperty searchProperty() {
    return search;
  }
  
  /**
   * Sets the character's hair color.
   * 
   * @param color the character's new hair color
   */
  public void setHairColor(Color color) {
    hairColor = color;
  }
  
  /**
   * Sets the character's hair color.
   * 
   * @param color the character's new hair color as a hex string
   */
  public void setHairColor(String color) {
    hairColor = ColorUtils.webColorToColor(color);
  }
  
  /**
   * Sets the name of the sound files related to this character.
   * 
   * @param soundName the name of the sound files related to this character
   */
  public void setSoundName(String soundName) {
    if (soundName == null || soundName.isEmpty()) {
      this.soundName = "_NO_SOUND_";
      sound.setValue(false);
    } else {
      this.soundName = soundName;
      sound.setValue(true);
    }
  }
  
  /**
   * Returns the character reference's sound property.
   * 
   * @return the character reference's sound property
   */
  public BooleanProperty soundProperty() {
    return sound;
  }
  
  /**
   * Returns the character's name as a String.
   *
   * @return the character's name as a String
   */
  @Override public String toString() {
    return name;
  }
}
