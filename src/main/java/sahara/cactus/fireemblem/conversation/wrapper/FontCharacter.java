/*
 * Copyright (C) 2016 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.wrapper;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import org.apache.commons.collections4.map.ReferenceMap;
import sahara.cactus.fireemblem.conversation.manager.ResourceManager;
import sahara.cactus.fireemblem.conversation.util.io.ByteUtils;

/**
 * Contains data about a drawable font character.
 * 
 * @author SciresM
 * @author Secretive Cactus
 */
public class FontCharacter {
  
  /**
   * The width of the character.
   */
  private byte width;
  
  /**
   * The height of the character.
   */
  private byte height;
  
  /**
   * The character's first padding size.
   */
  private byte padding1;
  
  /**
   * The character's second padding size.
   */
  private byte[] padding2;
  
  /**
   * The char value associated with the character.
   */
  private char character;
  
  /**
   * The crop height of the character.
   */
  private int cropHeight;
  
  /**
   * The crop width of the character.
   */
  private int cropWidth;
  
  /**
   * The image offset of the character.
   */
  private int img;
  
  /**
   * The integer value of the character.
   */
  private int value;
  
  /**
   * The x offset of the character.
   */
  private int xofs;
  
  /**
   * The y offset of the character.
   */
  private int yofs;
  
  /**
   * The pixel data of the character.
   */
  private Color[][] pixels;
  
  /**
   * The image of the character.
   */
  private Image glyph;
  
  /**
   * Alternately colored versions of the character.
   */
  private ReferenceMap<Color, WritableImage> glyphs;
  
  /**
   * Creates a new FontCharacter with the given data.
   * 
   * @param data the hex data from which the FontCharacter will be created
   */
  public FontCharacter(byte[] data) {
    this(data, 0);
  }
  
  /**
   * Creates a copy of an existing FontCharacter that has a different integer
   * value.
   * 
   * @param value the integer value of the new FontCharacter
   * @param character the character to copy
   */
  public FontCharacter(int value, FontCharacter character) {
    this.value = value;
    img = character.img;
    xofs = character.xofs;
    yofs = character.yofs;
    width = character.width;
    height = character.height;
    padding1 = character.padding1;
    cropHeight = character.cropHeight;
    cropWidth = character.cropWidth;
    padding2 = character.padding2;
    this.character = character.character;
    glyph = character.glyph;
    glyphs = character.glyphs;
    pixels = character.pixels;
  }
  
  /**
   * Creates a new FontCharacter with the given data and offset.
   * 
   * @param data the hex data from which the FontCharacter will be created
   * @param ofs the offset in the data at which the character begins
   */
  public FontCharacter(byte[] data, int ofs) {
    value = ByteUtils.getUInt16(data, ofs);
    img = ByteUtils.getUInt16(data, ofs + 2);
    xofs = ByteUtils.getUInt16(data, ofs + 4);
    yofs = ByteUtils.getUInt16(data, ofs + 6);
    width = data[ofs + 8];
    height = data[ofs + 9];
    padding1 = data[ofs + 0xA];
    cropHeight = data[ofs + 0xB];
    if (cropHeight > 0x7F) {
      cropHeight -= 256;
    }
    cropWidth = data[ofs + 0xC];
    if (cropWidth > 0x7F) {
      cropWidth -= 256;
    }
    padding2 = new byte[0x3];
    System.arraycopy(data, ofs + 0xD, padding2, 0, 3);
    character = (char) value;
    glyphs = new ReferenceMap<>(ReferenceMap.ReferenceStrength.SOFT,
                                ReferenceMap.ReferenceStrength.SOFT);
  }
  
  /**
   * Gets the char value associated with the character.
   * 
   * @return the char value associated with the character
   */
  public char getCharacter() {
    return character;
  }
  
  /**
   * Gets the character's crop height.
   * 
   * @return the character's crop height
   */
  public int getCropHeight() {
    return cropHeight;
  }
  
  /**
   * Gets the character's crop width.
   * 
   * @return the character's crop width
   */
  public int getCropWidth() {
    return cropWidth;
  }
  
  /**
   * Gets the character's image.
   * 
   * @return the character's image
   */
  public Image getGlyph() {
    return glyph;
  }
  
  
  /**
   * Gets the character's crop height.
   * 
   * @return the character's crop height
   */
  public byte getHeight() {
    return height;
  }
  
  /**
   * Gets the character's image offset.
   * 
   * @return the character's image offset
   */
  public int getImg() {
    return img;
  }
  
  /**
   * Gets the character's first padding size.
   * 
   * @return the character's first padding size.
   */
  public byte getPadding1() {
    return padding1;
  }
  
  /**
   * Gets the character's second padding size.
   * 
   * @return the character's second padding size
   */
  public byte[] getPadding2() {
    return padding2;
  }
  
  /**
   * Gets the character's integer value.
   * 
   * @return the character's integer value
   */
  public int getValue() {
    return value;
  }
  
  /**
   * Gets the character's width.
   * 
   * @return the character's width
   */
  public byte getWidth() {
    return width;
  }
  
  /**
   * Gets the character's x offset.
   * 
   * @return the character's x offset
   */
  public int getXofs() {
    return xofs;
  }
  
  /**
   * Gets the character's y offset.
   * 
   * @return the character's y offset
   */
  public int getYofs() {
    return yofs;
  }
  
  /**
   * Sets the character's crop height.
   * 
   * @param height the character's crop height
   */
  public void setCropHeight(int height) {
    cropHeight = height;
  }
  
  /**
   * Sets the character's image.
   * 
   * @param img the character's image
   */
  public void setGlyph(Image img) {
    glyph = img;
  }
  
  /**
   * Sets a drawable image of the character in the specified color.
   * 
   * @param newColor the color that the image should be
   * @return a drawable image of the character in the specified color
   */
  public Image getGlyph(Color newColor) {
    WritableImage coloredGlyph;
    if ((coloredGlyph = glyphs.get(newColor)) != null) {
      return coloredGlyph;
    }
    coloredGlyph = new WritableImage(glyph.getPixelReader(), xofs, yofs, width,
                                     height);
    if (pixels == null) {
      pixels = new Color[width][height];
      PixelReader pixelReader = coloredGlyph.getPixelReader();
      Color c;
      for (int x = 0; x < width; x++) {
        for (int y = 0; y < height; y++) {
          if ((c = pixelReader.getColor(x, y)).getOpacity() > 0) {
            pixels[x][y] = c;
          }
        }
      }
    }
    Color c;
    PixelWriter pixelWriter = coloredGlyph.getPixelWriter();
    for (int x = 0; x < width; x++) {
      for (int y = 0; y < height; y++) {
        if ((c = pixels[x][y]) != null) {
          pixelWriter.setColor(x, y, ResourceManager.getAdjustedColor(c,
                                                                     newColor));
        }
      }
    }
    glyphs.put(newColor, coloredGlyph);
    return coloredGlyph;
  }
}
