/*
 * Copyright (C) 2017 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.dialog;

import java.util.ArrayList;
import java.util.Random;
import java.util.function.UnaryOperator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.transformation.SortedList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import sahara.cactus.fireemblem.conversation.manager.CharacterManager;
import sahara.cactus.fireemblem.conversation.manager.Config;
import sahara.cactus.fireemblem.conversation.manager.Config.GameMode;
import sahara.cactus.fireemblem.conversation.manager.Config.GameMode.GameBranchMissingException;
import sahara.cactus.fireemblem.conversation.manager.ResourceManager;
import sahara.cactus.fireemblem.conversation.manager.XMLManager;
import sahara.cactus.fireemblem.conversation.myunit.Kamui;
import sahara.cactus.fireemblem.conversation.myunit.MyUnit;
import sahara.cactus.fireemblem.conversation.myunit.MyUnit.Gender;
import sahara.cactus.fireemblem.conversation.myunit.Robin;
import sahara.cactus.fireemblem.conversation.util.control.CanvasUtils;
import sahara.cactus.fireemblem.conversation.util.text.ConversationFormatter;

/**
 * A dialog that allows the user to customize My Units.
 * 
 * @author Secretive Cactus
 */
public class MyUnitCustomizationDialog extends Dialog<ButtonType> {
  
  /**
   * Creates the dialog.
   * 
   * @throws GameBranchMissingException when a branch has not been supplied
   * for a game mode
   */
  public MyUnitCustomizationDialog() throws GameBranchMissingException {
    super();
    setTitle(XMLManager.getText("My_Unit_Creation_Dialog_Title"));
    setDialogPane(new MyUnitCustomizationDialogPane());
    setOnCloseRequest((e) -> {
      ((MyUnitCustomizationDialogPane) getDialogPane()).unbindAll();
      XMLManager.saveMyUnits(CharacterManager.MY_UNITS);
    });
    initModality(Modality.APPLICATION_MODAL);
  }
  
  /**
   * Creates and displays an instance of the dialog.
   * 
   * @throws GameBranchMissingException when a branch has not been supplied
   * for a game mode
   */
  public static void createAndShow() throws GameBranchMissingException {
    new MyUnitCustomizationDialog().show();
  }
  
  /**
   * Contains the controls used to customize My Units.
   */
  private static class MyUnitCustomizationDialogPane extends DialogPane {
    
    /**
     * Provides logging for the class.
     */
    private static final Logger LOGGER =
                Logger.getLogger(MyUnitCustomizationDialogPane.class.getName());
    
    /**
     * Used to select a random sound effect to play for voice testing.
     */
    private static final Random RANDOM = new Random();
    
    /**
     * Contains the pane's content.
     */
    private SplitPane content;
    
    /**
     * Contains the My Unit-related controls.
     */
    private GridPane contentGrid;
    
    /**
     * Contains the list of My Units for user selection.
     */
    private ListView myUnitList;
    
    /**
     * Displays the selected My Unit.
     */
    private Canvas myUnitCanvas;
    
    /**
     * Shows the current language's text for Name.
     */
    private Label nameLabel;
    
    /**
     * Allows the editing of the selected My Unit's name. Filtered to ensure
     * that the length of an entered name is never longer than the maximum
     * player name length for the current game mode.
     */
    private TextField nameField;
    
    /**
     * The selected My Unit.
     */
    private MyUnit myUnit;
    
    /**
     * Shows the current language's text for Gender.
     */
    private Label genderLabel;
    
    /**
     * Ensures that only one gender button can be selected at a time.
     */
    private ToggleGroup genderGroup;
    
    /**
     * Shows the current language's text for Female.
     */
    private ToggleButton female;
    
    /**
     * Shows the current language's text for Male.
     */
    private ToggleButton male;
    
    /**
     * Shows the current language's text for Body.
     */
    private Label bodyLabel;
    
    /**
     * Cycles to the previous body option..
     */
    private Button lastBody;
    
    /**
     * Cycles to the next body option.
     */
    private Button nextBody;
    
    /**
     * The current body index.
     */
    private int body;
    
    /**
     * The maximum body index.
     */
    private int maxBody;
    
    /**
     * Shows the current language's text for Eyes.
     */
    private Label eyeLabel;
    
    /**
     * Cycles to the previous eye option.
     */
    private Button lastEyes;
    
    /**
     * Cycles to the next eye option.
     */
    private Button nextEyes;
    
    /**
     * The current eye index.
     */
    private int eyes;
    
    /**
     * The maximum eye index.
     */
    private int maxEyes;
    
    /**
     * Shows the current language's text for Hair Style.
     */
    private Label hairStyleLabel;
    
    /**
     * Cycles to the previous hair style option.
     */
    private Button lastHairStyle;
    
    /**
     * Cycles to the next hair style option.
     */
    private Button nextHairStyle;
    
    /**
     * The current hair style index.
     */
    private int hairStyle;
    
    /**
     * The maximum hair style index.
     */
    private int maxHairStyle;
    
    /**
     * Shows the current language's text for Hair Color.
     */
    private Label hairColorLabel;
    
    /**
     * Allows the selection of a hair color.
     */
    private ColorPicker hairColorPicker;
    
    /**
     * Shows the current language's text for Accessory 1.
     */
    private Label accessory1Label;
    
    /**
     * Cycles to the previous accessory 1 index.
     */
    private Button lastAccessory1;
    
    /**
     * Cycles to the next accessory 1 index.
     */
    private Button nextAccessory1;
    
    /**
     * The current accessory 1 index.
     */
    private int accessory1;
    
    /**
     * The maximum accessory 1 index.
     */
    private int maxAccessory1;
    
    /**
     * Shows the current language's text for Accessory 2.
     */
    private Label accessory2Label;
    
    /**
     * Cycles to the previous accessory 2 index.
     */
    private Button lastAccessory2;
    
    /**
     * Cycles to the next accessory 2 index.
     */
    private Button nextAccessory2;
    
    /**
     * The current accessory 2 index.
     */
    private int accessory2;
    
    /**
     * The maximum accessory 2 index.
     */
    private int maxAccessory2;
    
    /**
     * Shows the current language's text for Voice.
     */
    private Label voiceLabel;
    
    /**
     * Cycles to the previous voice index.
     */
    private Button lastVoice;
    
    /**
     * Cycles to the next voice index.
     */
    private Button nextVoice;
    
    /**
     * Plays a random voice clip from the current voice.
     */
    private Button testVoice;
    
    /**
     * The current voice index.
     */
    private int voice;
    
    /**
     * The maximum voice index.
     */
    private int maxVoice;
    
    /**
     * Opens the child hair color selection dialog.
     */
    private Button editChildren;
    
    /**
     * Contains the list of deleted My Units for restoring.
     */
    private final ArrayList<DeletedMyUnitPair> deletedMyUnits;
    
    /**
     * Holds information about a deleted My Unit and its original place in the
     * list of My Units.
     */
    private static class DeletedMyUnitPair {
      
      /**
       * The index that the My Unit occupied in the list.
       */
      private final int index;
      
      /**
       * The deleted My Unit.
       */
      private final MyUnit myUnit;
      
      /**
       * Creates a reference to a deleted My Unit with the given My Unit and
       * index.
       * 
       * @param myUnit the deleted My Unit
       * @param index the deleted My Unit's original position in the list of My
       * Units
       */
      private DeletedMyUnitPair(MyUnit myUnit, int index) {
        this.myUnit = myUnit;
        this.index = index;
      }
    }
    
    /**
     * Creates a pane for customizing My Units.
     *
     * @throws GameBranchMissingException when a branch has not been supplied
     * for a game mode
     */
    private MyUnitCustomizationDialogPane() throws GameBranchMissingException {
      setInitialValues();
      addControls();
      deletedMyUnits = new ArrayList<>();
      setContent(content);
      getButtonTypes().add(XMLManager.getButtonType(ButtonData.OK_DONE));
      setOnKeyPressed((e) -> {
        if (e.isControlDown()) {
          if (e.getCode() == KeyCode.N) {
            try {
              GameMode.branch(() -> { //Awakening
                CharacterManager.MY_UNITS.add(new Robin());
              }, () -> { // Fates
                CharacterManager.MY_UNITS.add(new Kamui());
              });
            } catch (GameBranchMissingException ex) {
              LOGGER.log(Level.SEVERE, "No branch exists for game mode " + 
                                       Config.getGameMode() + ".", ex);
            }
            e.consume();
          } else if (e.getCode() == KeyCode.Z) {
            if (deletedMyUnits.size() > 0) {
              DeletedMyUnitPair pair = deletedMyUnits.get(
                                                     deletedMyUnits.size() - 1);
              CharacterManager.MY_UNITS.add(pair.index, pair.myUnit);
              myUnitList.getSelectionModel().select(pair.myUnit);
              deletedMyUnits.remove(pair);
              e.consume();
            }
          }
        }
      });
    }
    
    /**
     * Adds the controls used by the pane.
     */
    private void addControls() {
      content = new SplitPane();
      myUnitList = new ListView(CharacterManager.MY_UNITS);
      myUnitList.getSelectionModel().selectedItemProperty().addListener(
        (o, oV, nV) -> {
          if (nV != null) {
            try {
              setMyUnit((MyUnit) nV);
            } catch (GameBranchMissingException ex) {
              LOGGER.log(Level.SEVERE, "No branch exists for game mode " + 
                                       Config.getGameMode() + ".", ex);
            }
          }
        });
      myUnitList.setOnKeyPressed((e) -> {
        if (e.getCode() == KeyCode.DELETE) {
          if (myUnit != null) {
            int i = myUnitList.getSelectionModel().getSelectedIndex();
            deletedMyUnits.add(new DeletedMyUnitPair(myUnit, i));
            CharacterManager.MY_UNITS.remove(myUnit);
            if (i > CharacterManager.MY_UNITS.size()) {
              i--;
            }
            if (CharacterManager.MY_UNITS.size() == 0) {
              try {
                GameMode.branch(() -> { // Awakening
                  CharacterManager.MY_UNITS.add(new Robin());
                }, () -> { // Fates
                  CharacterManager.MY_UNITS.add(new Kamui());
                });
              } catch (GameBranchMissingException ex) {
                LOGGER.log(Level.SEVERE, "No branch exists for game mode " + 
                                       Config.getGameMode() + ".", ex);
              }
              myUnitList.getSelectionModel().select(0);
            } else {
              myUnitList.getSelectionModel().select(i);
            }
          }
          e.consume();
        }
      });
      SplitPane.setResizableWithParent(myUnitList, false);
      myUnitList.setMinWidth(150);
      contentGrid = new GridPane();
      contentGrid.setVgap(8);
      contentGrid.setHgap(8);
      contentGrid.setPadding(new Insets(8, 8, 8, 8));
      contentGrid.setAlignment(Pos.TOP_LEFT);
      myUnitCanvas = new Canvas(256, 256);
      myUnitCanvas.setOnMouseClicked((MouseEvent e) -> {
        if (myUnit != null && e.getButton() == MouseButton.PRIMARY) {
          CanvasUtils.saveSnapshot(myUnitCanvas, myUnit.getName());
        }
      });
      GridPane.setValignment(myUnitCanvas, VPos.TOP);
      GridPane.setHalignment(myUnitCanvas, HPos.RIGHT);
      GridPane.setRowSpan(myUnitCanvas, Integer.SIZE);
      contentGrid.add(myUnitCanvas, 4, 0);
      
      int row = 0;
      nameLabel = new Label(XMLManager.getText(
                                         "My_Unit_Creation_Dialog_Name_Label"));
      nameLabel.setPadding(new Insets(4, 4, 4, 4));
      contentGrid.add(nameLabel, 0, row);
      nameField = new TextField();
      nameField.setTextFormatter(new TextFormatter(
                                     new UnaryOperator<TextFormatter.Change>() {
        @Override
        public TextFormatter.Change apply(TextFormatter.Change change) {
          if (!change.isContentChange()) {
            return change;
          }
          int maxWidth = ConversationFormatter.maxNameLength();
          String newText = change.getControlNewText();
          String oldText = change.getControlText();
          if (ResourceManager.getTextLength(newText) > maxWidth) {
            if (newText.length() > oldText.length() + 1) {
              if (change.isReplaced()) {
                oldText = oldText.substring(0, change.getRangeStart()) +
                          oldText.substring(change.getRangeEnd());
              }
              String text = change.getText();
              int times = 0;
              do {
                text = text.substring(0, text.length() - 1);
                times++;
              } while (ResourceManager.getTextLength(oldText + text) >
                       maxWidth);
              change.setText(text);
              change.setAnchor(change.getAnchor() - times);
              change.setCaretPosition(change.getCaretPosition() - times);
            } else {
              change.setAnchor(change.getAnchor() - 1);
              change.setCaretPosition(change.getCaretPosition() - 1);
              change.setText("");
            }
          }
          return change;
        }
      }));
      GridPane.setColumnSpan(nameField, 2);
      contentGrid.add(nameField, 1, row);
      row++;
      
      genderLabel = new Label(XMLManager.getText(
                                       "My_Unit_Creation_Dialog_Gender_Label"));
      genderLabel.setPadding(new Insets(4, 4, 4, 4));
      contentGrid.add(genderLabel, 0, row);
      genderGroup = new ToggleGroup();
      female = new ToggleButton(XMLManager.getText(
                                      "My_Unit_Creation_Dialog_Gender_Female"));
      female.setOnAction((e) -> {
        if (myUnit != null) {
          myUnit.setGender(Gender.female);
          redrawMyUnit();
        }
      });
      contentGrid.add(female, 1, row);
      male = new ToggleButton(XMLManager.getText(
                                        "My_Unit_Creation_Dialog_Gender_Male"));
      male.setOnAction((e) -> {
        if (myUnit != null) {
          myUnit.setGender(Gender.male);
          redrawMyUnit();
        }
      });
      contentGrid.add(male, 2, row);
      genderGroup.getToggles().addAll(female, male);
      row++;
      
      bodyLabel = new Label(XMLManager.getText(
                                         "My_Unit_Creation_Dialog_Body_Label"));
      bodyLabel.setPadding(new Insets(4, 4, 4, 4));
      contentGrid.add(bodyLabel, 0, row);
      lastBody = new Button("<-");
      lastBody.setOnAction((e) -> {
        if (myUnit != null) {
          body--;
          if (body < 0) {
            body = maxBody;
          }
          myUnit.setBody(body);
          redrawMyUnit();
        }
      });
      contentGrid.add(lastBody, 1, row);
      nextBody = new Button("->");
      nextBody.setOnAction((e) -> {
        if (myUnit != null) {
          body++;
          if (body > maxBody) {
            body = 0;
          }
          myUnit.setBody(body);
          redrawMyUnit();
        }
      });
      contentGrid.add(nextBody, 2, row);
      row++;
      
      eyeLabel = new Label(XMLManager.getText(
                                          "My_Unit_Creation_Dialog_Eye_Label"));
      eyeLabel.setPadding(new Insets(4, 4, 4, 4));
      contentGrid.add(eyeLabel, 0, row);
      lastEyes = new Button("<-");
      lastEyes.setOnAction((e) -> {
        if (myUnit != null) {
          eyes--;
          if (eyes < 0) {
            eyes = maxEyes;
          }
          myUnit.setEyes(eyes);
          redrawMyUnit();
        }
      });
      contentGrid.add(lastEyes, 1, row);
      nextEyes = new Button("->");
      nextEyes.setOnAction((e) -> {
        if (myUnit != null) {
          eyes++;
          if (eyes > maxEyes) {
            eyes = 0;
          }
          myUnit.setEyes(eyes);
          redrawMyUnit();
        }
      });
      contentGrid.add(nextEyes, 2, row);
      row++;
      
      hairStyleLabel = new Label(XMLManager.getText(
                                         "My_Unit_Creation_Dialog_Hair_Label"));
      hairStyleLabel.setPadding(new Insets(4, 4, 4, 4));
      contentGrid.add(hairStyleLabel, 0, row);
      lastHairStyle = new Button("<-");
      lastHairStyle.setOnAction((e) -> {
        if (myUnit != null) {
          hairStyle--;
          if (hairStyle < 0) {
            hairStyle = maxHairStyle;
          }
          myUnit.setHairStyle(hairStyle);
          redrawMyUnit();
        }
      });
      contentGrid.add(lastHairStyle, 1, row);
      nextHairStyle = new Button("->");
      nextHairStyle.setOnAction((e) -> {
        if (myUnit != null) {
          hairStyle++;
          if (hairStyle > maxHairStyle) {
            hairStyle = 0;
          }
          myUnit.setHairStyle(hairStyle);
          redrawMyUnit();
        }
      });
      contentGrid.add(nextHairStyle, 2, row);
      row++;
      
      hairColorLabel = new Label(XMLManager.getText(
                                   "My_Unit_Creation_Dialog_Hair_Color_Label"));
      hairColorLabel.setPadding(new Insets(4, 4, 4, 4));
      contentGrid.add(hairColorLabel, 0, row);
      hairColorPicker = new ColorPicker();
      hairColorPicker.valueProperty().addListener((o, oV, nV) -> {
        if (myUnit != null) {
          myUnit.setHairColor(nV);
          redrawMyUnit();
        }
      });
      GridPane.setColumnSpan(hairColorPicker, 3);
      contentGrid.add(hairColorPicker, 1, row);
      row++;
      
      if (Config.getGameMode().isFates()) {
        accessory1Label = new Label(XMLManager.getText(
                                  "My_Unit_Creation_Dialog_Accessory_1_Label"));
        accessory1Label.setPadding(new Insets(4, 4, 4, 4));
        contentGrid.add(accessory1Label, 0, row);
        lastAccessory1 = new Button("<-");
        lastAccessory1.setOnAction((e) -> {
          if (myUnit != null) {
            accessory1--;
            if (accessory1 < 0) {
              accessory1 = maxAccessory1;
            }
            ((Kamui) myUnit).setAccessory1(accessory1);
            redrawMyUnit();
          }
        });
        contentGrid.add(lastAccessory1, 1, row);
        nextAccessory1 = new Button("->");
        nextAccessory1.setOnAction((e) -> {
          if (myUnit != null) {
            accessory1++;
            if (accessory1 > maxAccessory1) {
              accessory1 = 0;
            }
            ((Kamui) myUnit).setAccessory1(accessory1);
            redrawMyUnit();
          }
        });
        contentGrid.add(nextAccessory1, 2, row);
        row++;
        
        accessory2Label = new Label(XMLManager.getText(
                                  "My_Unit_Creation_Dialog_Accessory_2_Label"));
        accessory2Label.setPadding(new Insets(4, 4, 4, 4));
        contentGrid.add(accessory2Label, 0, row);
        lastAccessory2 = new Button("<-");
        lastAccessory2.setOnAction((e) -> {
          if (myUnit != null) {
            accessory2--;
            if (accessory2 < 0) {
              accessory2 = maxAccessory2;
            }
            ((Kamui) myUnit).setAccessory2(accessory2);
            redrawMyUnit();
          }
        });
        contentGrid.add(lastAccessory2, 1, row);
        nextAccessory2 = new Button("->");
        nextAccessory2.setOnAction((e) -> {
          if (myUnit != null) {
            accessory2++;
            if (accessory2 > maxAccessory2) {
              accessory2 = 0;
            }
            ((Kamui) myUnit).setAccessory2(accessory2);
            redrawMyUnit();
          }
        });
        contentGrid.add(nextAccessory2, 2, row);
      }
      row++;
      
      voiceLabel = new Label(XMLManager.getText(
                                        "My_Unit_Creation_Dialog_Voice_Label"));
      voiceLabel.setPadding(new Insets(4, 4, 4, 4));
      contentGrid.add(voiceLabel, 0, row);
      lastVoice = new Button("<-");
      lastVoice.setOnAction((e) -> {
        if (myUnit != null) {
          voice--;
          if (voice < 1) {
            voice = maxVoice;
          }
          myUnit.setVoice("" + voice);
          redrawMyUnit();
        }
      });
      contentGrid.add(lastVoice, 1, row);
      nextVoice = new Button("->");
      nextVoice.setOnAction((e) -> {
        if (myUnit != null) {
          voice++;
          if (voice > maxVoice) {
            voice = 1;
          }
          myUnit.setVoice("" + voice);
          redrawMyUnit();
        }
      });
      contentGrid.add(nextVoice, 2, row);
      testVoice = new Button(XMLManager.getText(
                                   "My_Unit_Creation_Dialog_Test_Voice_Label"));
      testVoice.setOnAction((e) -> {
        if (myUnit != null) {
          String name = "";
          try {
            name = GameMode.branch(name, (s) -> {
              return (myUnit.isFemale() ? "Robin F" : "Robin M");
            }, (s) -> {
              return (myUnit.isFemale() ? "Corrin F" : "Corrin M");
            });
          } catch (GameBranchMissingException ex) {
            LOGGER.log(Level.SEVERE, "No branch exists for game mode " + 
                                     Config.getGameMode() + ".", ex);
          }
          SortedList<String> audioClips = CharacterManager.getByName(name)
                                          .getAudioClips();
          String sound = audioClips.get(RANDOM.nextInt(audioClips.size()));
          ResourceManager.playSound(sound.replace("#", myUnit.getVoice()));
        }
      });
      contentGrid.add(testVoice, 3, row);
      row++;
      
      editChildren = new Button(XMLManager.getText(
                                "My_Unit_Creation_Dialog_Edit_Children_Label"));
      editChildren.setOnAction((e) -> {
        if (myUnit != null) {
          ChildHairColorDialog.createAndShow();
        }
      });
      GridPane.setColumnSpan(editChildren, 3);
      contentGrid.add(editChildren, 0, row);
      
      content.getItems().addAll(myUnitList, contentGrid);
      content.setDividerPosition(0, 0);
      myUnitList.getSelectionModel().select(CharacterManager.getMyUnit());
    }
    
    /**
     * Sets the initial values for the pane's variables based on the current
     * game mode.
     * 
     * @throws GameBranchMissingException when a branch has not been supplied
     * for a game mode
     */
    private void setInitialValues() throws GameBranchMissingException {
      body = 0;
      eyes = 0;
      hairStyle = 0;
      accessory1 = 0;
      accessory2 = 0;
      voice = 1;
      maxVoice = 3;
      GameMode.branch(
        () -> { // Awakening
          maxBody = 2;
          maxEyes = 4;
          maxHairStyle = 4;
          maxAccessory1 = 0;
          maxAccessory2 = 0;
        },
        () -> { // Fates
          maxBody = 1;
          maxEyes = 6;
          maxHairStyle = 11;
          maxAccessory1 = 12;
          maxAccessory2 = 5;
        });
    }
    
    /**
     * Sets the values in the dialog to those of a newly selected My Unit.
     * 
     * @param myUnit the My Unit that has been selected
     * @throws GameBranchMissingException when a branch has not been supplied
     * for a game mode
     */
    private void setMyUnit(MyUnit myUnit) throws GameBranchMissingException {
      if (this.myUnit != null) {
        nameField.textProperty().unbindBidirectional(this.myUnit
                                                     .nameProperty());
      }
      CharacterManager.setMyUnit(myUnit);
      this.myUnit = myUnit;
      redrawMyUnit();
      nameField.textProperty().bindBidirectional(myUnit.nameProperty());
      if (myUnit.isFemale()) {
        female.setSelected(true);
      } else {
        male.setSelected(true);
      }
      body = myUnit.getBody();
      eyes = myUnit.getEyes();
      hairStyle = myUnit.getHairStyle();
      hairColorPicker.setValue(myUnit.getHairColor());
      voice = Integer.parseInt(myUnit.getVoice());
      if (Config.getGameMode().isFates()) {
        accessory1 = ((Kamui) myUnit).getAccessory1();
        accessory2 = ((Kamui) myUnit).getAccessory2();
      }
    }
    
    /**
     * Redraw the My Unit's portrait.
     */
    private void redrawMyUnit() {
      CanvasUtils.clearCanvas(myUnitCanvas);
      try {
        myUnit.drawCharacterStageImage(myUnitCanvas);
      } catch (GameBranchMissingException ex) {
        LOGGER.log(Level.SEVERE, "No branch exists for game mode " + 
                                 Config.getGameMode() + ".", ex);
      }
    }
    
    /**
     * Unbinds controls in the dialog.
     */
    private void unbindAll() {
      nameField.textProperty().unbindBidirectional(myUnit.nameProperty());
      myUnitList.setItems(null);
    }
  }
}
