/*
 * Copyright (C) 2017 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.dialog;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SplitPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import sahara.cactus.fireemblem.conversation.manager.CharacterManager;
import sahara.cactus.fireemblem.conversation.manager.Config;
import sahara.cactus.fireemblem.conversation.manager.Config.GameMode.GameBranchMissingException;
import sahara.cactus.fireemblem.conversation.manager.XMLManager;
import sahara.cactus.fireemblem.conversation.util.control.CanvasUtils;
import sahara.cactus.fireemblem.conversation.wrapper.CharacterBust;
import sahara.cactus.fireemblem.conversation.wrapper.CharacterReference;

/**
 * Allows the user to customize the hair colors of the child characters for the
 * current game mode.
 * 
 * @author Secretive Cactus
 */
public class ChildHairColorDialog extends Dialog<ButtonType> {
  
  /**
   * Creates an instance of the dialog tied to the current My Unit.
   */
  public ChildHairColorDialog() {
    super();
    setTitle(XMLManager.getText("Child_Hair_Color_Dialog_Title"));
    setDialogPane(new ChildHairColorDialogPane());
    setOnCloseRequest((e) -> {
      ((ChildHairColorDialogPane) getDialogPane()).unbindAll();
      XMLManager.saveMyUnits(CharacterManager.MY_UNITS);
    });
    initModality(Modality.APPLICATION_MODAL);
  }
  
  /**
   * Creates and displays an instance of the dialog.
   */
  public static void createAndShow() {
    new ChildHairColorDialog().show();
  }
  
  /**
   * Contains the controls related to customizing child hair colors.
   */
  private static class ChildHairColorDialogPane extends DialogPane {
    
    /**
     * Provides logging for the class.
     */
    private static final Logger LOGGER =
                     Logger.getLogger(ChildHairColorDialogPane.class.getName());
    
    /**
     * The selected child.
     */
    private CharacterBust child;
    
    /**
     * Displays the selected child.
     */
    private Canvas childCanvas;
    
    /**
     * Contains the list of children for user selection.
     */
    private ListView<CharacterReference> childList;
    
    /**
     * Contains the pane's content.
     */
    private SplitPane content;
    
    /**
     * Contains the child-related controls.
     */
    private GridPane contentGrid;
    
    /**
     * Shows the current language's text for Hair Color.
     */
    private Label hairColorLabel;
    
    /**
     * Allows the selection of a hair color.
     */
    private ColorPicker hairColorPicker;
    
    /**
     * Creates a pane for customizing child hair colors.
     */
    private ChildHairColorDialogPane() {
      child = new CharacterBust();
      addControls();
      setContent(content);
      getButtonTypes().add(XMLManager.getButtonType(ButtonData.OK_DONE));
    }
    
    /**
     * Adds the controls used by the pane.
     */
    private void addControls() {
      content = new SplitPane();
      childList = new ListView<>(CharacterManager.getChildren());
      childList.getSelectionModel().selectedItemProperty().addListener(
        (o, oV, nV) -> {
          if (child != null && nV != null) {
            CharacterBust tmp = child;
            child = null;
            tmp.setName(nV.getJapaneseName());
            hairColorPicker.setValue(tmp.getHairColor());
            child = tmp;
            redrawChild();
          }
        });
      SplitPane.setResizableWithParent(childList, false);
      childList.setMinWidth(150);
      contentGrid = new GridPane();
      contentGrid.setVgap(8);
      contentGrid.setHgap(8);
      contentGrid.setPadding(new Insets(8, 8, 8, 8));
      contentGrid.setAlignment(Pos.TOP_LEFT);
      childCanvas = new Canvas(256, 256);
      childCanvas.setOnMouseClicked((MouseEvent e) -> {
        CanvasUtils.saveSnapshot(childCanvas,
                           CharacterManager.getTranslatedName(child.getName()));
      });
      GridPane.setValignment(childCanvas, VPos.TOP);
      GridPane.setHalignment(childCanvas, HPos.RIGHT);
      GridPane.setRowSpan(childCanvas, Integer.SIZE);
      contentGrid.add(childCanvas, 3, 0);
      
      hairColorLabel = new Label(XMLManager.getText(
                                 "Child_Hair_Color_Dialog_Hair_Color_Label"));
      hairColorLabel.setPadding(new Insets(4, 4, 4, 4));
      contentGrid.add(hairColorLabel, 0, 0);
      hairColorPicker = new ColorPicker();
      hairColorPicker.valueProperty().addListener((o, oV, nV) -> {
        if (child != null) {
          CharacterManager.getMyUnit().getChildHairColors().put(child.getName(),
                                   nV.toString().substring(2, 8).toUpperCase());
          child.setHairColor(nV);
          redrawChild();
        }
      });
      GridPane.setColumnSpan(hairColorPicker, 2);
      contentGrid.add(hairColorPicker, 1, 0);
      
      content.getItems().addAll(childList, contentGrid);
      content.setDividerPosition(0, 0);
      childList.getSelectionModel().select(0);
    }
    
    /**
     * Redraws the child's portrait.
     */
    private void redrawChild() {
      CanvasUtils.clearCanvas(childCanvas);
      try {
        child.drawCharacterStageImage(childCanvas);
      } catch (GameBranchMissingException ex) {
        LOGGER.log(Level.SEVERE, "No branch exists for game mode " + 
                                 Config.getGameMode() + ".", ex);
      }
    }
    
    /**
     * Unbinds controls in the dialog.
     */
    private void unbindAll() {
      childList.setItems(null);
    }
  }
}
