/*
 * Copyright (C) 2016 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.dialog;

import java.util.Optional;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import sahara.cactus.fireemblem.conversation.file.FireEmblemTextFile;
import sahara.cactus.fireemblem.conversation.manager.Config;
import sahara.cactus.fireemblem.conversation.manager.XMLManager;
import sahara.cactus.fireemblem.conversation.ui.control.MessageTab;
import sahara.cactus.fireemblem.conversation.wrapper.FireEmblemMessage;

/**
 * A dialog used for creating a new message archive.
 * 
 * @author Secretive Cactus
 */
public class FileCreationDialog extends Dialog<ButtonType> {
  
  /**
   * The button that should be clicked to request that the new conversation be
   * created.
   */
  private Button okButton;
  
  /**
   * A field for entering the name of the message archive.
   */
  private TextField nameBox;
  
  /**
   * A field for entering the name of the first key in the message archive.
   */
  private TextField firstMessageNameBox;
  
  /**
   * The TabPane into which the dialog will direct its output.
   */
  private static TabPane tabs;
  
  /**
   * Creates a new instance of the dialog.
   */
  public FileCreationDialog() {
    super();
    setTitle(XMLManager.getText("New_File_Dialog_Title"));
    setDialogPane(new FileCreationDialogPane());
    okButton.setOnAction((e) -> createAndOpenArchive());
    initModality(Modality.APPLICATION_MODAL);
  }
  
  private class FileCreationDialogPane extends DialogPane {
    
    /**
     * Creates a new instance of the dialog pane.
     */
    private FileCreationDialogPane() {
      super();
      addControls();
    }
    
    /**
     * Adds the pane's controls.
     */
    private void addControls() {
      VBox column = new VBox(12);
      HBox row1 = new HBox();
      Text fileNameText = new Text(XMLManager.getText(
                                                  "New_File_Dialog_File_Name"));
      HBox.setMargin(fileNameText, new Insets(6, 6, 0, 0));
      nameBox = new TextField();
      HBox.setHgrow(nameBox, Priority.ALWAYS);
      row1.getChildren().addAll(fileNameText, nameBox);
      HBox row2 = new HBox();
      Text initialMessageText = new Text(XMLManager.getText(
                                           "New_File_Dialog_Initial_Key_Name"));
      HBox.setMargin(initialMessageText, new Insets(6, 6, 0, 24));
      firstMessageNameBox = new TextField();
      HBox.setHgrow(firstMessageNameBox, Priority.ALWAYS);
      row1.getChildren().addAll(initialMessageText, firstMessageNameBox);
      ButtonBar buttonBar = (ButtonBar) createButtonBar();
      okButton = (Button) createButton(
                                  XMLManager.getButtonType(ButtonData.OK_DONE));
      buttonBar.getButtons().add(okButton);
      Button cancelButton = (Button) createButton(XMLManager.getButtonType(
                                                      ButtonData.CANCEL_CLOSE));
      buttonBar.getButtons().add(cancelButton);
      column.getChildren().addAll(row1, row2, buttonBar);
      setContent(column);
    }
  }
  
  /**
   * Creates and displays an instance of the dialog with the given TabPane as
   * its output target.
   * 
   * @param target the TabPane in which the new archive should be displayed
   */
  public static void createAndShow(TabPane target) {
    tabs = target;
    Optional<ButtonType> result =
                          new FileCreationDialog().showAndWait();
    if (result.isPresent() &&
        result.get().getButtonData() == ButtonData.OK_DONE) {
      if (Config.getCurrentConversation() != null) {
        ObservableList<Tab> tabs = target.getTabs();
        for (Tab t : tabs) {
          ((MessageTab) t).removeBindings();
        }
        tabs.clear();
        Config.getCurrentConversation().getMessages().forEach((message) -> {
          tabs.add(new MessageTab(message.getName(), "",
                                  message.contentProperty()));
        });
      }
    }
  }
  
  /**
   * Displays a warning that the user's input was invalid.
   */
  private static void displayInvalidEntryWarning() {
    Platform.runLater(() -> {
      Alert alert = new Alert(AlertType.WARNING,
                            XMLManager.getText("New_File_Dialog_Invalid_Entry"),
                              XMLManager.getButtonType(ButtonData.YES),
                              XMLManager.getButtonType(ButtonData.NO));
      Optional<ButtonType> result = alert.showAndWait();
      if (result.isPresent() &&
          result.get().getButtonData() == ButtonData.YES) {
        createAndShow(tabs);
      }
    });
  }
  
  /**
   * Creates a new message archive based on the inputs in the dialog and opens
   * it in the editor.
   */
  public final void createAndOpenArchive() {
    if (!nameBox.getText().isEmpty() &&
        !firstMessageNameBox.getText().isEmpty()) {
      FireEmblemTextFile file = new FireEmblemTextFile(nameBox.getText());
      file.getMessages().add(new FireEmblemMessage(firstMessageNameBox
                                                   .getText()));
      Config.setCurrentConversation(file);
    } else {
      displayInvalidEntryWarning();
    }
  }
}
