/*
 * Copyright (C) 2016 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.dialog;

import java.util.Optional;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import sahara.cactus.fireemblem.conversation.file.SupportConversation;
import sahara.cactus.fireemblem.conversation.manager.CharacterManager;
import sahara.cactus.fireemblem.conversation.manager.Config;
import sahara.cactus.fireemblem.conversation.manager.XMLManager;
import sahara.cactus.fireemblem.conversation.ui.control.MessageTab;

/**
 * A dialog used for creating a new support conversation.
 * 
 * @author Secretive Cactus
 */
public class SupportConversationCreationDialog extends Dialog<ButtonType> {
  
  /**
   * The button that should be clicked to request that the new conversation be
   * created.
   */
  private Button okButton;
  
  /**
   * A box for selecting the maximum rank of the support conversation.
   */
  private ComboBox<String> maxLevelBox;
  
  /**
   * A box for selecting the kind of support conversation that should be
   * created.
   */
  private ComboBox<String> typeBox;
  
  /**
   * A field for entering the name of the first character in the support
   * conversation.
   */
  private TextField characterOne;
  
  /**
   * A field for entering the name of the second character in the support
   * conversation.
   */
  private TextField characterTwo;
  
  /**
   * The TabPane into which the dialog will direct its output.
   */
  private static TabPane tabs;
  
  /**
   * Creates a new instance of the dialog.
   */
  public SupportConversationCreationDialog() {
    super();
    setTitle(XMLManager.getText("New_Support_Dialog_Title"));
    setDialogPane(new SupportConversationCreationDialogPane());
    okButton.setOnAction((e) -> createAndOpenConversation());
    initModality(Modality.APPLICATION_MODAL);
  }
  
  private class SupportConversationCreationDialogPane extends DialogPane {
    
    /**
     * Creates a new instance of the dialog pane.
     */
    private SupportConversationCreationDialogPane() {
      super();
      addControls();
    }
    
    /**
     * Adds the pane's controls.
     */
    private void addControls() {
      VBox column = new VBox(12);
      HBox row1 = new HBox();
      Text characterOneText = new Text(XMLManager.getText(
                                           "New_Support_Dialog_Character_One"));
      HBox.setMargin(characterOneText, new Insets(6, 6, 0, 0));
      characterOne = new TextField();
      HBox.setHgrow(characterOne, Priority.ALWAYS);
      row1.getChildren().addAll(characterOneText, characterOne);
      Text characterTwoText = new Text(XMLManager.getText(
                                           "New_Support_Dialog_Character_Two"));
      HBox.setMargin(characterTwoText, new Insets(6, 6, 0, 24));
      characterTwo = new TextField();
      HBox.setHgrow(characterTwo, Priority.ALWAYS);
      row1.getChildren().addAll(characterTwoText, characterTwo);
      HBox row2 = new HBox();
      Text maxLevelText = new Text(XMLManager.getText(
                                               "New_Support_Dialog_Max_Level"));
      HBox.setMargin(maxLevelText, new Insets(6, 6, 0, 0));
      maxLevelBox = new ComboBox(FXCollections.observableArrayList('A', 'S'));
      maxLevelBox.getSelectionModel().select(0);
      row2.getChildren().addAll(maxLevelText, maxLevelBox);
      Text typeText = new Text(XMLManager.getText("New_Support_Dialog_Type"));
      HBox.setMargin(typeText, new Insets(6, 6, 0, 24));
      typeBox = new ComboBox(FXCollections.observableArrayList(
                         XMLManager.getText("Support_Conversation_Type_Normal"),
                         XMLManager.getText("Support_Conversation_Type_Parent"),
                     XMLManager.getText("Support_Conversation_Type_Siblings")));
      typeBox.getSelectionModel().select(0);
      row2.getChildren().addAll(typeText, typeBox);
      ButtonBar buttonBar = (ButtonBar) createButtonBar();
      okButton = (Button) createButton(
                                  XMLManager.getButtonType(ButtonData.OK_DONE));
      buttonBar.getButtons().add(okButton);
      Button cancelButton = (Button) createButton(XMLManager.getButtonType(
                                                      ButtonData.CANCEL_CLOSE));
      buttonBar.getButtons().add(cancelButton);
      column.getChildren().addAll(row1, row2, buttonBar);
      setContent(column);
    }
  }
  
  /**
   * Creates and displays an instance of the dialog with the given TabPane as
   * its output target.
   * 
   * @param target the TabPane in which the new conversation should be displayed
   */
  public static void createAndShow(TabPane target) {
    tabs = target;
    Optional<ButtonType> result =
                          new SupportConversationCreationDialog().showAndWait();
    if (result.isPresent() &&
        result.get().getButtonData() == ButtonData.OK_DONE) {
      if (Config.getCurrentConversation() != null) {
        ObservableList<Tab> tabs = target.getTabs();
        for (Tab t : tabs) {
          ((MessageTab) t).removeBindings();
        }
        tabs.clear();
        Config.getCurrentConversation().getMessages().forEach((message) -> {
          tabs.add(new MessageTab(message.getName(), "",
                                  message.contentProperty()));
        });
      }
    }
  }
  
  /**
   * Displays a warning that the user's input was invalid.
   */
  private static void displayInvalidEntryWarning() {
    Platform.runLater(() -> {
      Alert alert = new Alert(AlertType.WARNING,
                         XMLManager.getText("New_Support_Dialog_Invalid_Entry"),
                              XMLManager.getButtonType(ButtonData.YES),
                              XMLManager.getButtonType(ButtonData.NO));
      Optional<ButtonType> result = alert.showAndWait();
      if (result.isPresent() &&
          result.get().getButtonData() == ButtonData.YES) {
        createAndShow(tabs);
      }
    });
  }
  
  /**
   * Creates a new conversation based on the inputs in the dialog and opens it
   * in the editor.
   */
  public final void createAndOpenConversation() {
    if (!characterOne.getText().isEmpty() &&
        !characterTwo.getText().isEmpty()) {
      String name1;
      if (characterOne.getText().equals("Lucina") &&
          Config.getGameMode().isAwakening()) {
        name1 = "マルス";
      } else if (characterOne.getText().startsWith("Player")) {
        if (characterOne.getText().endsWith("F")) {
          name1 = "プレイヤー女";
        } else {
          name1 = "プレイヤー男";
        }
      } else if (characterOne.getText().startsWith("Morgan")) {
        if (characterOne.getText().endsWith("F")) {
          name1 = "マーク女";
        } else {
          name1 = "マーク男";
        }
      } else if (characterOne.getText().startsWith("Kanna")) {
        if (characterOne.getText().endsWith("F")) {
          name1 = "カンナ女";
        } else {
          name1 = "カンナ男";
        }
      } else if (characterOne.getText().startsWith("Azura")) {
        if (characterOne.getText().endsWith("(Birthright)")) {
          name1 = "アクア白";
        } else if (characterOne.getText().endsWith("(Conquest)")) {
          name1 = "アクア黒";
        } else {
          name1 = "アクア透";
        }
      } else {
        name1 = CharacterManager.getJapaneseName(characterOne.getText());
      }
      String name2;
      if (characterTwo.getText().equals("Lucina") &&
          Config.getGameMode().isAwakening()) {
        name2 = "マルス";
      } else if (characterTwo.getText().startsWith("Player")) {
        if (characterTwo.getText().endsWith("F")) {
          name2 = "プレイヤー女";
        } else {
          name2 = "プレイヤー男";
        }
      } else if (characterTwo.getText().startsWith("Morgan")) {
        if (characterTwo.getText().endsWith("F")) {
          name2 = "マーク女";
        } else {
          name2 = "マーク男";
        }
      } else if (characterTwo.getText().startsWith("Kanna")) {
        if (characterTwo.getText().endsWith("F")) {
          name2 = "カンナ女";
        } else {
          name2 = "カンナ男";
        }
      } else if (characterTwo.getText().startsWith("Azura")) {
        if (characterTwo.getText().endsWith("(Birthright)")) {
          name2 = "アクア白";
        } else if (characterTwo.getText().endsWith("(Conquest)")) {
          name2 = "アクア黒";
        } else {
          name2 = "アクア透";
        }
      } else {
        name2 = CharacterManager.getJapaneseName(characterTwo.getText());
      }
      if (name1 != null && name2 != null) {
        SupportConversation conversation = new SupportConversation(name1, name2,
                         3 + maxLevelBox.getSelectionModel().getSelectedIndex(),
                                 typeBox.getSelectionModel().getSelectedItem());
        conversation.initializeContents();
        Config.setCurrentConversation(conversation);
      } else {
        displayInvalidEntryWarning();
      }
    } else {
      displayInvalidEntryWarning();
    }
  }
}
