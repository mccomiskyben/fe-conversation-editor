/*
 * Copyright (C) 2016 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.manager;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Set;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import sahara.cactus.fireemblem.conversation.file.FireEmblemTextFile;
import sahara.cactus.fireemblem.conversation.wrapper.EmptyFunction;

/**
 * Contains configuration settings for the Editor.
 * 
 * @author Secretive Cactus
 */
public final class Config {
  
  /**
   * Provides logging for the class.
   */
  private static final Logger LOGGER = Logger.getLogger(Config.class.getName());
  
  /**
   * Used to represent the Editor's current game mode.
   * <p>
   * While future games can and should be added to this enum, they must
   * <b>always</b> be appended to the end. The ordinal values of the game names
   * in the enum are used for branching, and shuffling them around will lead to
   * unexpected behavior.
   */
  public static enum GameMode {
    
    /**
     * Represents Fire Emblem: Awakening.
     */
    Awakening,
    
    /**
     * Represents Fire Emblem: Fates.
     */
    Fates;
    
    /**
     * Thrown when a branch does not exist for the current game mode.
     * <p>
     * Generally speaking, this error should always be bubbled to the top rather
     * than captured. It is intended to provide useful information about
     * incomplete game modes and crash execution to prevent unknown outcomes
     * from occurring. If it must be caught, as in a lambda, ensure that it is
     * logged and consider closing the application.
     * 
     * @author Secretive Cactus
     */
    public static class GameBranchMissingException extends Exception {
      
      /**
       * Creates a new GameBranchMissingException with the given message.
       * 
       * @param mode the game mode whose branch is missing
       */
      public GameBranchMissingException(GameMode mode) {
        super("No branch exists for game mode " + mode + ".");
      }
    }
    
    /**
     * Returns the game mode with the given name, based on the current language.
     * 
     * @param mode the name of the game mode
     * @return the game mode as an enum representation
     */
    public static GameMode get(String mode) {
      if (mode.equalsIgnoreCase(XMLManager.getText("Game_Awakening"))) {
        return Awakening;
      } else if (mode.equalsIgnoreCase(XMLManager.getText("Game_Fates"))) {
        return Fates;
      }
      return null;
    }
    
    /**
     * Branches based on the ordinal value of the current game mode.
     * <p>
     * This method will run the branch at the index equal to the ordinal value
     * of the current game mode. Using this method should be preferred to a
     * series of if-else branches on
     * {@link Config#getGameMode() Config.getGameMode()}, but not to a check for
     * whether or not additional functionality should be added based on a
     * <i>single</i> game mode. Use it only when every game requires its own
     * processing to ensure that invalid results do not occur.
     * 
     * @param branches the branches for each game mode
     * @throws GameBranchMissingException when a branch has not been supplied
     * for a game mode
     */
    public static void branch(EmptyFunction ... branches) throws
                                                    GameBranchMissingException {
      int branch = getGameMode().ordinal();
      if (branch < branches.length && branches[branch] != null) {
        branches[branch].apply();
      } else {
        throw new GameBranchMissingException(getGameMode());
      }
    }
    
    /**
     * Branches based on the ordinal value of the current game mode.
     * <p>
     * This method takes a parameter that will be passed to the branch at the
     * ordinal value of the current game mode, which should return either the
     * same object or another one of the same class.
     * <p>
     * Using this method should be preferred to a series of if-else branches on
     * {@link Config#getGameMode() Config.getGameMode()}, but not to a check for
     * whether or not additional functionality should be added based on a
     * <i>single</i> game mode. Use it only when every game requires its own
     * processing to ensure that invalid results do not occur.
     * 
     * @param <T> the kind of value that the branch will take and return
     * @param value the initial value that will be passed to the branch for the
     * current game mode
     * @param branches the branches for each game mode
     * @return the transformation on the value performed by the branches
     * @throws GameBranchMissingException when a branch has not been supplied
     * for a game mode
     */
    public static <T> T branch(T value, UnaryOperator<T> ... branches) throws
                                                    GameBranchMissingException {
      int branch = getGameMode().ordinal();
      if (branch < branches.length && branches[branch] != null) {
        return branches[branch].apply(value);
      } else {
        throw new GameBranchMissingException(getGameMode());
      }
    }
    
    /**
     * Gets the default name of the game mode.
     * 
     * @return the default name of the game mode
     */
    public String getName() {
      return super.toString();
    }
    
    /**
     * Returns whether or not the game mode is Awakening.
     * 
     * @return whether or not the game mode is Awakening
     */
    public boolean isAwakening() {
      return this == Awakening;
    }
    
    /**
     * Returns whether or not the game mode is Fates.
     * 
     * @return whether or not the game mode is Fates
     */
    public boolean isFates() {
      return this == Fates;
    }
    
    /**
     * Returns the name of the game mode as a lower-case string.
     * 
     * @return the name of the game mode as a lower-case string
     */
    @Override
    public String toString() {
      return super.toString().toLowerCase();
    }
  };
  
  /**
   * The list of active My Unit indices for different game modes.
   */
  public static final HashMap<GameMode,Integer> MY_UNIT_INDICES =
                                                                new HashMap<>();
  
  /**
   * Whether or not the Settings.xml file has been loaded yet.
   */
  private static boolean LOADED;
  
  /**
   * The property containing the current game mode.
   */
  private static final ObjectProperty<GameMode> gameMode =
                                                   new SimpleObjectProperty<>();
  
  /**
   * The property related to automatic conversion of text to scripts.
   */
  private static final BooleanProperty autoconvertToScript =
                                               new SimpleBooleanProperty(false);
  
  /**
   * The property containing the current active language.
   */
  private static final StringProperty language =
                                              new SimpleStringProperty("en-us");
  
  /**
   * The property related to playing sound.
   */
  private static final BooleanProperty playSound
                                              = new SimpleBooleanProperty(true);
  
  /**
   * The property related to displaying backgrounds in the conversation
   * simulator.
   */
  private static final BooleanProperty showBackground =
                                                new SimpleBooleanProperty(true);
  
  /**
   * The property related to using the active My Unit name in scripts.
   */
  private static final BooleanProperty useMyUnitNameInScripts =
                                               new SimpleBooleanProperty(false);
  
  static {
    gameModeProperty().addListener((o, oV, nV) -> {
      if (oV != nV) {
        if (LOADED) {
          saveConfiguration();
        }
        try {
          ResourceManager.reload();
          XMLManager.loadLanguage(language.get());
          CharacterManager.initialize();
        } catch (GameMode.GameBranchMissingException ex) {
          LOGGER.log(Level.SEVERE, "No branch exists for game mode " + 
                                   Config.getGameMode() + ".", ex);
        }
      }
    });
    language.addListener((o, oV, nV) -> {
      if (LOADED && !oV.equals(nV)) {
        saveConfiguration();
        try {
          XMLManager.loadLanguage(nV);
        } catch (GameMode.GameBranchMissingException ex) {
          LOGGER.log(Level.SEVERE, "No branch exists for game mode " + 
                                   Config.getGameMode() + ".", ex);
        }
      }
    });
    ChangeListener saveListener = (o, oV, nV) -> {
      if (LOADED && oV != nV) {
        saveConfiguration();
      }
    };
    autoconvertToScript.addListener(saveListener);
    useMyUnitNameInScripts.addListener(saveListener);
    showBackground.addListener(saveListener);
    playSound.addListener(saveListener);
    CharacterManager.myUnitProperty().addListener((o, oV, nV) -> {
      if (oV != nV && oV != null && oV.getClass().equals(nV.getClass())) {
        MY_UNIT_INDICES.put(getGameMode(), CharacterManager.MY_UNITS
                                                           .indexOf(nV));
        if (LOADED) {
          saveConfiguration();
        }
      }
    });
  }
  
  /**
   * The property containing the current message archive.
   */
  private static final ObjectProperty<FireEmblemTextFile> currentConversation =
                                                   new SimpleObjectProperty<>();
  
  /**
   * Whether or not new audio playback is paused.
   */
  private static boolean PAUSED;
  
  /**
   * The current output file.
   */
  private static File CURRENT_FILE;
  
  /**
   * The root directory for the editor.
   */
  public static final String ROOT_DIRECTORY;
  static {
    URL src = (new Config()).getClass().getProtectionDomain().getCodeSource()
                            .getLocation();
    String root = src.getPath();
    if (root.endsWith(".jar")) {
      root = root.replaceFirst("/[^/]+/[^/]+\\.jar", "/");
    } else {
      root = root.replaceFirst("/[^/]+/$", "/");
    }
    root = root.replaceAll("%20", "\\ ");
    ROOT_DIRECTORY = root;
  }
  
  /**
   * Used only to get the root directory.
   */
  private Config() {}
  
  /**
   * Gets the property related to automatic conversion of text to scripts.
   * 
   * @return the property related to automatic conversion of text to scripts
   */
  public static BooleanProperty autoconvertToScriptProperty() {
    return autoconvertToScript;
  }
  
  /**
   * Closes the current conversation, allowing it to be garbage collected.
   */
  public static void closeCurrentConversation() {
    if (currentConversation.get() != null) {
      currentConversation.get().close();
    }
    currentConversation.set(null);
    CURRENT_FILE = null;
  }
  
  /**
   * Gets the property containing the current message archive.
   * 
   * @return the property containing the current message archive
   */
  public static ObjectProperty<FireEmblemTextFile>
                                                 currentConversationProperty() {
    return currentConversation;
  }
  
  /**
   * Gets the property containing the current game mode.
   * 
   * @return the property containing the current game mode
   */
  public static ObjectProperty<GameMode> gameModeProperty() {
    return gameMode;
  }
  
  /**
   * Gets whether or not to automatically convert opened messages to scripts.
   * 
   * @return whether or not to automatically convert opened messages to scripts
   */
  public static boolean getAutoconvertToScript() {
    return autoconvertToScript.get();
  }
  
  /**
   * Gets the current message archive.
   * 
   * @return the current message archive
   */
  public static FireEmblemTextFile getCurrentConversation() {
    return currentConversation.get();
  }
  
  /**
   * Gets the current output file.
   * 
   * @return the current output file
   */
  public static File getCurrentFile() {
    return CURRENT_FILE;
  }
  
  /**
   * Gets the current game mode.
   * 
   * @return the current game mode
   */
  public static GameMode getGameMode() {
    return gameMode.get();
  }
  
  /**
   * Gets the current language.
   * 
   * @return the current language
   */
  public static String getLanguage() {
    return language.get();
  }
  
  /**
   * Gets whether or not audio playback is enabled.
   * 
   * @return whether or not audio playback is enabled
   */
  public static boolean getPlaySound() {
    return playSound.get();
  }
  
  /**
   * Gets whether or not to display backgrounds in the conversation simulator.
   * 
   * @return whether or not to display backgrounds in the conversation simulator
   */
  public static boolean getShowBackground() {
    return showBackground.get();
  }
  
  /**
   * Gets whether or not the active My Unit name should be used in script mode.
   * 
   * @return whether or not the active My Unit name should be used in script
   * mode
   */
  public static boolean getUseMyUnitNameInScripts() {
    return useMyUnitNameInScripts.get();
  }
  
  /**
   * Gets whether or not playback of new sounds is paused.
   * 
   * @return whether or not playback of new sounds is paused
   */
  public static boolean isSoundPaused() {
    return PAUSED;
  }
  
  /**
   * Gets the property containing the current active language.
   * 
   * @return the property containing the current active language
   */
  public static StringProperty languageProperty() {
    return language;
  }
  
  /**
   * Loads a configuration from the Settings.xml file.
   */
  public static void loadConfiguration() {
    NodeList properties = XMLManager.getNodesFromFile(
                                     ResourceManager.getFileResource(
                                              "txt/xml/userdata/Settings.xml"));
    int len = properties.getLength();
    Node node;
    String name;
    GameMode game;
    for (int i = 0; i < len; i++) {
      node = properties.item(i);
      switch (node.getNodeName()) {
        case "game_mode":
          gameMode.set(GameMode.valueOf(node.getTextContent()));
          break;
        case "language":
          language.set(node.getTextContent());
        case "play_sound":
          playSound.set(Boolean.parseBoolean(node.getTextContent()));
          break;
        case "autoconvert_to_script":
          autoconvertToScript.set(Boolean.parseBoolean(node.getTextContent()));
          break;
        case "use_my_unit_name_in_scripts":
          useMyUnitNameInScripts.set(Boolean.parseBoolean(node
                                                          .getTextContent()));
          break;
        case "show_background":
          showBackground.set(Boolean.parseBoolean(node.getTextContent()));
          break;
        case "active_my_unit":
          int mu = Integer.parseInt(node.getTextContent());
          name = node.getAttributes().getNamedItem("game").getNodeValue();
          game = GameMode.valueOf(name);
          if (mu < CharacterManager.MY_UNITS.size()) {
            if (getGameMode().equals(game)) {
              CharacterManager.setMyUnit(CharacterManager.MY_UNITS.get(mu));
            }
            MY_UNIT_INDICES.put(game, mu);
          } else {
            MY_UNIT_INDICES.put(game, 0);
          }
          break;
      }
    }
    LOADED = true;
  }
  
  /**
   * Pauses the playback of new sounds.
   */
  public static void pauseSound() {
    PAUSED = true;
  }
  
  /**
   * The property related to playing sound.
   * 
   * @return the property related to playing sound
   */
  public static BooleanProperty playSoundProperty() {
    return playSound;
  }
  
  /**
   * Resumes the playback of new sounds.
   */
  public static void resumeSound() {
    PAUSED = false;
  }
  
  /**
   * Saves a configuration to the Settings.xml file.
   */
  public static void saveConfiguration() {
    Function<Document,Node> properties = (doc) -> {
      Element root = doc.createElement("settings");
      Element property;
      
      property = doc.createElement("language");
      property.appendChild(doc.createTextNode(language.get()));
      root.appendChild(property);
      
      property = doc.createElement("game_mode");
      property.appendChild(doc.createTextNode(gameMode.get().getName()));
      root.appendChild(property);
      
      property = doc.createElement("autoconvert_to_script");
      property.appendChild(doc.createTextNode("" + autoconvertToScript.get()));
      root.appendChild(property);
      
      property = doc.createElement("play_sound");
      property.appendChild(doc.createTextNode("" + playSound.get()));
      root.appendChild(property);
      
      property = doc.createElement("use_my_unit_name_in_scripts");
      property.appendChild(doc.createTextNode("" +
                                              useMyUnitNameInScripts.get()));
      root.appendChild(property);
      
      property = doc.createElement("show_background");
      property.appendChild(doc.createTextNode("" + showBackground.get()));
      root.appendChild(property);
      
      Set<GameMode> gameModes = MY_UNIT_INDICES.keySet();
      Attr game;
      for (GameMode key : gameModes) {
        property = doc.createElement("active_my_unit");
        property.appendChild(doc.createTextNode("" + MY_UNIT_INDICES.get(key)));
        game = doc.createAttribute("game");
        game.setValue(key.getName());
        property.getAttributes().setNamedItem(game);
        root.appendChild(property);
      }
      return root;
    };
    XMLManager.saveToFile(ResourceManager.getFileResource(
                                  "txt/xml/userdata/Settings.xml"), properties);
  }
  
  /**
   * Sets the active message archive.
   * 
   * @param conversation the wrapper around the active message archive
   */
  public static void setCurrentConversation(FireEmblemTextFile conversation) {
    if (currentConversation.get() != null) {
      currentConversation.get().close();
    }
    currentConversation.set(conversation);
    CURRENT_FILE = null;
  }
  
  /**
   * Sets the current output file.
   * 
   * @param file the new output file
   */
  public static void setCurrentFile(File file) {
    CURRENT_FILE = file;
  }
  
  /**
   * Sets the current game mode.
   * 
   * @param mode the new game mode
   */
  public static void setGameMode(GameMode mode) {
    gameMode.set(mode);
  }
  
  /**
   * Sets the current language.
   * 
   * @param lang the new language
   */
  public static void setLanguage(String lang) {
    language.set(lang);
  }
  
  /**
   * Sets whether or not sound should be played.
   * 
   * @param enabled whether or not sound should be played
   */
  public static void setPlaySound(boolean enabled) {
    playSound.set(enabled);
  }
  
  /**
   * Sets whether or not backgrounds should be displayed in the conversation
   * simulator.
   * 
   * @param show whether or not backgrounds should be displayed in the
   * conversation simulator
   */
  public static void setShowBackground(boolean show) {
    showBackground.set(show);
  }
  
  /**
   * Sets whether or not to use the active My Unit's names in scripts.
   * 
   * @param use whether or not to use the active My Unit's names in scripts
   */
  public static void setUseMyUnitNameInScripts(boolean use) {
    useMyUnitNameInScripts.set(use);
  }
  
  /**
   * Gets the property related to displaying backgrounds in the conversation
   * simulator.
   * @return the property related to displaying backgrounds in the conversation
   * simulator.
   */
  public static BooleanProperty showBackgroundProperty() {
    return showBackground;
  }
  
  /**
   * Gets the property related to using the active My Unit name in scripts.
   * 
   * @return the property related to using the active My Unit name in scripts
   */
  public static BooleanProperty useMyUnitNameInScriptsProperty() {
    return useMyUnitNameInScripts;
  }
}
