/*
 * Copyright (C) 2016 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.manager;

import java.io.File;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import org.apache.commons.collections4.map.ReferenceMap;
import sahara.cactus.fireemblem.conversation.manager.Config.GameMode;
import sahara.cactus.fireemblem.conversation.manager.Config.GameMode.GameBranchMissingException;
import sahara.cactus.fireemblem.conversation.util.io.ByteUtils;
import sahara.cactus.fireemblem.conversation.wrapper.EmptyFunction;
import sahara.cactus.fireemblem.conversation.wrapper.FontCharacter;

/**
 * Manages the loading and transformation of resources for the Editor.
 * 
 * @author Secretive Cactus
 */
public final class ResourceManager {
  
  /**
   * Provides logging for the class.
   */
  private static final Logger LOGGER = Logger.getLogger(
                                               ResourceManager.class.getName());
  
  /**
   * The path to global resources.
   */
  private static final String GLOBAL_RESOURCE_PATH = Config.ROOT_DIRECTORY +
                                                     "resources/global/";
  
  /**
   * The last audio clip to have been played.
   */
  private static AudioClip LAST_SOUND_CLIP;
  
  /**
   * Maps the available sound names to their paths.
   */
  private static final HashMap<String,String> SOUND_REFERENCE =
                                                            new HashMap<>(4000);
  
  /**
   * An observable list of the available audio clip names.
   */
  public static final ObservableList<String> AVAILABLE_SOUNDS =
                                            FXCollections.observableArrayList();
  
  /**
   * Used when calculating faded colors.
   */
  private static final double BLACK_A = 1.0 - (113.0 / 255.0);
  
  /**
   * A mapping of colors to their faded versions.
   */
  private static final ReferenceMap<Color,Color> FADED_COLORS =
                         new ReferenceMap<>(ReferenceMap.ReferenceStrength.SOFT,
                                           ReferenceMap.ReferenceStrength.SOFT);
  
  /**
   * A mapping of colors to their adjusted versions based on other colors.
   */
  private static final ReferenceMap<Color,ReferenceMap<Color, Color>> COLORS =
                         new ReferenceMap<>(ReferenceMap.ReferenceStrength.SOFT,
                                           ReferenceMap.ReferenceStrength.SOFT);
  
  /**
   * An array of the validity status of font characters by integer value.
   */
  private static boolean[] VALID_FONT_CHARACTERS;
  
  /**
   * An array of the available font characters.
   */
  private static FontCharacter[] FONT_CHARACTERS;
  
  /**
   * An array of font images.
   */
  private static Image[] FONT_IMAGES;
  
  /**
   * The name of the last sound file to have been played.
   */
  private static String LAST_SOUND;
  
  /**
   * The last BGM to have been played.
   */
  private static Media BGM;
  
  /**
   * The name of the last BGM to have been played.
   */
  private static String LAST_BGM;
  
  /**
   * The player for the current BGM.
   */
  private static MediaPlayer BGM_PLAYER;
  
  /**
   * The path to the current game's resource directory.
   */
  private static String RESOURCE_PATH;
  
  /**
   * The path to the current game's image resources.
   */
  private static String IMAGE_PATH;
  
  /**
   * A cached map of loaded images.
   */
  private static ReferenceMap<String,Image> LOADED_IMAGES;
  
  /**
   * A cached map of faded images.
   */
  private static final ReferenceMap<Image, WritableImage> FADED_IMAGES =
                         new ReferenceMap<>(ReferenceMap.ReferenceStrength.SOFT,
                                           ReferenceMap.ReferenceStrength.SOFT);
  
  /**
   * A cached map of recolored images.
   */
  private static final HashMap<Color, ReferenceMap<Image, WritableImage>>  
                                               COLORED_IMAGES = new HashMap<>();
  
  static {
    Config.playSoundProperty().addListener((o, oV, nV) -> {
      if (BGM != null) {
        if (nV) {
          playBGM(LAST_BGM, 0);
        } else {
          stopBGM();
        }
      }
    });
  }
  
  /**
   * returns whether or not the given BGM name exists in the current game mode.
   * 
   * @param bgm the BGM whose presence should be determined
   * @return whether or not the given BGM exists
   */
  public static boolean bgmExists(String bgm) {
    if (!Config.getPlaySound()) {
      return true;
    }
    String bgmName = bgm.replaceFirst("_\\d+$", "");
    return SOUND_REFERENCE.get(bgmName) != null;
  }
  
  /**
   * Gets the blend overlay for one value over another.
   * 
   * @param src the new value
   * @param dst the old value
   * @return the blended value
   */
  private static double blendOverlay(double src, double dst) {
    if (dst < 0.5) {
      return Math.max(Math.min((src * dst) * 2, 1), 0);
    } else {
      return Math.max(Math.min(1 - ((1 - src) * (1 - dst)) * 2, 1), 0);
    }
  }
  
  /**
   * Recolors the passed image based on the given color and returns the result.
   * 
   * @param baseImage the image to recolor
   * @param color the color with which to paint it
   * @return the recolored image
   */
  public static Image colorImage(Image baseImage, Color color) {
    ReferenceMap<Image, WritableImage> coloredHair;
    WritableImage bmp;
    if ((coloredHair = COLORED_IMAGES.get(color)) != null) {
      if ((bmp = coloredHair.get(baseImage)) != null) {
        return bmp;
      }
    } else {
      coloredHair = new ReferenceMap<>(ReferenceMap.ReferenceStrength.SOFT,
                                       ReferenceMap.ReferenceStrength.SOFT);
      COLORED_IMAGES.put(color, coloredHair);
    }
    bmp = new WritableImage(baseImage.getPixelReader(),
                           (int) baseImage.getWidth(),
                            (int) baseImage.getHeight());
    PixelReader pixelReader = bmp.getPixelReader();
    PixelWriter pixelWriter = bmp.getPixelWriter();
    Color newColor;
    double r;
    double g;
    double b;
    for (int y = 0; y < bmp.getHeight(); y++){
      for (int x = 0; x < bmp.getWidth(); x++){
        if ((newColor = pixelReader.getColor(x, y)).getOpacity() > 0) {
          r = blendOverlay(color.getRed(), newColor.getRed());
          g = blendOverlay(color.getGreen(), newColor.getGreen());
          b = blendOverlay(color.getBlue(), newColor.getBlue());
          pixelWriter.setColor(x, y, new Color(r, g, b, newColor.getOpacity()));
        }
      }
    }
    coloredHair.put(baseImage, bmp);
    return bmp;
  }
  
  /**
   * Fades the passed image and returns the result.
   * 
   * @param baseImage the image to fade
   * @return the faded image
   */
  public static Image fadeImage(Image baseImage) {
    WritableImage bmp;
    if ((bmp = FADED_IMAGES.get(baseImage)) != null) {
      return bmp;
    }
    bmp = new WritableImage(baseImage.getPixelReader(),
                            (int) baseImage.getWidth(),
                            (int) baseImage.getHeight());
    PixelReader pixelReader = bmp.getPixelReader();
    PixelWriter pixelWriter = bmp.getPixelWriter();
    Color color;
    for (int y = 0; y < bmp.getHeight(); y++){
      for (int x = 0; x < bmp.getWidth(); x++){
        if ((color = pixelReader.getColor(x, y)).getOpacity() > 0) {
          pixelWriter.setColor(x, y, ResourceManager.getFadedColor(color));
        }
      }
    }
    FADED_IMAGES.put(baseImage, bmp);
    return bmp;
  }
  
  /**
   * Adjusts a color based on another color.
   * 
   * @param oldColor the color to adjust
   * @param newColor the new color
   * @return the adjusted color
   */
  public static Color getAdjustedColor(Color oldColor, Color newColor) {
    ReferenceMap<Color, Color> resultMap = COLORS.get(oldColor);
    Color result;
    if (resultMap != null) {
      if ((result = resultMap.get(newColor)) != null) {
        return result;
      } else {
        result = new Color(newColor.getRed(), newColor.getGreen(),
                           newColor.getBlue(), oldColor.getOpacity());
        resultMap.put(newColor, result);
        return result;
      }
    } else {
      resultMap = new ReferenceMap<>(ReferenceMap.ReferenceStrength.SOFT,
                                     ReferenceMap.ReferenceStrength.SOFT);
      result = new Color(newColor.getRed(), newColor.getGreen(),
                         newColor.getBlue(), oldColor.getOpacity());
      resultMap.put(newColor, result);
      COLORS.put(oldColor, resultMap);
      return result;
    }
  }
  
  /**
   * Gets the faded version of the passed color.
   * 
   * @param color the color to fade
   * @return the faded color
   */
  private static Color getFadedColor(Color color) {
    Color c = FADED_COLORS.get(color);
    if (c != null) {
      return c;
    } else {
      double r = (color.getRed() * color.getOpacity() * BLACK_A);
      double g = (color.getGreen() * color.getOpacity() * BLACK_A);
      double b = (color.getBlue() * color.getOpacity() * BLACK_A);
      c = new Color(r, g, b, color.getOpacity());
      FADED_COLORS.put(color, c);
      return c;
    }
  }
  
  /**
   * Gets a file from the resource paths, giving precedence to the path for the
   * current game mode.
   * 
   * @param fileName the name of the file
   * @return a representation of the file
   */
  public static File getFileResource(String fileName) {
    File file = new File(RESOURCE_PATH + fileName);
    if (file.exists()) {
      return file;
    }
    return new File(GLOBAL_RESOURCE_PATH + fileName);
  }
  
  /**
   * Gets the font character with the specified integer value.
   * 
   * @param character the integer value of the font character
   * @return the font character with the specified integer value
   */
  public static FontCharacter getFontCharacter(int character) {
    return FONT_CHARACTERS[character];
  }
  
  /**
   * Gets the path to a named sound file.
   * 
   * @param name the name of the sound file
   * @return the path to the sound file, if it exists
   */
  public static String getSound(String name) {
    return SOUND_REFERENCE.get(name);
  }
  
  /**
   * Gets the width of the passed string of text based on the editor's font.
   * 
   * @param text the text whose width should be checked
   * @return the width of the text
   */
  public static int getTextLength(String text) {
    int sum = 0;
    int c;
    for (int i = 0; i < text.length(); i++) {
      c = text.codePointAt(i);
      sum += Math.max(FONT_CHARACTERS[c].getWidth(),
                      FONT_CHARACTERS[c].getCropWidth());
    }
    return sum;
  }
  
  /**
   * Iterates recursively over all of the sound files in a directory tree,
   * adding them to the sound references.
   * 
   * @param directory the root directory to iterate
   */
  private static void iterateSoundFiles(File directory) {
    File[] files = directory.listFiles();
    String name;
    for (File f : files) {
      if (f.isDirectory()) {
        iterateSoundFiles(f);
      } else {
        if (f.getName().contains(".mp3")) {
          name = f.getName().replace(".mp3", "");
        } else {
          name = f.getName().replace(".wav", "");
        }
        AVAILABLE_SOUNDS.add(name);
        SOUND_REFERENCE.put(name, f.toURI().toString());
      }
    }
  }
  
  /**
   * Loads the editor's font characters.
   * 
   * @throws GameBranchMissingException when a branch has not been supplied for
   * a game mode
   */
  private static void loadFontCharacters() throws GameBranchMissingException {
    EmptyFunction loadFatesAwakening = () -> {
      FONT_IMAGES = new Image[] {
        loadImage("Awakening_0.png"),
        loadImage("Awakening_1.png")
      };
      VALID_FONT_CHARACTERS = new boolean[0x10000];
      FONT_CHARACTERS = new FontCharacter[0x10000];
      byte[] chars = ByteUtils.getFileBytes(getFileResource("bin/chars.bin"));
      for (int i = 0; i < chars.length / 0x10; i++) {
        FontCharacter fc = new FontCharacter(chars, i * 0x10);
        VALID_FONT_CHARACTERS[fc.getValue()] = true;
        fc.setGlyph(FONT_IMAGES[fc.getImg()]);
        FONT_CHARACTERS[fc.getValue()] = fc;
      }
      VALID_FONT_CHARACTERS[8212] = true;
      FONT_CHARACTERS[8212] = new FontCharacter(8212, FONT_CHARACTERS[8213]);
      FONT_CHARACTERS[8212].setCropHeight(
                                     FONT_CHARACTERS[8212].getCropHeight() - 2);
    };
    GameMode.branch(loadFatesAwakening, loadFatesAwakening);
  }
  
  /**
   * Loads the image with the given name, starting from the img/ directory in
   * the current game mode's resource path.
   * 
   * @param imageName the name of the image to load
   * @return the loaded image
   */
  public static Image loadImage(String imageName) {
    if (LOADED_IMAGES == null) {
      LOADED_IMAGES = new ReferenceMap<>(ReferenceMap.ReferenceStrength.SOFT,
                                         ReferenceMap.ReferenceStrength.SOFT);
    }
    Image result;
    if ((result = LOADED_IMAGES.get(imageName)) != null) {
      return result;
    } else {
      result = new Image(IMAGE_PATH + imageName);
      LOADED_IMAGES.put(imageName, result);
      return result;
    }
  }
  
  /**
   * Loads references to all of the audio in the current game mode.
   */
  public static void loadSoundReference() {
    LAST_BGM = null;
    if (BGM != null) {
      if (BGM_PLAYER != null) {
        BGM_PLAYER.stop();
        BGM_PLAYER = null;
      }
      BGM = null;
    }
    AVAILABLE_SOUNDS.clear();
    SOUND_REFERENCE.clear();
    File sound = getFileResource("sound");
    if (sound.exists()) {
      iterateSoundFiles(sound);
    }
  }
  
  /**
   * Plays the given BGM.
   * 
   * @param bgmName the name of the BGM to play
   * @param offset the offset in the BGM. Currently unused.
   */
  public static void playBGM(String bgmName, int offset) {
    String sourceBgmName = bgmName.replaceFirst("_\\d+$", "");
    if (!sourceBgmName.equals(LAST_BGM)) {
      String bgmPath = SOUND_REFERENCE.get(sourceBgmName);
      if (bgmPath != null) {
        BGM = new Media(bgmPath);
      }
      LAST_BGM = sourceBgmName;
    }
    if (Config.getPlaySound() && !Config.isSoundPaused()) {
      if (BGM != null) {
        stopBGM();
        BGM_PLAYER = new MediaPlayer(BGM);
        BGM_PLAYER.setCycleCount(MediaPlayer.INDEFINITE);
        BGM_PLAYER.play();
      }
    }
  }
  
  /**
   * Plays the passed sound effect.
   * 
   * @param sound the name of the sound effect to play
   */
  public static void playSound(String sound) {
    if (Config.getPlaySound() && !Config.isSoundPaused()) {
      if (LAST_SOUND_CLIP != null && sound.equals(LAST_SOUND)) {
        LAST_SOUND_CLIP.stop();
        LAST_SOUND_CLIP.play();
        return;
      }
      LAST_SOUND = sound;
      String soundPath = SOUND_REFERENCE.get(sound);
      if (soundPath != null) {
        if (LAST_SOUND_CLIP != null) {
          LAST_SOUND_CLIP.stop();
        }
        LAST_SOUND_CLIP = new AudioClip(soundPath);
        LAST_SOUND_CLIP.play();
      } else {
        LOGGER.log(Level.INFO, "Unable to find audio file \"{0}\"", sound);
      }
    }
  }
  
  /**
   * Reloads the Resource Manager, as when the game mode changes.
   * 
   * @throws GameBranchMissingException when a branch has not been supplied for
   * a game mode
   */
  public static void reload() throws GameBranchMissingException {
    if (LOADED_IMAGES != null) {
      LOADED_IMAGES.clear();
      FADED_IMAGES.clear();
      COLORED_IMAGES.clear();
    }
    setPaths();
    loadFontCharacters();
    loadSoundReference();
  }
  
  /**
   * Returns whether or not the specified resource exists.
   * 
   * @param fileName the resource to check for
   * @return whether or not the resource exists
   */
  public static boolean resourceExists(String fileName) {
    return getFileResource(fileName).exists();
  }
  
  /**
   * Sets the resource paths for the current game mode.
   */
  public static void setPaths() {
    RESOURCE_PATH = Config.ROOT_DIRECTORY + "resources/" + Config.getGameMode()
                                                         + "/";
    IMAGE_PATH = "file:" + RESOURCE_PATH + "img/";
  }
  
  /**
   * Stops BGM playback.
   */
  public static void stopBGM() {
    if (BGM_PLAYER != null) {
      BGM_PLAYER.stop();
    }
  }
}
