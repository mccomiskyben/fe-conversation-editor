/*
 * Copyright (C) 2016 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.ui.control;

import ar.com.hjg.pngj.ImageInfo;
import ar.com.hjg.pngj.ImageLineInt;
import ar.com.hjg.pngj.PngReader;
import ar.com.hjg.pngj.PngWriter;
import ar.com.hjg.pngj.chunks.ChunkCopyBehaviour;
import ar.com.hjg.pngj.chunks.ChunkLoadBehaviour;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.UnaryOperator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Cursor;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import sahara.cactus.fireemblem.conversation.manager.CharacterManager;
import sahara.cactus.fireemblem.conversation.manager.Config;
import sahara.cactus.fireemblem.conversation.manager.Config.GameMode;
import sahara.cactus.fireemblem.conversation.manager.Config.GameMode.GameBranchMissingException;
import sahara.cactus.fireemblem.conversation.manager.ResourceManager;
import sahara.cactus.fireemblem.conversation.manager.XMLManager;
import sahara.cactus.fireemblem.conversation.util.control.CanvasUtils;
import sahara.cactus.fireemblem.conversation.util.text.ConversationFormatter;
import sahara.cactus.fireemblem.conversation.util.text.ScriptHelper;
import sahara.cactus.fireemblem.conversation.wrapper.CharacterBust;
import sahara.cactus.fireemblem.conversation.wrapper.FontCharacter;

/**
 * Displays a conversation much as it would appear in the current game mode.
 * 
 * @author Secretive Cactus
 */
public class ConversationPane extends Pane {
  
  /**
   * Provides logging for the class.
   */
  private static final Logger LOGGER = Logger.getLogger(
                                              ConversationPane.class.getName());
  
  /**
   * A dialog used to select output locations for images.
   */
  private FileChooser fileSelectionDialog;

  /**
   * Whether or not the current conversation can display $Nu values. Relevant
   * only for Fates.
   */
  private boolean hasPerms;
  
  /**
   * Whether or not the current conversation has already declared a type.
   */
  private boolean setType;
  
  /**
   * The current message index in the displayed conversation.
   */
  private int curIndex;
  
  /**
   * A list of the messages in the conversation.
   */
  private ArrayList<String> messages;
  
  /**
   * Represents the first character in a conversation.
   */
  private CharacterBust charA;
  
  /**
   * Represents the second character in a conversation.
   */
  private CharacterBust charB;
  
  /**
   * References the active speaker.
   */
  private CharacterBust activeChar;
  
  /**
   * Represents the display type of the current conversation.
   */
  private ConversationTypes conversationType;
  
  /**
   * The background image for the conversation.
   */
  private Image backgroundImage;
  
  /**
   * Whether or not a conversation is currently being displayed.
   */
  private boolean displaying;
  
  /**
   * The top message in a type 0 conversation.
   */
  private String topMessage;
  
  /**
   * The bottom message in a type 0 conversation.
   */
  private String bottomMessage;
  
  /**
   * A list of text control codes that take no parameters.
   */
  private static final String[] noParams = {
                                  "$Wa", "$Wc", "$a", "$Nu", "$N0", "$N1",
                                  "$t0", "$t1", "$k", "$p", "$Wv"
                                };
  
  /**
   * A list of text control codes that take one parameter.
   */
  private static final String[] singleParams = {
                                  "$E", "$Sbs", "$Sve", "$Svj", "$Svp", "$Sre",
                                  "$Fw", "$Ws", "$VF", "$Ssp", "$Fo", "$VNMPID",
                                  "$Fi", "$b", "$Wd", "$w", "$l"
                                };
  
  /**
   * A list of text control codes that take two parameters.
   */
  private static final String[] doubleParams = {
                                  "$G", "$Wm", "$Sbv", "$Sbp", "$Sls", "$Slp"
                                };
  
  /**
   * The normal text color.
   */
  private static final Color TEXT_COLOR = Color.rgb(68, 8, 0);
  
  /**
   * The color of names in the nameboxes.
   */
  private static final Color NAME_COLOR = Color.rgb(253, 234, 177);
  
  /**
   * A set of parameters used to take snapshots with transparent backgrounds.
   */
  private static final SnapshotParameters SNAPSHOT_PARAMETERS;
  static {
    SNAPSHOT_PARAMETERS = new SnapshotParameters();
    SNAPSHOT_PARAMETERS.setFill(Color.TRANSPARENT);
  }
  
  /**
   * The layer on which the background will be drawn.
   */
  private Canvas background;
  
  /**
   * The layer on which characters are drawn.
   */
  private Canvas characterLayer;
  
  /**
   * A canvas used for drawing characters in the type 0 display mode.
   */
  private Canvas zeroCharacterLayer;
  
  /**
   * A canvas used for drawing text boxes.
   */
  private Canvas textBox;
  
  /**
   * A canvas used for drawing text strings.
   */
  private Canvas text;
  
  /**
   * A canvas used for drawing text strings in the type 0 display mode.
   */
  private Canvas zeroText;
  
  /**
   * A canvas used for drawing name boxes.
   */
  private Canvas nameBox;
  
  /**
   * The current text color.
   */
  private Color textColor;
  
  /**
   * Whether or not the color has been set for the current message.
   */
  private boolean setColor;
  
  /**
   * The scale of the ConversationPane.
   */
  private double scale;
  
  /**
   * A list of control codes to put off for the next message.
   */
  private ArrayList<Command> forNext;
  
  /**
   * A list of control codes that need to be put off for the next message.
   */
  private ArrayList<Command> useForNext;
  
  /**
   * A context menu used for saving images of a conversation.
   */
  private ContextMenu saveImageMenu;
  
  /**
   * A menu item used to save the current conversation frame.
   */
  private MenuItem saveSingleImageItem;
  
  /**
   * A menu item used to save an entire conversation.
   */
  private MenuItem saveAllImagesItem;
  
  /**
   * An enum representing display modes.
   */
  private enum ConversationTypes {
    /**
     * Represents the type 0 display mode, where speakers are displayed
     * vertically and more than one message is displayed at a time.
     */
    TYPE_0,
    
    /**
     * Represents the type 1 display mode, where speakers are displayed
     * horizontally and only a single message is displayed at a time.
     */
    TYPE_1
  }
  
  /**
   * Represents a text control code.
   */
  private class Command {
    
    /**
     * The control code itself.
     */
    private String cmd;
    
    /**
     * Any parameters passed to the control code.
     */
    private String[] params;
    
    /**
     * Creates an empty command.
     */
    public Command() {
      cmd = "";
      params = new String[0];
    }
  }
  
  /**
   * Represents a text control code an an accompanying message.
   */
  private class CommandPair {
    
    /**
     * The control code.
     */
    private final Command command;
    
    /**
     * The message.
     */
    private final String message;
    
    /**
     * Creates a new CommandPair for the specified message and control code.
     * 
     * @param message the message
     * @param command the control code
     */
    public CommandPair(String message, Command command) {
      this.message = message;
      this.command = command;
    }
  }
  
  /**
   * Creates a new ConversationPane.
   */
  public ConversationPane() {
    super();
    initializeValues();
    setupDragDrop();
  }
  
  /**
   * Clears all of the pane's canvases.
   */
  public void clearAll() {
    CanvasUtils.clearCanvas(background);
    CanvasUtils.clearCanvas(characterLayer);
    CanvasUtils.clearCanvas(textBox);
    CanvasUtils.clearCanvas(zeroCharacterLayer);
    CanvasUtils.clearCanvas(text);
    CanvasUtils.clearCanvas(zeroText);
    CanvasUtils.clearCanvas(nameBox);
  }
  
  /**
   * Combines multiple output images into a single larger image.
   * <p>
   * Adapted from leonbloy's work at http://stackoverflow.com/a/6176487
   * 
   * @param images the array of images that need to be combined
   * @param outFile the file in which the combined image should be saved
   */
  public static void combineImageChunks(File images[], File outFile) {
    int nimageNames = images.length;
    ImageInfo imi1, imi2;
    PngReader reader = new PngReader(images[0]);
    imi1 = reader.imgInfo;
    reader.readSkippingAllRows();
    reader.end();
    reader = new PngReader(images[images.length - 1]);
    int rows = (imi1.rows * (images.length - 1)) + reader.imgInfo.rows;
    imi2 = new ImageInfo(imi1.cols, rows, imi1.bitDepth, imi1.alpha,
                         imi1.greyscale, imi1.indexed);
    PngWriter pngw = new PngWriter(outFile, imi2, true);
    pngw.copyChunksFrom(reader.getChunksList(), ChunkCopyBehaviour.COPY_PALETTE
                        | ChunkCopyBehaviour.COPY_TRANSPARENCY);
    reader.readSkippingAllRows();
    reader.end();
    ImageLineInt line2 = new ImageLineInt(imi2);
    int row2 = 0;
    for (int ty = 0; ty < nimageNames; ty++) {
      Arrays.fill(line2.getScanline(), 0);
      reader = new PngReader(images[ty]);
      reader.setChunkLoadBehaviour(ChunkLoadBehaviour.LOAD_CHUNK_NEVER);
      imi1 = reader.imgInfo;
      for (int row1 = 0; row1 < imi1.rows; row1++, row2++) {
        ImageLineInt line1 = (ImageLineInt) reader.readRow(row1);
        System.arraycopy(line1.getScanline(), 0, line2.getScanline(), 0,
                         line1.getScanline().length);
        pngw.writeRow(line2, row2);
      }
      reader.end();
    }
    pngw.end();
  }
  
  /**
   * Displays the next message.
   * 
   * @param nextButton the button that will advance the display to the next
   * message
   * @param previousButton the button that will revert the display to the
   * previous message
   * @throws GameBranchMissingException when a branch has not been supplied for
   * a game mode
   */
  public void displayNext(Button nextButton, Button previousButton) throws
                                                    GameBranchMissingException {
    curIndex++;
    drawCurrentMessage();
    if (curIndex >= messages.size() - 1) {
      nextButton.setDisable(true);
    }
    if (curIndex > 0) {
      previousButton.setDisable(false);
    }
  }
  
  /**
   * Displays the previous message.
   * 
   * @param nextButton the button that will advance the display to the next
   * message
   * @param previousButton the button that will revert the display to the
   * previous message
   * @throws GameBranchMissingException when a branch has not been supplied for
   * a game mode
   */
  public void displayPrevious(Button nextButton, Button previousButton) throws
                                                    GameBranchMissingException {
    curIndex--;
    displayUpTo(curIndex);
    if (curIndex < messages.size() - 1) {
      nextButton.setDisable(false);
    }
    if (curIndex == 0) {
      previousButton.setDisable(true);
    }
  }
  
  /**
   * Quickly runs every message up to the passed value in order to ensure that
   * all variables are in the right state.
   * 
   * @param last the message that should be displayed once the processing is
   * completed
   * @throws GameBranchMissingException when a branch has not been supplied for
   * a game mode
   */
  public void displayUpTo(int last) throws GameBranchMissingException {
    curIndex = last;
    topMessage = "";
    bottomMessage = "";
    charA.clear();
    charB.clear();
    Config.pauseSound();
    for (int i = 0; i < last; i++)
      parseMessage(messages.get(i));
    Config.resumeSound();
    drawCurrentMessage();
  }
  
  /**
   * Draws the current message with the conversation's display type.
   * 
   * @throws GameBranchMissingException when a branch has not been supplied for
   * a game mode
   */
  public void drawCurrentMessage() throws GameBranchMissingException {
    if (conversationType == ConversationTypes.TYPE_0) {
      renderTypeZero(messages.get(curIndex));
    } else {
      renderTypeOne(messages.get(curIndex));
    }
  }
  
  /**
   * Draws the passed string on the given canvas at the specified x and y with
   * the specified text color.
   * 
   * @param canvas the canvas to draw on
   * @param msg the string to draw
   * @param sX the x at which the string should be drawn
   * @param sY the y at which the string should be drawn
   * @param tC the color in which the string should be drawn
   */
  private void drawString(Canvas canvas, String msg, int sX, int sY,
                            Color tC) {
    int cX = sX;
    int cY = sY;
    GraphicsContext g = canvas.getGraphicsContext2D();
    int c;
    for (int i = 0; i < msg.length(); i++) {
      c = msg.codePointAt(i);
      if (c == 10) {
        cY += 20;
        cX = sX;
      } else {
        FontCharacter cur = ResourceManager.getFontCharacter(c);
        g.drawImage(cur.getGlyph(tC), cX, cY - cur.getCropHeight());
        cX += cur.getCropWidth();
      }
    }
  }
  
  /**
   * Initializes the conversation pane's variables and creates its children.
   */
  private void initializeValues() {
    scale = 1;
    conversationType = ConversationTypes.TYPE_1;
    charA = new CharacterBust(true);
    charB = new CharacterBust();
    activeChar = new CharacterBust();
    messages = new ArrayList<>();
    background = new Canvas(400, 240);
    characterLayer = new Canvas(400, 240);
    characterLayer.getGraphicsContext2D().save();
    zeroCharacterLayer = new Canvas(400, 240);
    zeroCharacterLayer.getGraphicsContext2D().save();
    textBox = new Canvas(400, 240);
    text = new Canvas(351, 240);
    zeroText = new Canvas(358, 240);
    nameBox = new Canvas(400, 240);
    forNext = new ArrayList<>();
    useForNext = new ArrayList<>();
    getChildren().add(background);
    getChildren().add(characterLayer);
    getChildren().add(textBox);
    getChildren().add(zeroCharacterLayer);
    getChildren().add(text);
    getChildren().add(zeroText);
    getChildren().add(nameBox);
    saveImageMenu = new ContextMenu();
    saveSingleImageItem = new MenuItem();
    saveSingleImageItem.textProperty().bind(XMLManager.getTextProperty(
                                                          "Save_Single_Image"));
    saveSingleImageItem.setOnAction((e) -> saveSingleImage());
    saveAllImagesItem = new MenuItem();
    saveAllImagesItem.textProperty().bind(XMLManager.getTextProperty(
                                                            "Save_All_Images"));
    saveAllImagesItem.setOnAction((e) -> {
      try {
        saveAllImages();
      } catch (GameBranchMissingException ex) {
        LOGGER.log(Level.SEVERE, "No branch exists for game mode " + 
                                 Config.getGameMode() + ".", ex);
      }
    });
    saveImageMenu.getItems().addAll(saveSingleImageItem, saveAllImagesItem);
    setOnMouseClicked((MouseEvent e) -> {
      if (Config.getCurrentConversation() != null && displaying) {
        if (e.getButton() == MouseButton.SECONDARY) {
          saveImageMenu.show(this, e.getScreenX(), e.getScreenY());
        } else {
          saveImageMenu.hide();
        }
      }
    });
    Config.showBackgroundProperty().addListener((o, ov, nv) -> {
      if (nv && backgroundImage != null) {
        background.getGraphicsContext2D().drawImage(backgroundImage, 0, 0);
      } else {
        CanvasUtils.clearCanvas(background);
      }
    });
  }
  
  /**
   * Parses control codes in the given message, starting at the given offset.
   * 
   * @param msg the message to parse
   * @param ofs the offset at which to begin
   * @return a CommandPair representing the results
   */
  private CommandPair parseCommand(String msg, int ofs) {
    String cmdMsg = null;
    String trunc = msg.substring(ofs);
    Command cmd = new Command();
    for (String delim : noParams) {
      if (trunc.startsWith(delim)) {
        cmd.cmd = delim;
        cmdMsg = msg.substring(0, ofs) + msg.substring(ofs + delim.length());
      }
    }
    if (cmdMsg == null) {
      for (String delim : singleParams) {
        if (trunc.startsWith(delim)) {
          cmd.cmd = delim;
          int ind = msg.indexOf("|", ofs);
          int start = ofs + delim.length();
          int end = ind - (ofs + delim.length());
          cmd.params = new String[] { msg.substring(start, start + end) };
          cmdMsg = msg.substring(0, ofs) + msg.substring(ind + 1);
        }
      }
      if (cmdMsg == null) {
        for (String delim : doubleParams) {
          if (trunc.startsWith(delim)) {
            cmd.cmd = delim;
            int ind = msg.indexOf("|", ofs);
            int ind2 = msg.indexOf("|", ind + 1);
            if (delim.equals("$Wm")) {
              int start = ofs + delim.length();
              cmd.params = new String[] {
                msg.substring(start, ind), msg.substring(ind + 1, ind + 2)
              };
              cmdMsg = msg.substring(0, ofs) + msg.substring(ind + 2);
            } else {
              int start = ofs + delim.length();
              int end = start + ind - (ofs + delim.length());
              if (delim.equals("$G")) {
                ind2 = ind;
                ind = msg.indexOf(",", start);
                end = ind;
              }
              cmd.params = new String[] {
                msg.substring(start, end),
                msg.substring(ind + 1, ind2)
              };
              cmdMsg = msg.substring(0, ofs) + msg.substring(ind2 + 1);
            }
          }
        }
        if (cmdMsg == null) {
          if (trunc.startsWith("$c")) {
            cmd.cmd = "$c";
            cmd.params = trunc.substring(2, trunc.indexOf("|")).split(",");
            cmdMsg = msg.substring(0, ofs) + msg.substring(ofs + 1 +
                                                            trunc.indexOf("|"));
          }
        }
      }
    }
    return new CommandPair(cmdMsg, cmd);
  }
  
  /**
   * Parses a script and sets it as the displayed message.
   * 
   * @param script the script to parse
   * @param nextButton the button that will advance the display to the next
   * message
   * @param previousButton the button that will revert the display to the
   * previous message
   * @throws GameBranchMissingException when a branch has not been supplied for
   * a game mode
   */
  public void parseScript(String script, Button nextButton,
                          Button previousButton) throws
                                                    GameBranchMissingException {
    setText(ConversationFormatter.formatLines(
           ScriptHelper.convertFromScript(script)), nextButton, previousButton);
  }
  
  /**
   * Renders a message with the type one display format.
   * 
   * @param msg the message to render
   * @throws GameBranchMissingException when a branch has not been supplied for
   * a game mode
   */
  public void renderTypeOne(String msg) throws GameBranchMissingException {
    text.setVisible(true);
    characterLayer.setVisible(true);
    zeroText.setVisible(false);
    zeroCharacterLayer.setVisible(false);
    String eol = System.getProperty("line.separator");
    while (msg.contains(eol)) {
      msg = msg.replace(eol, "$k$p");
    }
    msg = msg.replaceAll("\\\\n", "\n");
    msg = parseMessage(msg);
    String name;
    CanvasUtils.clearCanvas(background);
    if (Config.getShowBackground() && backgroundImage != null) {
      background.getGraphicsContext2D().drawImage(backgroundImage, 0, 0);
    }
    if (CharacterManager.getNames().containsKey(activeChar.getName())) {
      if (activeChar.getName().equals("username") ||
          activeChar.getName().equals("username2")) {
        name = CharacterManager.getMyUnit().getName();
      } else {
        name = CharacterManager.getNames().get(activeChar.getName());
      }
    } else {
      name = activeChar.getName();
    }
    characterLayer.getGraphicsContext2D().restore();
    characterLayer.getGraphicsContext2D().save();
    CanvasUtils.clearCanvas(characterLayer);
    if (!activeChar.getName().isEmpty()) {
      if (activeChar.equals(charA)) {
        charB.drawCharacterStageImage(characterLayer, false);
        charA.drawCharacterStageImage(characterLayer, true);
      } else {
        charA.drawCharacterStageImage(characterLayer, false);
        charB.drawCharacterStageImage(characterLayer, true);
      }
    } else {
      charA.drawCharacterStageImage(characterLayer, false);
      charB.drawCharacterStageImage(characterLayer, false);
    }
    CanvasUtils.clearCanvas(textBox);
    Image tB  = ResourceManager.loadImage("TextBox.png");
    int x = 10;
    int y = (int) (240 - tB.getHeight() + 2);
    GraphicsContext g = textBox.getGraphicsContext2D();
    g.drawImage(tB, x, y);
    CanvasUtils.clearCanvas(text);
    String bmsg = msg;
    int bx = x;
    int by = y;
    GameMode.branch(() -> {
      drawString(text, bmsg, bx + 29, by + 26, textColor);
    }, () -> {
      drawString(text, bmsg, bx + 29, by + 22, textColor);
    });
    if (curIndex < messages.size() - 1) {
      g.drawImage(ResourceManager.loadImage("KeyPress.png"),
                  textBox.getWidth() - 33 ,
                  textBox.getHeight() - tB.getHeight() + 32);
    }
    CanvasUtils.clearCanvas(nameBox);
    if (!activeChar.getName().isEmpty()) {
      g = nameBox.getGraphicsContext2D();
      Image nB = ResourceManager.loadImage("NameBox.png");
      int xofs;
      int yofs;
      if (activeChar.equals(charB)) {
        xofs = (int) (400 - nB.getWidth() - 6);
        if (Config.getGameMode().isAwakening()) {
          xofs -= 30;
        }
        yofs = (int) (240 - tB.getHeight() - 14);
      } else {
        xofs = 7;
        if (Config.getGameMode().isAwakening()) {
          xofs += 30;
        } 
        yofs = (int) (240 - tB.getHeight() - 14);
      }
      g.drawImage(nB, xofs, yofs);
      int xpos = (int) (xofs + nB.getWidth() / 2 -
                        ResourceManager.getTextLength(name) / 2);
      drawString(nameBox, name, xpos, yofs + 16, NAME_COLOR);
    }
  }
  
  /**
   * Renders a message with the type zero display format.
   * 
   * @param msg the message to render
   * @throws GameBranchMissingException when a branch has not been supplied for
   * a game mode
   */
  public void renderTypeZero(String msg) throws GameBranchMissingException {
    String eol = System.getProperty("line.separator");
    while (msg.contains(eol)) {
      msg = msg.replace(eol, "$k$p");
    }
    msg = msg.replaceAll("\\\\n", "\n");
    parseMessage(msg);
    CanvasUtils.clearCanvas(background);
    if (Config.getShowBackground() && backgroundImage != null) {
      background.getGraphicsContext2D().drawImage(backgroundImage, 0, 0);
    }
    text.setVisible(false);
    characterLayer.setVisible(false);
    zeroText.setVisible(true);
    zeroCharacterLayer.setVisible(true);
    zeroCharacterLayer.getGraphicsContext2D().restore();
    zeroCharacterLayer.getGraphicsContext2D().save();
    CanvasUtils.clearCanvas(zeroCharacterLayer);
    CanvasUtils.clearCanvas(textBox);
    Image tB  = ResourceManager.loadImage("TextBox.png");
    int x = 10;
    int y;
    GraphicsContext g = textBox.getGraphicsContext2D();
    GraphicsContext g2 = nameBox.getGraphicsContext2D();
    CanvasUtils.clearCanvas(zeroText);
    CanvasUtils.clearCanvas(nameBox);
    Image nB = ResourceManager.loadImage("NameBox.png");
    String name;
    if (!topMessage.isEmpty()) {
      UnaryOperator<Integer> awakening = (i) -> 20;
      UnaryOperator<Integer> fates = (i) -> 3;
      y = GameMode.branch(0, awakening, fates);
      g.drawImage(tB, x, y);
      charA.drawCharacterBustImage(zeroCharacterLayer, y);
      int bx = x;
      int by = y;
      GameMode.branch(() -> { // Awakening
        drawString(zeroText, topMessage, bx + 76, by + 26, textColor);
      }, () -> { // Fates
        drawString(zeroText, topMessage, bx + 76, by + 22, textColor);
      });
      if (!charA.getName().isEmpty()) {
        if (CharacterManager.getNames().containsKey(charA.getName())) {
          if (activeChar.getName().equals("username") ||
              activeChar.getName().equals("username2")) {
            name = CharacterManager.getMyUnit().getName();
          } else {
            name = CharacterManager.getNames().get(charA.getName());
          }
        } else {
          name = charA.getName();
        }
        awakening = (i) -> (int) (i - nB.getHeight() + 10);
        fates = (i) -> (int) (by + tB.getHeight() - (nB.getHeight() - 16));
        int yofs = GameMode.branch(y, awakening, fates);
        GameMode.branch(() -> { // Awakening
          g2.drawImage(nB, 76, yofs);
        }, () -> { // Fates
          g2.drawImage(nB, 7, yofs);
        });
        String uName = name;
        awakening = (i) -> (int) (76 + nB.getWidth() / 2 -
                                 ResourceManager.getTextLength(uName) / 2);
        fates = (i) -> (int) (7 + nB.getWidth() / 2 -
                             ResourceManager.getTextLength(uName) / 2);
        int xpos = GameMode.branch(0, awakening, fates);
        drawString(nameBox, name, xpos, yofs + 16, NAME_COLOR);
      }
    }
    if (!bottomMessage.isEmpty()) {
      y = 240 - (int) tB.getHeight() + 2;
      g.drawImage(tB, x, y);
      charB.drawCharacterBustImage(zeroCharacterLayer, y); 
      int bx = x;
      int by = y;
      GameMode.branch(() -> {
        drawString(zeroText, bottomMessage, bx + 76, by + 26, textColor);
      }, () -> {
        drawString(zeroText, bottomMessage, bx + 76, by + 22, textColor);
      });
      if (!charB.getName().isEmpty()) {
        if (CharacterManager.getNames().containsKey(charB.getName())) {
          if (activeChar.getName().equals("username") ||
              activeChar.getName().equals("username2")) {
            name = CharacterManager.getMyUnit().getName();
          } else {
            name = CharacterManager.getNames().get(charB.getName());
          }
        } else {
          name = charB.getName();
        }
        UnaryOperator<Integer> awakening = (i) -> {
          return (int) (i - (nB.getHeight() - 10));
        };
        UnaryOperator<Integer> fates = (i) -> {
          return (int) (i - (nB.getHeight() - 8));
        };
        int yofs = GameMode.branch(y, awakening, fates);
        GameMode.branch(() -> { // Awakening
          g2.drawImage(nB, 76, yofs);
        }, () -> { // Fates
          g2.drawImage(nB, 7, yofs);
        });
        String uName = name;
        awakening = (i) -> (int) (76 + nB.getWidth() / 2 -
                                 ResourceManager.getTextLength(uName) / 2);
        fates = (i) -> (int) (7 + nB.getWidth() / 2 -
                             ResourceManager.getTextLength(uName) / 2);
        int xpos = GameMode.branch(0, awakening, fates);
        drawString(nameBox, name, xpos, yofs + 16, NAME_COLOR);
      }
    }
  }
  
  /**
   * Saves all of the frames in the conversation to the chosen file.
   * 
   * @throws GameBranchMissingException when a branch has not been supplied for
   * a game mode
   */
  public void saveAllImages() throws GameBranchMissingException {
    if (fileSelectionDialog == null) {
      fileSelectionDialog = new FileChooser();
      fileSelectionDialog.setInitialFileName("output.png");
    }
    Canvas imageOutputCanvas;
    File outFile = fileSelectionDialog.showSaveDialog(null);
    if (outFile != null) {
      try {
        getScene().setCursor(Cursor.WAIT);
        int height = messages.size() * (int) (240 * scale);
        Config.pauseSound();
        WritableImage[] outImages = new WritableImage[messages.size()];
        charA.clear();
        charB.clear();
        for (int i = 0; i < messages.size(); i++) {
          outImages[i] = new WritableImage((int) (400 * scale),
                                           (int) (240 * scale));
          if (conversationType == ConversationTypes.TYPE_0) {
            renderTypeZero(messages.get(i));
          } else {
            renderTypeOne(messages.get(i));
          }
          snapshot(null, outImages[i]);
        }
        displayUpTo(curIndex);
        if (height <= 4096 || "sw".equals(System.getProperties()
                                          .getProperty("prism.order"))) {
          imageOutputCanvas = new Canvas(400, height);
          GraphicsContext g = imageOutputCanvas.getGraphicsContext2D();
          for (int i = 0; i < outImages.length; i++) {
            g.drawImage(outImages[i], 0, i * (240 * scale));
          }
          WritableImage outImg = new WritableImage((int) (400 * scale), height);
          getChildren().add(imageOutputCanvas);
          imageOutputCanvas.snapshot(SNAPSHOT_PARAMETERS, outImg);
          getChildren().remove(imageOutputCanvas);
          try {
            ImageIO.write(SwingFXUtils.fromFXImage(outImg, null), "png",
                          outFile);
          } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Failed to write image to file \"" +
                                     outFile + "\".", e);
          }
        } else {
          int c = 0;
          GraphicsContext g;
          WritableImage outImg;
          int i = 0;
          int last = 0;
          int offset = 0;
          int imgHeight = (int) (240 * scale);
          int perImg = 4080 / imgHeight;
          int h;
          for (c = 0; c <= height / 4080; c++) {
            i = last;
            offset = last;
            last = last + perImg;
            if (last < outImages.length) {
              h = perImg * imgHeight;
            } else {
              h = (outImages.length - (last - perImg)) * imgHeight;
              last = outImages.length;
            }
            imageOutputCanvas = new Canvas(400 * scale, h);
            g = imageOutputCanvas.getGraphicsContext2D();
            while (i < last && i < outImages.length) {
              g.drawImage(outImages[i], 0, (i - offset) * imgHeight);
              i++;
            }
            getChildren().add(imageOutputCanvas);
            outImg = new WritableImage((int) (400 * scale), h);
            imageOutputCanvas.snapshot(null, outImg);
            getChildren().remove(imageOutputCanvas);
            try {
              ImageIO.write(SwingFXUtils.fromFXImage(outImg, null), "png",
                            new File("tmp" + c + ".png"));
            } catch (IOException e) {
              LOGGER.log(Level.SEVERE, "Failed to write image to file \"tmp" +
                                       c + ".png\".", e);
            }
          }
          File[] images = new File[c];
          for (i = 0; i < c; i++) {
            images[i] = new File("tmp" + i + ".png");
          }
          combineImageChunks(images, outFile);
          for (i = 0; i < images.length; i++) {
            images[i].delete();
          }
        }
        Config.resumeSound();
      } finally {
        getScene().setCursor(Cursor.DEFAULT);
      }
    }
  }
  
  /**
   * Saves the current frame in the conversation to the chosen file.
   */
  public void saveSingleImage() {
    if (fileSelectionDialog == null) {
      fileSelectionDialog = new FileChooser();
      fileSelectionDialog.setInitialFileName("output.png");
    }
    File outFile = fileSelectionDialog.showSaveDialog(null);
    WritableImage outImg = new WritableImage((int) (400 * scale),
                                             (int) (240 * scale));
    snapshot(SNAPSHOT_PARAMETERS, outImg);
    if (outFile != null) {
      try {
        ImageIO.write(SwingFXUtils.fromFXImage(outImg, null), "png", outFile);
      } catch (IOException e) {
        LOGGER.log(Level.SEVERE, "Failed to write image to file \"" +
                                 outFile + "\".", e);
      }
    }
  }
  
  /**
   * Changes the scale of the ConversationPane.
   * 
   * @param scale the ConversationPane's new scale.
   */
  public void setScale(double scale) {
    this.scale = scale;
    background.setScaleX(scale);
    background.setScaleY(scale);
    characterLayer.setScaleX(scale);
    characterLayer.setScaleY(scale);
    zeroCharacterLayer.setScaleX(scale);
    zeroCharacterLayer.setScaleY(scale);
    textBox.setScaleX(scale);
    textBox.setScaleY(scale);
    text.setScaleX(scale);
    text.setScaleY(scale);
    zeroText.setScaleX(scale);
    zeroText.setScaleY(scale);
    nameBox.setScaleX(scale);
    nameBox.setScaleY(scale);
    setMinSize(400 * scale, 240 * scale);
    setPrefSize(400 * scale, 240 * scale);
  }
  
  /**
   * Sets the displayed text to the given message.
   * 
   * @param msg the message to display
   * @param nextButton the button that will advance the display to the next
   * message
   * @param previousButton the button that will revert the display to the
   * previous message
   * @throws GameBranchMissingException  when a branch has not been supplied for
   * a game mode
   */
  public void setText(String msg, Button nextButton, Button previousButton)
                                             throws GameBranchMissingException {
    displaying = true;
    ResourceManager.stopBGM();
    forNext.clear();
    useForNext.clear();
    textColor = TEXT_COLOR;
    setType = false;
    activeChar.clear();
    charA.clear();
    charB.clear();
    clearAll();
    topMessage = "";
    bottomMessage = "";
    if (backgroundImage == null) {
      backgroundImage = ResourceManager.loadImage("SupportBG.png");
    }
    if (msg.isEmpty()) {
      nextButton.setDisable(true);
      previousButton.setDisable(true);
      return;
    }
    if (msg.startsWith("[" + XMLManager.getText("Script_Command_Script"))) {
      msg = ConversationFormatter.formatLines(msg);
    } else if (msg.startsWith("[" + XMLManager.getText(
                                          "Script_Command_Formatted_Script"))) {
      msg = ScriptHelper.convertFromScript(msg);
    }
    String[] lines = msg.split("([\r\n+]|\\$k\\$p|\\$k\\\\n)");
    messages.clear();
    for (String line : lines) {
      if (!line.isEmpty() && !line.matches("^\\$Sbs\\w+\\|")) {
        messages.add(line);
      }
      if (!setType) {
        if (line.contains("$t0")) {
          conversationType = ConversationTypes.TYPE_0;
          setType = true;
        } else if (line.contains("$t1")) {
          conversationType = ConversationTypes.TYPE_1;
          setType = true;
        }
      }
    }
    curIndex = -1;
    previousButton.setDisable(true);
    nextButton.setDisable(false);
    displayNext(nextButton, previousButton);
  }
  
  /**
   * Sets up the drag and drop events for the conversation pane.
   */
  private void setupDragDrop() {
    setOnDragOver((DragEvent e) -> {
      Dragboard d = e.getDragboard();
      if (d.hasFiles()) {
        String name = d.getFiles().get(0).getName().toLowerCase();
        if (name.endsWith(".png") || name.endsWith(".jpg") ||
            name.endsWith(".jpeg") || name.endsWith("bmp") ||
            name.endsWith(".txt") || name.endsWith(".bin") ||
            name.endsWith(".bin.lz") || name.endsWith(".fescript")) {
          e.acceptTransferModes(TransferMode.COPY);
        }
      } else {
        e.consume();
      }
    });
    setOnDragDropped((DragEvent e) -> {
      if (e.getDragboard().hasFiles()) {
        File file = e.getDragboard().getFiles().get(0);
        Platform.runLater(() -> {
          String name = file.getName().toLowerCase();
          if (name.endsWith(".txt") || name.endsWith(".bin") ||
            name.endsWith(".bin.lz") || name.endsWith(".fescript")) {
            getScene().getRoot().fireEvent(e);
          } else {
            try {
              backgroundImage = new Image(file.toURI().toURL().toString());
              if (!messages.isEmpty()) {
                CanvasUtils.clearCanvas(background);
                GraphicsContext g = background.getGraphicsContext2D();
                g.drawImage(backgroundImage, 0, 0);
              }
              e.setDropCompleted(true);
            } catch (NullPointerException | IllegalArgumentException |
                     MalformedURLException ex) {
              LOGGER.log(Level.SEVERE, "Unable to find image \"" + file + "\".",
                         ex);
              e.setDropCompleted(false);
            } finally {
              e.consume();
            }
          }
        });
      } else {
        e.setDropCompleted(false);
        e.consume();
      }
    });
  }
  
  /**
   * Parses text control codes in the given message string.
   * 
   * @param msg the message string to parse
   * @return the message string with control codes stripped out
   */
  private String parseMessage(String msg) {
    setColor = false;
    forNext.forEach((cmd) -> runCommand(null, cmd, 0));
    forNext.clear();
    for (int i = 0; i < msg.length(); i++) {
      if (msg.charAt(i) == '$') {
        CommandPair res = parseCommand(msg, i);
        msg = runCommand(res.message, res.command, i);
        i--;
      }
    }
    msg = msg.replaceAll("\\\\n", "\n");
    if (charA.equals(activeChar)) {
      topMessage = msg;
    } else {
      bottomMessage = msg;
    }
    forNext.addAll(useForNext);
    useForNext.clear();
    return msg;
  }
  
  /**
   * Runs a control code.
   * 
   * @param msg the message to which the control code belongs
   * @param cmd the control code
   * @param i the index of the control code in the message
   * @return the message after any transformations the control code may perform
   */
  private String runCommand(String msg, Command cmd, int i) {
    switch (cmd.cmd) {
      case "$E":
        if (!activeChar.getName().isEmpty() && activeChar.equals(charB)) {
          charB.setEmotion(cmd.params[0]);
        } else {
          charA.setEmotion(cmd.params[0]);
        }
        break;
      case "$G":
        int isFemale = 0;
        if (CharacterManager.getMyUnit().isFemale()) {
          isFemale = 1;
        }
        String gText = cmd.params[isFemale];
        msg = msg.substring(0, i) + gText + msg.substring(i);
        break;
      case "$Ws":
        if (charA.getName().equals(cmd.params[0])) {
          activeChar.set(charA);
        } else if (charB.getName().equals(cmd.params[0])) {
          activeChar.set(charB);
        } else {
          activeChar.setName(cmd.params[0]);
        }
        break;
      case "$Wm":
        if (conversationType == ConversationTypes.TYPE_1) {
          if (cmd.params[1].equals("3")) {
            charA.setName(cmd.params[0]);
            charA.resetEmotion();
          } else if (cmd.params[1].equals("7")) {
            charB.setName(cmd.params[0]);
            charB.resetEmotion();
          }
        } else {
          switch (cmd.params[1]) {
            case "0":
            case "2":
              charA.setName(cmd.params[0]);
              charA.resetEmotion();
              break;
            case "6":
              charB.setName(cmd.params[0]);
              charB.resetEmotion();
              break;
          }
        }
        break;
      case "$Wd":
        if (activeChar.equals(charB)) {
          activeChar.set(charA);
          charB.setName("");
          bottomMessage = "";
        } else {
          activeChar.set(charB);
          charA.setName("");
          topMessage = "";
        }
        break;
      case "$a":
        hasPerms = true;
        break;
      case "$t0":
        if (!setType) {
          conversationType = ConversationTypes.TYPE_0;
        }
        setType = true;
        break;
      case "$t1":
        if (!setType) {
          conversationType = ConversationTypes.TYPE_1;
        }
        setType = true;
        break;
      case "$Nu":
        if (hasPerms || Config.getGameMode().isAwakening()) {
          msg = msg.substring(0, i) +
                CharacterManager.getMyUnit().getName() +
                msg.substring(i);
        } else {
          msg = msg.substring(0, i) + "$Nu" + msg.substring(i);
          i += 2;
        }
        break;
      case "$Svp":
        String clip = cmd.params[0];
        if (clip.contains("#")) {
          clip = clip.replace("#", CharacterManager.getMyUnit().getVoice());
        }
        ResourceManager.playSound(clip);
        break;
      case "$Sbp":
        ResourceManager.playBGM(cmd.params[0],
                              Integer.parseInt(cmd.params[1]));
        break;
      case "$c":
        if (setColor) {
          useForNext.add(cmd);
        } else {
          setColor = true;
          String[] rgba = cmd.params;
          textColor = Color.rgb(Integer.parseInt(rgba[0]),
                                Integer.parseInt(rgba[1]),
                                Integer.parseInt(rgba[2]),
                                Integer.parseInt(rgba[3]) / 255.0);
        }
        break;
      case "$Wa":
      case "$Wc":
      default:
        break;
    }
    return msg;
  }
}
