/*
 * Copyright (C) 2016 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.ui.control;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import org.apache.commons.collections4.map.ReferenceMap;
import sahara.cactus.fireemblem.conversation.manager.CharacterManager;
import sahara.cactus.fireemblem.conversation.manager.Config;
import sahara.cactus.fireemblem.conversation.manager.ResourceManager;
import sahara.cactus.fireemblem.conversation.wrapper.CharacterReference;

/**
 * Allows for the selection of audio in the Conversation Editor.
 * 
 * @author SecretiveCactus
 */
public class Palette extends HBox {
  
  /**
   * Contains all of the application's audio panels, each of which are
   * mapped to the name of their owner.
   */
  private final ReferenceMap<String,ScrollPane> audioPanels;
  
  /**
   * Contains all of the application's portrait panels, each of which are
   * mapped to the name of their owner.
   */
  
  /**
   * Allows the user to select which portrait panel should be displayed. Pulls
   * its contents from filteredCharacterReferences.
   */
  private ListView<CharacterReference> characterList;
  
  /**
   * Contains all of the children of this class.
   */
  private SplitPane contentPane;
  
  /**
   * Contains the search box and character list.
   */
  private VBox listVBox;
  
  /**
   * Allows the user to search for characters by typing in the beginning of
   * their names. Does not support regular expression matching or matching of
   * later parts of names.
   */
  private TextField searchBox;
  
  /**
   * The TabPane on which the AudioPalette is based.
   */
  private final TabPane tabs;
  
  /**
   * A pattern for sorting sounds.
   */
  private static final Pattern SOUND_PATTERN = Pattern.compile("^(.+?)_(\\d+)");
  
  /**
   * A generic empty panel used for empty cases.
   */
  private static final VBox emptyPanel = new VBox();  
  
  
  /**
   * Creates an AudioPalette with the specified number of clips per row and
   * player width.
   * 
   * @param tabs the TabPane on which the AudioPalette is based
   */
  public Palette(TabPane tabs) {
    super();
    this.tabs = tabs;
    audioPanels = new ReferenceMap<>(ReferenceMap.ReferenceStrength.SOFT,
                                     ReferenceMap.ReferenceStrength.SOFT);
    addSplitPane();
    addCharacterList();
    CharacterManager.searchProperty().bind(searchBox.textProperty());
    characterList.getSelectionModel().select(0);
  }
  
  /**
   * Handles playing the desired audio file and copying its string
   * to the user's system clipboard.
   */
  private final class AudioHandler implements EventHandler<ActionEvent>,
                                              ClipboardOwner {
    /**
     * The name of the file that will be played when the user clicks the owning
     * button.
     */
    private final String name;
    
    /**
     * The TabPane on which the Palette that owns the AudioHandler is based.
     */
    private final TabPane tabs;
    
    /**
     * The text that will be copied to the user's system clipboard when the
     * owning button is clicked under script mode.
     */
    private final StringSelection scriptText;
    
    /**
     * The text that will be copied to the user's system clipboard when the
     * owning button is clicked under raw mode.
     */
    private final StringSelection rawText;
    
    /**
     * Creates an AudioHandler with the given text.
     *
     * @param audioName the text that will be copied to the user's system
     * clipboard when the owning portrait is clicked
     * @param tabs the tab panes from which the AudioHandler will determine what
     * type of text to copy
     */
    public AudioHandler(String audioName, TabPane tabs) {
      name = audioName;
      rawText = new StringSelection("$Svp" + name + "|");
      scriptText = new StringSelection("[" + name + "]");
      this.tabs = tabs;
    }
    
    /**
     * Copies the AudioHandler's text to the user's system clipboard.
     * Called when the user clicks on the owning portrait.
     */
    @Override
    public void handle(ActionEvent e) {
      if (name.contains("#")) {
        ResourceManager.playSound(name.replace("#",
                                      CharacterManager.getMyUnit().getVoice()));
      } else {
        ResourceManager.playSound(name);
      }
      Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
      Tab tab = tabs.getSelectionModel().getSelectedItem();
      if (tab != null) {
        MessageArea area = (MessageArea) tab.getContent();
        if (area.getText().startsWith("[SCRIPT]") ||
            area.getText().startsWith("[FORMATTED SCRIPT]")) {
          clipboard.setContents(scriptText, this);
        } else {
          clipboard.setContents(rawText, this);
        }
      }
    }
    
    /** {@inheritDoc} */
    @Override
    public void lostOwnership(Clipboard cboard, Transferable cntnt){
    }
  }
  
  /**
   * Creates the VBox that contains all list-related children. 
   */
  private void addCharacterList() {
    listVBox = new VBox();
    SplitPane.setResizableWithParent(listVBox, false);
    listVBox.setMinWidth(150);
    addSearchBox();
    addListView();
    contentPane.getItems().addAll(listVBox, emptyPanel);
  }
  
  /**
   * Creates the ListView that allows the user to select which character they
   * wish to select portraits for and adds it to the listVBox.
   */
  private void addListView() {
    characterList = new ListView<>(CharacterManager.charactersWithAudio());
    VBox.setVgrow(characterList, Priority.ALWAYS);
    characterList.getSelectionModel().selectedItemProperty().addListener(
      (o, oV, nV) -> {
        if (nV != null) {
          contentPane.getItems().set(1, createAudioPanel(nV.toString()));
        }
      }
    );
    Config.gameModeProperty().addListener((o, ov, nv) -> {
      characterList.getSelectionModel().select(0);
    });
    listVBox.getChildren().add(characterList);
  }
  
  /**
   * Creates the search box and adds it to the ListVBox.
   */
  private void addSearchBox() {
    searchBox = new TextField();
    searchBox.textProperty().addListener((observable, oldValue, newValue) -> {
      Platform.runLater(() -> characterList.getSelectionModel().select(0));
    });
    listVBox.getChildren().add(searchBox);
  }
  
  /**
   * Creates the SplitPane that contains all of the application's child controls
   * and adds it to the PortraitPane.
   */
  private void addSplitPane() {
    contentPane = new SplitPane();
    HBox.setHgrow(contentPane, Priority.ALWAYS);
    contentPane.setDividerPosition(0, 0);
    getChildren().add(contentPane);
  }
  
  /**
   * Creates an audio panel for the character with the given name.
   * @param name the name of the character for whom a panel should be created
   * @return an audio panel for the character with the given name
   */
  private Region createAudioPanel(String name) {
    CharacterReference character;
    if (audioPanels.get(name) != null) {
      return audioPanels.get(name);
    }
    else if (!(character = CharacterManager.getByName(name)).hasSound()) {
      return emptyPanel;
    }
    VBox audioPane;
    TilePane buttonGroup = new TilePane(12, 12);
    Button audioButton;
    ScrollPane scrollPane;
    String currentGroup = null;
    Matcher match;
    Insets insets = new Insets(12, 0, 12, 12);
    audioPane = new VBox();
    VBox.setMargin(buttonGroup, insets);
    audioPane.setFillWidth(true);
    SortedList<String> audioClips = character.getAudioClips();
    for (int j = 0; j < audioClips.size(); j++ ) {
      match = SOUND_PATTERN.matcher(audioClips.get(j));
      match.find();
      if (j == 0) {
        currentGroup = match.group(1);
      } else if (!currentGroup.equals(match.group(1))) {
        currentGroup = match.group(1);
        audioPane.getChildren().add(buttonGroup);
        buttonGroup = new TilePane(12, 12);
        VBox.setMargin(buttonGroup, insets);
      }
      audioButton = new Button();
      audioButton.setMnemonicParsing(false);
      audioButton.setText(audioClips.get(j));
      audioButton.setOnAction(new AudioHandler(audioClips.get(j), tabs));
      audioButton.setMaxWidth(Double.MAX_VALUE);
      buttonGroup.getChildren().add(audioButton);
    }
    audioPane.getChildren().add(buttonGroup);
    scrollPane = new ScrollPane(audioPane);
    scrollPane.setFitToWidth(true);
    return scrollPane;
  }
}
