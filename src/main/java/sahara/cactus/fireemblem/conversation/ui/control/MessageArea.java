/*
 * Copyright (C) 2017 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.ui.control;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.StyleSpans;
import org.fxmisc.richtext.StyleSpansBuilder;
import sahara.cactus.fireemblem.conversation.manager.CharacterManager;
import sahara.cactus.fireemblem.conversation.manager.ResourceManager;
import sahara.cactus.fireemblem.conversation.manager.XMLManager;
import sahara.cactus.fireemblem.conversation.util.text.ScriptHelper;

/**
 * A text area with syntax highlighting for script commands.
 * 
 * @author Secretive Cactus
 */
public class MessageArea extends CodeArea {
  
  /**
   * Provides logging for the class.
   */
  private static final Logger LOGGER =
                                  Logger.getLogger(MessageArea.class.getName());
  
  /**
   * The name of the last character found during lexing.
   */
  private static String LAST_CHARACTER;
  
  /**
   * Contains a pattern that matches script syntax. Refer to the regular
   * expression documentation in ScriptHelper to find out what these all do.
   */
  private static final Pattern SCRIPT_SYNTAX = Pattern.compile(
    "(?:(?<BGM>\\(" + XMLManager.getText("Script_Command_BGM") +
      ":\\s*(\\w+)(?:,\\s*(.+?))?\\))" +
    "|(?<SOUNDEFFECT>\\(" + XMLManager.getText("Script_Command_Sound_Effect") +
      ":\\s*[\\w#]+\\))" +
    "|(?<SCRIPT>\\[(?:" + XMLManager.getText("Script_Command_Script") + "|" +
      XMLManager.getText("Script_Command_Formatted_Script") + ")\\])" +
    "|(?<TEXTCOLOR>\\(" + XMLManager.getText("Script_Command_Text_Color") +
      ":\\s*\\d+,\\s*\\d+,\\s*\\d+,\\s*\\d+\\))" +
    "|(?<TYPE>\\(" + XMLManager.getText("Script_Command_Type") +
      ":\\s*\\w+\\))" +
    "|(?<ENTER>\\(" + XMLManager.getText("Script_Command_Enter") + 
      "\\s+(\\w+):\\s*([^,\\)]+)(?:,\\s*(\\w+)([^\\)]+)?)?\\))" +
    "|(?<EMOTION>\\(" + XMLManager.getText("Script_Command_Emotion_Shift") +
      ":\\s*([^,\\)]+)(?:,\\s*(\\w+)([^\\)\r\n]+))\\))" +
    "|(?<LONEEMOTION>\\(" + XMLManager.getText("Script_Command_Emotion_Shift") +
      ":\\s*(\\w+)\\))" +
    "|(?<EXIT>\\(" + XMLManager.getText("Script_Command_Exit") +
      "(?::\\s*([^\\)]+))?\\))" +
    "|(?<SPEAK>\n([^:\\[\\(\r\n]+)(?:\\s*\\{[^\\}]*\\})?(?:\\s*\\[([\\w#]+)" +
      "\\])?(?:\\s*\\{[^\\}]*\\})?(?:\\s*\\((\\w+)([^\\)]+)?\\))?:\\s*(.+))" +
    "|(?<G>\\$G([^,]+),([^|]+)\\|)" +
    "|(?<NU>\\$Nu)" +
    "|(?<PERMANENTS>\\(" + XMLManager.getText("Script_Command_Has_Permanents") +
      "\\)))"
  );
  
  /**
   * Creates a new MessageArea with the given initial content.
   * 
   * @param content the initial content of the MessageArea
   */
  public MessageArea(String content) {
    super(content);
    richChanges().filter((ch) -> !ch.getInserted().equals(ch.getRemoved()))
      .subscribe((change) -> {
        try {
          setStyleSpans(0, lexScript(getText()));
        } catch (IllegalArgumentException ex) {
          LOGGER.log(Level.FINE, ex.toString(), ex);
        }
      });
    try {
      setStyleSpans(0, lexScript(getText()));
    } catch (IllegalArgumentException ex) {
      LOGGER.log(Level.FINE, ex.toString(), ex);
    }
  }
  
  /**
   * The style class for a keyword.
   */
  private static final Set<String> KEYWORD = Collections.singleton("keyword");
  
  /**
   * The style class for a valid parameter.
   */
  private static final Set<String> VALID_PARAMETER = Collections.singleton(
                                                             "valid-parameter");
  
  /**
   * The style class for an invalid parameter.
   */
  private static final Set<String> INVALID_PARAMETER = Collections.singleton(
                                                           "invalid-parameter");
  
  /**
   * The style class for a valid character name.
   */
  private static final Set<String> VALID_NAME = Collections.singleton(
                                                                  "valid-name");
  
  /**
   * Matches and styles control codes within regular dialog text.
   * 
   * @param text the text to be styled
   * @param builder the builder that will style the text
   */
  private static void handleText(String text,
                                StyleSpansBuilder<Collection<String>> builder) {
    String type = null;
    int last = 0;
    int len;
    Matcher match = SCRIPT_SYNTAX.matcher(text);
    while (match.find()) {
      if (match.group("G") != null) {
        type = "G";
      } else if (match.group("NU") != null) {
        type = "NU";
      }
      if ((len = match.start() - last) > 0) {
        builder.add(Collections.emptyList(), len);
      }
      if (type != null) {
        styleText(type, match.group(0), builder);
      } else {
        builder.add(Collections.emptyList(), match.end() - match.start());
      }
      last = match.end();
    }
    builder.add(Collections.emptyList(), text.length() - last);
  }
  
  /**
   * Scans the script for styleable areas.
   * 
   * @param text the script to scan
   * @return a StyleSpans object reflecting the script's styles
   */
  private static StyleSpans<Collection<String>> lexScript(String text) {
    LAST_CHARACTER = null;
    int last = 0;
    int len;
    StyleSpansBuilder<Collection<String>> builder = new StyleSpansBuilder<>();
    if (text.startsWith("[" + XMLManager.getText("Script_Command_Script")) ||
        text.startsWith("[" + XMLManager.getText(
                                          "Script_Command_Formatted_Script"))) {
      Matcher match = SCRIPT_SYNTAX.matcher(text);
      String type = null;
      while (match.find()) {
        if (match.group("SPEAK") != null) {
          type = "SPEAK";
        } else if (match.group("EMOTION") != null) {
          type = "EMOTION";
        } else if (match.group("LONEEMOTION") != null) {
          type = "LONEEMOTION";
        } else if (match.group("NU") != null) {
          type = "NU";
        } else if (match.group("G") != null) {
          type = "G";
        } else if (match.group("ENTER") != null) {
          type = "ENTER";
        } else if (match.group("EXIT") != null) {
          type = "EXIT";
        } else if (match.group("SOUNDEFFECT") != null) {
          type = "SOUNDEFFECT";
        } else if (match.group("BGM") != null) {
          type = "BGM";
        } else if (match.group("PERMANENTS") != null) {
          type = "SCRIPT";
        } else if (match.group("TYPE") != null) {
          type = "TYPE";
        } else if (match.group("SCRIPT") != null) {
          type = "SCRIPT";
        } else if (match.group("TEXTCOLOR") != null) {
          type = "TEXTCOLOR";
        }
        if ((len = match.start() - last) > 0) {
          builder.add(Collections.emptyList(), len);
        }
        if (type != null) {
          styleText(type, match.group(0), builder);
        } else {
          builder.add(Collections.emptyList(), match.end() - match.start());
        }
        last = match.end();
      }
      builder.add(Collections.emptyList(), text.length() - last);
    } else {
      builder.add(Collections.emptyList(), text.length());
    }
    return builder.create();
  }
  
  /**
   * Checks whether or not the passed script contains a valid character name.
   * 
   * @param script the script to scan
   * @param i the starting position of the name
   * @param i2 the ending position of the name
   * @param builder the builder that will style the name
   */
  private static void processCharacterName(String script, int i, int i2,
                                StyleSpansBuilder<Collection<String>> builder) {
    LAST_CHARACTER = script.substring(i, i2);
    if (CharacterManager.getInverseNames().get(LAST_CHARACTER) != null ||
        CharacterManager.getMyUnit().getName().equals(LAST_CHARACTER)) {
      builder.add(VALID_NAME, i2 - i);
    } else {
      builder.add(INVALID_PARAMETER, i2 - i);
    }
  }
  
  /**
   * Processes the emotions in the passed segment of a script.
   * 
   * @param script the segment of a script that contains the emotions
   * @param i the index at which the emotions begin
   * @param builder the builder that will style the emotions
   * @return the ending offset of the emotions
   */
  private static int processEmotions(String script, int i,
                                StyleSpansBuilder<Collection<String>> builder) {
    script = script.substring(0, script.indexOf(")") + 1);
    int oi = i;
    int i2;
    int imod = 0;
    int index = 0;
    i2 = i + 1;
    while (script.charAt(i) == ' ') {
      i++;
    }
    if (i > i2) {
      builder.add(Collections.emptyList(), i - i2);
    }
    while ((i2 = script.indexOf(",", i)) > 0) {
      if (i2 > i) {
        if (ScriptHelper.isValidEmotion(LAST_CHARACTER, script.substring(i, i2),
                                        index)) {
          builder.add(VALID_PARAMETER, i2 - (i + imod));
        } else {
          builder.add(INVALID_PARAMETER, i2 - (i + imod));
        }
        imod = 0;
      }
      builder.add(KEYWORD, 1);
      i = ++i2;
      while (script.charAt(i2) == ' ') {
        i2++;
      }
      if (i2 > i) {
        builder.add(Collections.emptyList(), i2 - i);
      }
      i = i2;
      index++;
    }
    i2 = script.indexOf(")");
    if (i2 > i) {
      if (ScriptHelper.isValidEmotion(LAST_CHARACTER, script.substring(i, i2),
                                      index)) {
        builder.add(VALID_PARAMETER, i2 - i);
      } else {
        builder.add(INVALID_PARAMETER, i2 - i);
      }
    }
    builder.add(KEYWORD, 1);
    return i2 + 1;
  }
  
  /**
   * Performs the actual processing related to determining the types and lengths
   * of styled text.
   * 
   * @param type the type of class for which styling will be applied
   * @param script the script fragment to which the styling will be applied
   * @param builder the builder that will style the script
   */
  private static void styleText(String type, String script,
                                StyleSpansBuilder<Collection<String>> builder) {
    int i;
    int i2;
    int i3;
    String sub;
    switch(type) {
      case "SPEAK":
        script = script.substring(1);
        builder.add(Collections.emptyList(), 1);
        i = script.indexOf(":");
        i2 = script.indexOf("[");
        i3 = script.indexOf("{");
        if (i2 > -1 && i2 < i) {
          if (i3 > -1 && i3 < i2) {
            i = i3;
            while (script.charAt(i - 1) == ' ') i--;
            processCharacterName(script, 0, i, builder);
            builder.add(Collections.emptyList(), i3 - i);
            builder.add(KEYWORD, 1);
            i3 = script.indexOf("}");
            builder.add(Collections.emptyList(), i3 - (i + 2));
            builder.add(KEYWORD, 1);
            builder.add(Collections.emptyList(), i2 - (i3 + 1));
          } else {
            i = i2;
            while (script.charAt(i - 1) == ' ') i--;
            processCharacterName(script, 0, i, builder);
            builder.add(Collections.emptyList(), i2 - i);
          }
          builder.add(KEYWORD, 1);
          i = script.indexOf("]");
          if (ResourceManager.getSound(script.substring(i2 + 1, i)) != null) {
            builder.add(VALID_PARAMETER, i - (i2 + 1));
          } else {
            builder.add(INVALID_PARAMETER, i - (i2 + 1));
          }
          builder.add(KEYWORD, 1);
          i = script.indexOf(":");
          i2 = script.indexOf("(");
          i3 = script.indexOf("{", i3);
          if (i2 > -1 && i2 < i) {
            if (i3 > -1 && i3 < i2) {
              builder.add(Collections.emptyList(),
                          i3 - (script.indexOf("]") + 1));
              builder.add(KEYWORD, 1);
              i = script.indexOf("}", i3);
              builder.add(Collections.emptyList(), i - (i3 + 1));
              builder.add(KEYWORD, 1);
              i++;
            } else {
              i = script.indexOf("]") + 1;
            }
            builder.add(Collections.emptyList(), i2 - i);
            builder.add(KEYWORD, 1);
            i2 = processEmotions(script, i2 + 1, builder);
          } else {
            i2 = i;
          }
          builder.add(KEYWORD, 1);
          handleText(script.substring(i2 + 1), builder);
        } else if ((i2 = script.indexOf("(")) > -1 && i2 < i) {
          if (i3 > -1 && i3 < i2) {
            i = i3;
            while (script.charAt(i - 1) == ' ') i--;
            processCharacterName(script, 0, i, builder);
            builder.add(Collections.emptyList(), i3 - i);
            builder.add(KEYWORD, 1);
            i3 = script.indexOf("}");
            builder.add(Collections.emptyList(), i3 - (i + 2));
            builder.add(KEYWORD, 1);
            builder.add(Collections.emptyList(), i2 - (i3 + 1));
          } else {
            i = i2;
            while (script.charAt(i - 1) == ' ') i--;
            processCharacterName(script, 0, i, builder);
            builder.add(Collections.emptyList(), i2 - i);
          }
          builder.add(KEYWORD, 1);
          i2 = processEmotions(script, i2 + 1, builder);
          builder.add(KEYWORD, 1);
          handleText(script.substring(i2 + 1), builder);
        } else {
          if (i3 > -1) {
            i = i3;
            while (script.charAt(i - 1) == ' ') i--;
            processCharacterName(script, 0, i, builder);
            builder.add(Collections.emptyList(), i3 - i);
            builder.add(KEYWORD, 1);
            i = i3;
            i3 = script.indexOf("}");
            builder.add(Collections.emptyList(), i3 - (i + 1));
            builder.add(KEYWORD, 1);
            i = i3 + 1;
          } else {
            processCharacterName(script, 0, i, builder);
          }
          builder.add(KEYWORD, 1);
          handleText(script.substring(i + 1), builder);
        }
        break;
      case "EMOTION":
        i2 = script.indexOf(":") + 1;
        builder.add(KEYWORD, i2);
        i = i2;
        while (script.charAt(i2) == ' ') {
          i2++;
        }
        builder.add(Collections.emptyList(), i2 - i);
        i = i2;
        i2 = script.indexOf(",", i);
        if (ScriptHelper.isValidEmotion(LAST_CHARACTER,
                                        script.substring(i, i2), 0)) {
          processEmotions(script, i, builder);
        } else {
          processCharacterName(script, i, i2, builder);
          builder.add(KEYWORD, 1);
          builder.add(Collections.emptyList(), 1);
          i = i2 + 1;
          processEmotions(script, i, builder);
        }
        break;
      case "LONEEMOTION":
        i2 = script.indexOf(":") + 1;
        builder.add(KEYWORD, i2);
        i = i2;
        while (script.charAt(i2) == ' ') {
          i2++;
        }
        builder.add(Collections.emptyList(), i2 - i);
        processEmotions(script, i2, builder);
        break;
      case "NU":
        builder.add(KEYWORD, 3);
        break;
      case "G":
        builder.add(KEYWORD, 2);
        i = script.indexOf(",");
        builder.add(Collections.emptyList(), i - 2);
        builder.add(KEYWORD, 1);
        i2 = script.indexOf("|");
        builder.add(Collections.emptyList(), i2 - (i + 1));
        builder.add(KEYWORD, 1);
        break;
      case "ENTER":
        i = script.indexOf(" ");
        builder.add(KEYWORD, i);
        while (script.charAt(i) == ' ') {
          i++;
        }
        i2 = script.indexOf(":") + 1;
        sub = script.substring(i, i2 - 1);
        if (sub.equals(XMLManager.getText("Script_Command_Enter_Left")) ||
            sub.equals(XMLManager.getText("Script_Command_Enter_Right")) ||
            sub.equals(XMLManager.getText("Script_Command_Enter_Top")) ||
            sub.equals(XMLManager.getText("Script_Command_Enter_Bottom"))) {
          builder.add(VALID_PARAMETER, i2 - i);
        } else {
          builder.add(INVALID_PARAMETER, i2 - i);
        }
        builder.add(KEYWORD, 1);
        i2 = script.indexOf(":") + 1;
        i = i2;
        while (script.charAt(i2) == ' ') {
          i2++;
        }
        builder.add(Collections.emptyList(), i2 - i);
        i = i2;
        i2 = script.indexOf(",", i);
        if (i2 > -1) {
          processCharacterName(script, i, i2, builder);
          builder.add(KEYWORD, 1);
          builder.add(Collections.emptyList(), 1);
          i = i2 + 1;
          processEmotions(script, i, builder);
        } else {
          i2 = script.indexOf(")", i);
          processCharacterName(script, i, i2, builder);
          builder.add(KEYWORD, 1);
        }
        break;
      case "EXIT":
        i = script.indexOf(":") + 1;
        if (i > 0) {
          builder.add(KEYWORD, i);
          i2 = i;
          while (script.charAt(i) == ' ') {
            i++;
          }
          builder.add(Collections.emptyList(), i - i2);
          i2 = script.indexOf(")");
          processCharacterName(script, i, i2, builder);
          builder.add(KEYWORD, 1);
        } else {
          builder.add(KEYWORD, 6);
        }
        break;
      case "SOUNDEFFECT":
        i = script.indexOf(":") + 1;
        builder.add(KEYWORD, i);
        i2 = i;
        while (script.charAt(i) == ' ') {
          i++;
        }
        builder.add(Collections.emptyList(), i - i2);
        i2 = script.indexOf(")");
        if (ResourceManager.getSound(script.substring(i, i2)) != null) {
          builder.add(VALID_PARAMETER, i2 - i);
        } else {
          builder.add(INVALID_PARAMETER, i2 - i);
        }
        builder.add(KEYWORD, 1);
        break;
      case "BGM":
        i = script.indexOf(":") + 1;
        builder.add(KEYWORD, i);
        while (script.charAt(i) == ' ') {
          i++;
        }
        i2 = script.indexOf(",") + 1;
        if (i2 == 0) {
          i2 = script.indexOf(")") + 1;
          if (ResourceManager.bgmExists(script.substring(i, i2 - 1))) {
            builder.add(VALID_PARAMETER, i2 - i);
          } else {
            builder.add(INVALID_PARAMETER, i2 - i);
          }
          builder.add(KEYWORD, 1);
        } else {
          if (ResourceManager.bgmExists(script.substring(i, i2 - 1))) {
            builder.add(VALID_PARAMETER, i2 - i);
          } else {
            builder.add(INVALID_PARAMETER, i2 - i);
          }
          builder.add(KEYWORD, 1);
          i = i2;
          i2 = script.indexOf(")");
          try {
            Integer.parseInt(script.substring(i + 1, i2));
            builder.add(VALID_PARAMETER, i2 - i);
          } catch (NumberFormatException e) {
            builder.add(INVALID_PARAMETER, i2 - i);
          }
          builder.add(KEYWORD, 1);
        }
        break;
      case "TEXTCOLOR":
        i = script.indexOf(":") + 1;
        builder.add(KEYWORD, i);
        i2 = i;
        while ((i2 = script.indexOf(",", i2)) > -1) {
          builder.add(VALID_PARAMETER, i2 - i);
          builder.add(KEYWORD, 1);
          i2++;
          i = i2;
        }
        i2 = script.indexOf(")");
        builder.add(VALID_PARAMETER, i2 - i);
        builder.add(KEYWORD, 1);
        break;
      case "SCRIPT":
        builder.add(KEYWORD, script.length());
        break;
      case "TYPE":
        i = script.indexOf(":") + 1;
        builder.add(KEYWORD, i);
        i2 = i;
        while (script.charAt(i) == ' ') {
          i++;
        }
        builder.add(Collections.emptyList(), i - i2);
        sub = script.substring(i, script.indexOf(")"));
        if (sub.equals(XMLManager.getText("Script_Command_Type_Zero")) ||
            sub.equals(XMLManager.getText("Script_Command_Type_One"))) {
          builder.add(VALID_PARAMETER, sub.length());
        } else {
          builder.add(INVALID_PARAMETER, sub.length());
        }
        builder.add(KEYWORD, 1);
        break;
    }
  }
}
