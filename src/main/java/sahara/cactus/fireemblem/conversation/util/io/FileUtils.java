/*
 * Copyright (C) 2017 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.util.io;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import sahara.cactus.fireemblem.conversation.file.FireEmblemTextFile;
import sahara.cactus.fireemblem.conversation.file.SupportConversation;
import sahara.cactus.fireemblem.conversation.manager.Config;
import sahara.cactus.fireemblem.conversation.manager.XMLManager;
import sahara.cactus.fireemblem.conversation.ui.control.MessageTab;
import sahara.cactus.fireemblem.conversation.wrapper.FireEmblemMessage;

/**
 * Contains utility methods for dealing with files.
 * 
 * @author Secretive Cactus
 */
public final class FileUtils {
  
  /**
   * Provides logging for the class.
   */
  private static final Logger LOGGER = Logger.getLogger(
                                                     FileUtils.class.getName());
  
  /**
   * Matches the name convention for a support conversation archive.
   */
  private static final Pattern SUPPORT_PATTERN = Pattern.compile(
                                                // Match a pair of names
                                                "MESS_ARCHIVE_([^_]+)_([^\\.]+)"
                                                                );
  
  /**
   * Matches the name of a regular text archive.
   */
  private static final Pattern TEXT_PATTERN = Pattern.compile(
                                                             "MESS_ARCHIVE_(.+)"
                                                             );
  
  /**
   * Allows the opening and saving of files.
   */
  private static final FileChooser FILE_SELECTION_DIALOG = new FileChooser();
  static {
    FILE_SELECTION_DIALOG.getExtensionFilters().addAll(
        new FileChooser.ExtensionFilter("All Supported Files (.fescript, " +
                                        ".txt, .bin, .bin.lz)", "*.fescript",
                                        "*.txt", "*.bin", "*.bin.lz"),
        new FileChooser.ExtensionFilter("Fire Emblem Message Script Files " +
                                        "(.fescript)", "*.fescript"),
        new FileChooser.ExtensionFilter("Text Files (.txt)", "*.txt"),
        new FileChooser.ExtensionFilter("Fire Emblem Message Archives (.bin)",
                                        "*.bin"),
        new FileChooser.ExtensionFilter(
                            "Fire Emblem Compressed Message Archives (.bin.lz)",
                                        "*.bin.lz")
      );
  }
  
  /**
   * Opens a file and loads it into the given tabs.
   * 
   * @param stage the stage to which the open file dialog will belong
   * @param tabs the tabs into which the file's contents should be loaded
   * @return whether or not the operation succeeded
   */
  public static boolean openFile(Window stage, TabPane tabs) {
    FILE_SELECTION_DIALOG.setTitle(XMLManager.getText(
                                                     "File_Open_Conversation"));
    if (Config.getCurrentFile() != null) {
      FILE_SELECTION_DIALOG.setInitialDirectory(Config.getCurrentFile()
                                              .getParentFile());
    }
    File selectedFile = FILE_SELECTION_DIALOG.showOpenDialog(stage);
    if (selectedFile != null) {
      FILE_SELECTION_DIALOG.setInitialDirectory(selectedFile.getParentFile());
      ObservableList<Tab> tabList = tabs.getTabs();
      for (Tab t : tabList) {
        ((MessageTab) t).removeBindings();
      }
      tabs.getTabs().clear();
      Config.setCurrentFile(selectedFile);
      if (selectedFile.getName().endsWith(".bin.lz")) {
        String[] lines = LZUtils.unpackMessageArchiveAsArray(selectedFile,
                                                             false);
        if (lines == null) {
          Alert alert = new Alert(Alert.AlertType.ERROR,
                                  XMLManager.getText("File_Invalid_LZ_Archive"),
                                  XMLManager.getButtonType(ButtonData.OK_DONE));
          alert.showAndWait();
          return false;
        } else if (!lines[0].startsWith("MESS_ARCHIVE")) {
          Alert alert = new Alert(Alert.AlertType.ERROR,
                             XMLManager.getText("File_Invalid_Message_Archive"),
                                  XMLManager.getButtonType(ButtonData.OK_DONE));
          alert.showAndWait();
          return false;
        }
        if (lines[4].contains("MID_支援")) {
          openSupport(tabs, lines);
        } else {
          openText(tabs, lines);
        }
      } else if (selectedFile.getName().endsWith(".bin")) {
        String[] lines = LZUtils.unpackMessageBinAsArray(selectedFile);
        if (lines == null) {
          Alert alert = new Alert(Alert.AlertType.ERROR,
                              XMLManager.getText("File_Invalid_Binary_Archive"),
                                  XMLManager.getButtonType(ButtonData.OK_DONE));
          alert.showAndWait();
          return false;
        } else if (!lines[0].startsWith("MESS_ARCHIVE")) {
          Alert alert = new Alert(Alert.AlertType.ERROR,
                             XMLManager.getText("File_Invalid_Message_Archive"),
                                  XMLManager.getButtonType(ButtonData.OK_DONE));
          alert.showAndWait();
          return false;
        }
        if (lines[4].contains("MID_支援")) {
          openSupport(tabs, lines);
        } else {
          openText(tabs, lines);
        }
      } else {
        try {
          List<String> file = Files.readAllLines(Paths.get(selectedFile
                                                           .getAbsolutePath()));
          String[] lines = new String[file.size()];
          file.toArray(lines);
           if (!lines[0].startsWith("MESS_ARCHIVE")) {
            Alert alert = new Alert(Alert.AlertType.ERROR,
                             XMLManager.getText("File_Invalid_Message_Archive"),
                                  XMLManager.getButtonType(ButtonData.OK_DONE));
            alert.showAndWait();
            return false;
          }
          if (lines[6].contains("MID_支援")) {
            openSupport(tabs, lines);
          } else {
            openText(tabs, lines);
          }
        } catch (IOException ex) {
          LOGGER.log(Level.SEVERE, XMLManager.getText("File_Load_Failed") +
                                   " \"" + selectedFile + "\".", ex);
          return false;
        }
      }
    }
    return true;
  }
  
  /**
   * Opens a file and loads it into the given tabs.
   * 
   * @param selectedFile the file to open
   * @param tabs the tabs into which the file's contents should be loaded
   * @return whether or not the operation succeeded
   */
  public static boolean openFile(File selectedFile, TabPane tabs) {
    if (selectedFile != null) {
      FILE_SELECTION_DIALOG.setInitialDirectory(selectedFile.getParentFile());
      ObservableList<Tab> tabList = tabs.getTabs();
      for (Tab t : tabList) {
        ((MessageTab) t).removeBindings();
      }
      tabs.getTabs().clear();
      Config.setCurrentFile(selectedFile);
      if (selectedFile.getName().endsWith(".bin.lz")) {
        String[] lines = LZUtils.unpackMessageArchiveAsArray(selectedFile,
                                                             false);
        if (lines == null) {
          Alert alert = new Alert(Alert.AlertType.ERROR,
                                  XMLManager.getText("File_Invalid_LZ_Archive"),
                                  XMLManager.getButtonType(ButtonData.OK_DONE));
          alert.showAndWait();
          return false;
        } else if (!lines[0].startsWith("MESS_ARCHIVE")) {
          Alert alert = new Alert(Alert.AlertType.ERROR,
                             XMLManager.getText("File_Invalid_Message_Archive"),
                                  XMLManager.getButtonType(ButtonData.OK_DONE));
          alert.showAndWait();
          return false;
        }
        if (lines[4].contains("MID_支援")) {
          openSupport(tabs, lines);
        } else {
          openText(tabs, lines);
        }
      } else if (selectedFile.getName().endsWith(".bin")) {
        String[] lines = LZUtils.unpackMessageBinAsArray(selectedFile);
        if (lines == null) {
          Alert alert = new Alert(Alert.AlertType.ERROR,
                              XMLManager.getText("File_Invalid_Binary_Archive"),
                                  XMLManager.getButtonType(ButtonData.OK_DONE));
          alert.showAndWait();
          return false;
        } else if (!lines[0].startsWith("MESS_ARCHIVE")) {
          Alert alert = new Alert(Alert.AlertType.ERROR,
                             XMLManager.getText("File_Invalid_Message_Archive"),
                                  XMLManager.getButtonType(ButtonData.OK_DONE));
          alert.showAndWait();
          return false;
        }
        if (lines[4].contains("MID_支援")) {
          openSupport(tabs, lines);
        } else {
          openText(tabs, lines);
        }
      } else if (selectedFile.getName().endsWith(".fescript")) {
        try {
          List<String> file = Files.readAllLines(
                                     Paths.get(selectedFile.getAbsolutePath()));
          String[] lines = new String[file.size()];
          file.toArray(lines);
           if (!lines[0].startsWith("MESS_ARCHIVE")) {
            Alert alert = new Alert(Alert.AlertType.ERROR,
                               XMLManager.getText("File_Invalid_Fescript_File"),
                                  XMLManager.getButtonType(ButtonData.OK_DONE));
            alert.showAndWait();
            return false;
          }
          if (lines[6].contains("MID_支援")) {
            openSupport(tabs, lines);
          } else {
            openText(tabs, lines);
          }
        } catch (IOException ex) {
          LOGGER.log(Level.SEVERE, XMLManager.getText("File_Load_Failed") +
                                   " \"" + selectedFile + "\".", ex);
          return false;
        }
      } else {
        try {
          List<String> file = Files.readAllLines(
                                     Paths.get(selectedFile.getAbsolutePath()));
          String[] lines = new String[file.size()];
          file.toArray(lines);
           if (!lines[0].startsWith("MESS_ARCHIVE")) {
            Alert alert = new Alert(Alert.AlertType.ERROR,
                             XMLManager.getText("File_Invalid_Message_Archive"),
                                  XMLManager.getButtonType(ButtonData.OK_DONE));
            alert.showAndWait();
            return false;
          }
          if (lines[6].contains("MID_支援")) {
            openSupport(tabs, lines);
          } else {
            openText(tabs, lines);
          }
        } catch (IOException ex) {
          LOGGER.log(Level.SEVERE, XMLManager.getText("File_Load_Failed") +
                                   " \"" + selectedFile + "\".", ex);
          return false;
        }
      }
    }
    return true;
  }
  
  /**
   * Reads a support conversation and loads it into the given tabs.
   * 
   * @param tabs the tabs into which the conversation should be loaded
   * @param lines the lines of the message archive containing the support
   */
  private static void openSupport(TabPane tabs, String[] lines) {
    Config.closeCurrentConversation();
    Matcher support = SUPPORT_PATTERN.matcher(lines[0]);
    String type;
    Matcher match;
    String key;
    String text;
    StringProperty content;
    SupportConversation file;
    if (support.find()) {
      type = XMLManager.getText("Support_Conversation_Type_Normal");
      if (lines[0].contains("兄弟")) {
        type = XMLManager.getText("Support_Conversation_Type_Siblings");
      } else if (lines[0].contains("親子")) {
        type = XMLManager.getText("Support_Conversation_Type_Parent");
      }
      file = new SupportConversation(support.group(1), support.group(2), type);
    } else {
      return;
    }
    for (String line : lines) {
      if (line.startsWith("MID_支援")) {
        text = line.substring(line.indexOf(": ") + 2);
        if (text.startsWith("[" + XMLManager.getText(
                                                    "Script_Command_Script")) ||
            text.startsWith("[" + XMLManager.getText(
                                          "Script_Command_Formatted_Script"))) {
          text = text.replaceAll(Pattern.quote("\\/"), "\n");
        }
        key = line.substring(7, line.indexOf(": "));
        content = new SimpleStringProperty();
        tabs.getTabs().add(new MessageTab(key, text, content));
        file.getMessages().add(new FireEmblemMessage(key, content));
      }
    }
    Config.setCurrentConversation(file);
  }
  
  /**
   * Reads a set of messages and loads them into the given tabs.
   * 
   * @param tabs the tabs into which the messages should be loaded
   * @param lines the lines of the message archive containing the messages
   */
  private static void openText(TabPane tabs, String[] lines) {
    Config.closeCurrentConversation();
    Matcher name = TEXT_PATTERN.matcher(lines[0]);
    String text;
    name.find();
    FireEmblemTextFile file = new FireEmblemTextFile(name.group(1));
    String key;
    String messageKey = "MID_" + name.group(1);
    int keyLength = messageKey.length() + 1;
    StringProperty content;
    for (String line : lines) {
      if (line.startsWith(messageKey)) {
        text = line.substring(line.indexOf(": ") + 2);
        if (text.startsWith("[" + XMLManager.getText(
                                                    "Script_Command_Script")) ||
            text.startsWith("[" + XMLManager.getText(
                                          "Script_Command_Formatted_Script"))) {
          text = text.replaceAll(Pattern.quote("\\/"), "\n");
        }
        key = line.substring(keyLength, line.indexOf(": "));
        content = new SimpleStringProperty();
        tabs.getTabs().add(new MessageTab(key, text, content));
        file.getMessages().add(new FireEmblemMessage(key, content));
      }
    }
    if (tabs.getTabs().size() > 0) {
      tabs.getTabs().get(0).setDisable(false);
    }
    Config.setCurrentConversation(file);
  }
  
  /**
   * Saves the current conversation to the opened file, prompting for a location
   * if it is not set.
   * 
   * @param stage the stage to which the save file dialog will belong
   */
  public static void saveFile(Window stage) {
    if (Config.getCurrentConversation() == null) {
      return;
    }
    if (Config.getCurrentFile() == null) {
      FILE_SELECTION_DIALOG.setTitle(XMLManager.getText(
                                                     "File_Save_Conversation"));
      FILE_SELECTION_DIALOG.setInitialFileName(Config.getCurrentConversation()
                                             .getName() + ".bin.lz");
      Config.setCurrentFile(FILE_SELECTION_DIALOG.showSaveDialog(stage));
      if (Config.getCurrentFile() == null) {
        return;
      }
    }
    FILE_SELECTION_DIALOG.setInitialDirectory(
                                       Config.getCurrentFile().getParentFile());
    if (Config.getCurrentFile().getName().endsWith(".bin.lz")) {
      LZUtils.repackMessageArchive(Config.getCurrentFile(),
                                 Config.getCurrentConversation().toBinOutput());
    } else if (Config.getCurrentFile().getName().endsWith(".bin")) {
      LZUtils.repackMessageBin(Config.getCurrentFile(),
                               Config.getCurrentConversation().toBinOutput());
    } else {
      try {
        List<String> output;
        if (Config.getCurrentFile().getName().endsWith(".fescript")) {
          output = Config.getCurrentConversation().toScriptOutput();
        } else {
          output = Config.getCurrentConversation().toTextOutput();
        }
        Files.write(Paths.get(Config.getCurrentFile().getAbsolutePath()),
                    output);
      } catch (IOException ex) {
        LOGGER.log(Level.SEVERE, XMLManager.getText("File_Save_Failed") +
                                 " \"" + Config.getCurrentFile() + "\".", ex);
      }
    }
  }
  
  /**
   * Saves the current conversation to the selected file.
   * 
   * @param stage the stage to which the save file dialog will belong
   */
  public static void saveFileAs(Window stage) {
    if (Config.getCurrentConversation() == null) {
      return;
    }
    FILE_SELECTION_DIALOG.setTitle(XMLManager.getText(
                                                  "File_Save_Conversation_As"));
    if (Config.getCurrentFile() != null) {
      FILE_SELECTION_DIALOG.setInitialFileName(Config.getCurrentFile()
                                               .getName());
    } else {
      FILE_SELECTION_DIALOG.setInitialFileName(Config.getCurrentConversation()
                                             .getName() + ".bin.lz");
    }
    Config.setCurrentFile(FILE_SELECTION_DIALOG.showSaveDialog(stage));
    if (Config.getCurrentFile() == null) {
      return;
    }
    saveFile(stage);
  }
}
