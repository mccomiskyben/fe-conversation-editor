/*
 * Copyright (C) 2017 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.util.text;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import sahara.cactus.fireemblem.conversation.manager.CharacterManager;
import sahara.cactus.fireemblem.conversation.manager.Config;
import sahara.cactus.fireemblem.conversation.manager.XMLManager;

/**
 * Converts a script to game text.
 * <p>
 * If the text does not have a script header, the text will simply be returned
 * as-is.
 * 
 * @author Secretive Cactus
 */
public class ScriptToTextConverter {
  
  /**
   * Matches a script command to play a BGM.
   * <p>
   * Format: \(BGM: (Name){, (Offset)}?\)
   */
  private static Pattern SCRIPT_BGM_PATTERN;
  
  /**
   * Matches a script command to play a sound effect. Actual command varies
   * based on the current editor language.
   * <p>
   * Format: \(SOUND EFFECT: (Name)\)
   */
  private static Pattern SCRIPT_SOUND_EFFECT_PATTERN;
  
  /**
   * Matches a script command to change a character's emotions. Actual command
   * varies based on the current editor language.
   * <p>
   * Format: \(EMOTION SHIFT: [(Name), ]?(Emotions)\)
   * Emotions Format: (Emotion), {(Modifier){, (Modifier)}?}?
   */
  private static Pattern SCRIPT_EMOTION_PATTERN;
  
  /**
   * Matches a script command to display a character portrait. May contain
   * instructions to begin with a specific emotional expression. Actual command
   * varies based on the current editor language.
   * <p>
   * Format: \(ENTER (Direction): (Name){, (Emotions)}?\)
   * <p>
   * Emotions Format: (Emotion), {(Modifier){, (Modifier)}?}?
   */
  private static Pattern SCRIPT_ENTER_PATTERN;
  
  /**
   * Matches a script command to remove a character portrait. Actual command
   * varies based on the current editor language.
   * <p>
   * Format: \(EXIT{: (Name)}?\)
   */
  private static Pattern SCRIPT_EXIT_PATTERN;
  
  /**
   * Matches a script command to set the text color. Actual command varies
   * based on the current editor language.
   * <p>
   * Format: \(TEXT COLOR: (Red), (Green), (Blue), (Alpha)\)
   */
  private static Pattern SCRIPT_TEXT_COLOR_PATTERN;
  /**
   * Matches a script command to display text. May contain instructions to use a
   * specific emotional expression and play a sound effect.
   * <p>
   * Format: (Name){ [(Sound Effect)]}?{ \((Emotions)\)}?: Text
   * <p>
   * Emotions Format: (Emotion), {(Modifier){, (Modifier)}?}?
   */
  private static final Pattern SCRIPT_SPEAKING_PATTERN = Pattern.compile(
                                                          // Match Name
                                                       "^([^:\\[\\(\\{\r\n]+)" +
                                                    "(?:\\s+\\{([^\\}]+)\\})?" +
                                                    // Match Sound Effect
                                                    "(?:\\s+\\[([\\w#]+)\\])?" +
                                                    "(?:\\s+\\{([^\\}]+)\\})?" +
                                         // Match Emotions
                                         "(?:\\s+\\(([^,\\)]+)([^\\)]+)?\\))?" +
                                                                   // Match Text
                                                                     ":\\s*(.+)"
                                                  );
  
  /**
   * Matches a script command to use a specific display type. Actual command
   * varies based on the current editor language.
   * <p>
   * Format: \(Type: (Type)\)
   */
  private static Pattern SCRIPT_TYPE_PATTERN;
  
  /**
   * Matches a script command to allow the use of $Nu. Fates only. Actual
   * command varies based on the current editor language.
   * <p>
   * Format: \(HAS PERMANENTS\)
   */
  private static Pattern SCRIPT_PERMANENTS_PATTERN;
  
  /**
   * Contains substitutions for pieces of text known to be problematic.
   */
  private static final SubstitutionPair[] SCRIPT_SUBSTITUTIONS =
                                                  new SubstitutionPair[] {
             // Matches {\n|$p}$Sbs{\d+}|$k and replaces it with $k{$1}$Sbs{$2}|
                         new SubstitutionPair("(\\\\n|\\$p)\\$Sbs(\\d+)\\|\\$k",
                                              "\\$k$1\\$Sbs$2|"),
                                      // Matches \n$b and replaces it with $b
                                      new SubstitutionPair("\\\\n\\$b", "\\$b"),
                                 // Matches an ending that would already have
                                 // been paginated.
                                new SubstitutionPair("\\$w0\\|\\$k$", "\\$w0|"),
           // Matches $Fw{\d+}|$k\n$Ws{[^|]+}|$Wd
           // and replaces it with $Fw{$1}|$Ws{$2}|$Wd
           new SubstitutionPair("\\$Fw(\\d+)\\|\\$k\\\\n\\$Ws([^\\|]+)\\|\\$Wd", 
                                "\\$Fw$1\\|\\$Ws$2\\|\\$Wd")
                                                  };
  
  /**
   * Whether or not the converter is finished running.
   */
  private boolean finished;
  
  /**
   * Whether or not the next BGM pattern will be the first one.
   */
  private boolean firstBGM;
  
  /**
   * Whether or not the next entrance pattern will be the first one.
   */
  private boolean firstEntrance;
  
  /**
   * Whether or not the script is formatted.
   */
  private boolean formatted;
  
  /**
   * The current processing index.
   */
  private int index;
  
  /**
   * The name of the last character to have been used in a script command.
   */
  private String lastChar;
  
  /**
   * The current line to process.
   */
  private String line;
  
  /**
   * Contains the script's lines.
   */
  private String[] lines;
  
  /**
   * The most recent line match.
   */
  private Matcher match;
  
  /**
   * Whether or not the output needs a new page.
   */
  private boolean needsNewPage;
  
  /**
   * Contains the processed output.
   */
  private StringBuilder output;
  
  /**
   * Whether or not the next request for a new page should be overridden.
   */
  private boolean overrideNewPage;
  
  /**
   * The game text produced by processing the script.
   */
  private String processedScript;
  
  /**
   * Whether or not the next line break should be skipped.
   */
  private boolean skipLine;
  
  /**
   * The display type of the script.
   */
  private int type;

  /**
   * Creates a script converter for the given script.
   * 
   * @param script the script that the converter will convert
   */
  public ScriptToTextConverter(String script) {
    // Return the text if it lacks a script header.
    if (!script.startsWith("[" + XMLManager.getText("Script_Command_Script")) &&
        !script.startsWith("[" +XMLManager.getText(
                                          "Script_Command_Formatted_Script"))) {
      processedScript = script;
      finished = true;
    } else {
      finished = false;
      lines = script.split("\n");
      index = 1;
      output = new StringBuilder(script.length() + 100);
      needsNewPage = false;
      overrideNewPage = false;
      firstEntrance = true;
      firstBGM = true;
      skipLine = false;
      type = 1;
      lastChar = null;
      if (lines[0].startsWith("[" + XMLManager.getText(
                                          "Script_Command_Formatted_Script"))) {
        // A formatted script will not receive line wrapping.
        formatted = true;
      } else {
        formatted = false;
      }
    }
  }
  
  /**
   * Returns whether or not the next BGM pattern will be the first.
   * 
   * @return whether or not the next BGM pattern will be the first
   */
  public boolean getFirstBGM() {
    return firstBGM;
  }
  
  /**
   * Returns whether or not the next entrance pattern will be the first.
   * 
   * @return whether or not the next entrance pattern will be the first
   */
  public boolean getFirstEntrance() {
    return firstEntrance;
  }
  
  /**
   * Returns the current processing index.
   * 
   * @return the current processing index
   */
  public int getIndex() {
    return index;
  }
  
  /**
   * Returns the name of the last character to have been used in a script
   * command
   * 
   * @return the name of the last character to have been used in a script
   * command
   */
  public String getLastChar() {
    return lastChar;
  }
  
  /**
   * Returns the current line to process.
   * 
   * @return the current line to process
   */
  public String getLine() {
    return line;
  }
  
  /**
   * Returns the script's lines.
   * 
   * @return the script's lines
   */
  public String[] getLines() {
    return lines;
  }
  
  /**
   * Returns the most recent line match.
   * 
   * @return the most recent line match
   */
  public Matcher getMatch() {
    return match;
  }
  
  /**
   * Returns the StringBuilder containing the processed output.
   * 
   * @return the StringBuilder containing the processed output
   */
  public StringBuilder getOutput() {
    return output;
  }
  
  /**
   * Returns the game text produced by processing the script.
   * 
   * @return the game text produced by processing the script
   */
  public String getProcessedScript() {
    return processedScript;
  }
  
  /**
   * Returns the display type of the script.
   * 
   * @return the display type of the script
   */
  public int getType() {
    return type;
  }
  
  /**
   * Returns whether or not the converter has finished processing.
   * 
   * @return whether or not the converter has finished processing
   */
  public boolean isFinished() {
    return finished;
  }
  
  /**
   * Returns whether or not the script has been converted.
   * 
   * @return whether or not the script has been converted
   */
  public boolean isFormatted() {
    return formatted;
  }
  
  /**
   * Returns whether or not the script needs a new page.
   * 
   * @return whether or not the script needs a new page
   */
  public boolean needsNewPage() {
    return needsNewPage;
  }
  
  /**
   * Returns whether or not the next request for a new page should be
   * overridden.
   * 
   * @return whether or not the next request for a new page should be
   * overridden
   */
  public boolean overrideNewPage() {
    return overrideNewPage;
  }

  /**
   * Parses the script line-by-line, delegating the actual processing to
   * specialized methods.
   */
  private void parseLine() {
    // We swallow this if the game is Awakening, but in Fates we need to tell
    // the game that we intend to use the $Nu control code.
    if (SCRIPT_PERMANENTS_PATTERN.matcher(line).find()) {
      processPermanents();
    // Determines what kind of display the game will use.
    } else if ((match = SCRIPT_TYPE_PATTERN.matcher(line)).find()) {
      processType();
    // Displays a character portrait.
    } else if ((match = SCRIPT_ENTER_PATTERN.matcher(line)).find()) {
      processEntrance();
    // Removes a character portrait from the screen.
    } else if ((match = SCRIPT_EXIT_PATTERN.matcher(line)).find()) {
      processExit();
    // Changes the expression on a character portrait.
    } else if ((match = SCRIPT_EMOTION_PATTERN.matcher(line)).find()) {
      processEmotion();
    // Displays some text in the text box.
    } else if ((match = SCRIPT_SPEAKING_PATTERN.matcher(line)).find()) {
      processSpeech();
    // Changes the playing BGM.
    } else if ((match = SCRIPT_BGM_PATTERN.matcher(line)).find()) {
      processBGM();
    // Plays a sound effect.
    } else if ((match = SCRIPT_SOUND_EFFECT_PATTERN.matcher(line)).find()) {
      processSoundEffect();
    // Changes the text color.
    } else if ((match = SCRIPT_TEXT_COLOR_PATTERN.matcher(line)).find()) {
      processTextColor();
    // Directly adds the contents, potentially paginating.
    } else {
      processText();
    }
  }

  /**
   * Processes the BGM script command.
   */
  private void processBGM() {
    if (!overrideNewPage && !firstBGM) {
      output.append("$k$p");
      overrideNewPage = true;
    } else {
      firstBGM = false;
    }
    output.append(line.substring(0, line.indexOf("(")));
    output.append("$Sbp").append(match.group(1)).append("|");
    if (match.group(2) != null) {
      output.append(match.group(2)).append("|");
    } else {
      output.append("0|");
    }
  }

  /**
   * Processes the Emotion Shift script command.
   */
  private void processEmotion() {
    // We don't want to add a new page if we're in the middle of a modifier
    // sequence.
    if (!overrideNewPage && output.charAt(output.length() - 1) != '|') {
      output.append("$k$p");
      overrideNewPage = true;
    }
    output.append(line.substring(0, line.indexOf("(")));
    // If the first parameter is an emotion rather than a character, we
    // simply request the emotion and the game will apply it to the last
    // character used.
    if (ScriptHelper.EMOTION_TR_AS_JA.get(match.group(1)) != null) {
      overrideNewPage = false;
      needsNewPage = false;
      skipLine = true;
      output.append("$E").append(ScriptHelper.
                                          EMOTION_TR_AS_JA.get(match.group(1)));
      output.append(",");
      if (match.group(2) != null) {
        if (match.group(2).contains(XMLManager.getText("Emotion_Blushing"))) {
          output.append("照");
        }
        if (match.group(2).contains(XMLManager.getText("Emotion_Sweat"))) {
          output.append("汗");
        }
      }
      output.append("|");
    // If the first parameter is a character, we want to specifically
    // request that the emotion be applied to that character.
    } else {
      if (lastChar != null && !lastChar.equals(match.group(1))) {
        output.append("$Ws");
        output.append(CharacterManager.getJapaneseName(match.group(1)));
        output.append("|");
      }
      if (match.group(2) != null) {
        output.append("$Ws");
        output.append(CharacterManager.getJapaneseName(match.group(1)));
        output.append("|$Wa$E");
        output.append(ScriptHelper.EMOTION_TR_AS_JA.get(match.group(2)));
        if (match.group(3) != null) {
          output.append(",");
          if (match.group(3).contains(XMLManager.getText("Emotion_Blushing"))) {
            output.append("照");
          }
        if (match.group(3).contains(XMLManager.getText("Emotion_Sweat"))) {
            output.append("汗");
          }
        }
        output.append("|");
      }
    }
  }

  /**
   * Processes the Enter script command.
   */
  private void processEntrance() {
    // We only need a new page if this isn't the first entrance and the last
    // script command didn't provide a fresh page.
    if (!firstEntrance && !overrideNewPage) {
      output.append("$k\\n");
      // In this case, the command itself provides a fresh page.
      overrideNewPage = true;
    // The first entrance always defaults to a fresh page.
    } else if (firstEntrance) {
      firstEntrance = false;
      overrideNewPage = true;
    }
    output.append(line.substring(0, line.indexOf("(")));
    output.append("$Wm");
    output.append(CharacterManager.getJapaneseName(match.group(2)));
    output.append("|");
    if (type == 1) {
      if (match.group(1).equals(XMLManager.getText(
                                                "Script_Command_Enter_Left")) ||
          match.group(1).equalsIgnoreCase(XMLManager.getText(
                                                 "Script_Command_Enter_Top"))) {
        output.append("3$w0|");
      } else {
        output.append("7$w0|");
      }
    } else {
      if (match.group(1).equals(XMLManager.getText(
                                                "Script_Command_Enter_Left")) ||
          match.group(1).equalsIgnoreCase(XMLManager.getText(
                                                 "Script_Command_Enter_Top"))) {
        output.append("0$w0|");
      } else {
        output.append("6$w0|");
      }
    }
    // If the entrance contains a starting emotion, we need to handle that.
    if (match.group(3) != null) {
      output.append("$Ws");
      output.append(CharacterManager.getJapaneseName(match.group(2)));
      output.append("|$Wa$E").append(ScriptHelper.
                                          EMOTION_TR_AS_JA.get(match.group(3)));
      if (match.group(4) != null) {
        output.append(",");
        if (match.group(4).contains(XMLManager.getText("Emotion_Blushing"))) {
          output.append("照");
        }
        if (match.group(4).contains(XMLManager.getText("Emotion_Sweat"))) {
          output.append("汗");
        }
      }
      output.append("|");
    }
  }

  /**
   * Processes the Exit script command.
   */
  private void processExit() {
    // If no name is listed, we shouldn't try to apply it. The last used
    // character will automatically be removed by the game.
    if (match.group(1) == null) {
      if (!overrideNewPage) {
        output.append("$k\\n");
        overrideNewPage = true;
      }
      output.append(line.substring(0, line.indexOf("(")));
    // If a name is listed, we specifically request that character as the
    // target for removal.
    } else {
      if (!overrideNewPage) {
        output.append("$k\\n");
        overrideNewPage = true;
      }
      output.append(line.substring(0, line.indexOf("(")));
      output.append("$Ws");
      output.append(CharacterManager.getJapaneseName(match.group(1)));
      output.append("|");
    }
    output.append("$Wd$w0|");
  }

  /**
   * Processes the Has Permanents script command.
   */
  private void processPermanents() {
    // Run only in Fates mode.
    if (Config.getGameMode().isFates()) {
      output.append(line.substring(0, line.indexOf("(")));
      output.append("$a");
    }
  }

  /**
   * Processes actual dialog.
   */
  private void processSpeech() {
    if (CharacterManager
          .getJapaneseName(match.group(1)).equals(match.group(1)) &&
        !CharacterManager.getNames().containsKey(match.group(1))) {
      if (needsNewPage) {
        output.append("$k$p").append(line);
        needsNewPage = false;
      } else {
        if (!skipLine) {
          if (formatted) {
            output.append("\\n").append(line);
          } else {
            output.append("\n").append(line);
          }
          needsNewPage = true;
        } else {
          output.append(line);
          skipLine = false;
        }
      }
      return;
    }
    if (!overrideNewPage && !skipLine) {
      if (match.group(1).equals(lastChar)) {
        output.append("$k$p");
      } else {
        output.append("$k\\n");
      }
    } else {
      overrideNewPage = false;
      skipLine = false;
    }
    needsNewPage = false;
    // If we need to apply modifiers or specify a different speaker, we'll
    // have some extended processing. If not, we simply attach the text.
    if (match.group(2) != null || match.group(3) != null || 
        match.group(5) != null || !match.group(1).equals(lastChar)) {
      lastChar = match.group(1);
      output.append("$Ws");
      output.append(CharacterManager.getJapaneseName(match.group(1)));
      output.append("|");
      if (match.group(2) != null) {
        output.append(match.group(2));
      }
      output.append("$Wa");
      if (match.group(3) != null) {
        output.append("$Svp").append(match.group(3)).append("|");
      }
      if (match.group(4) != null) {
        output.append(match.group(4));
      }
      if (match.group(5) != null) {
        output.append("$E").append(ScriptHelper.
                                          EMOTION_TR_AS_JA.get(match.group(5)));
        if (match.group(6) != null) {
          output.append(",");
          if (match.group(6).contains(XMLManager.getText("Emotion_Blushing"))) {
            output.append("照");
          }
          if (match.group(6).contains(XMLManager.getText("Emotion_Sweat"))) {
            output.append("汗");
          }
        }
        output.append("|");
      }
    }
    output.append(match.group(7));
  }

  /**
   * Processes the Sound Effect script command.
   */
  private void processSoundEffect() {
    if (!overrideNewPage && output.charAt(output.length() - 1) != '|') {
      output.append("$k$p");
    }
    overrideNewPage = false;
    needsNewPage = false;
    skipLine = true;
    output.append(line.substring(0, line.indexOf("(")));
    output.append("$Svp").append(match.group(1)).append("|");
  }

  /**
   * Processes a standard line of text.
   */
  private void processText() {
    if (needsNewPage) {
      output.append("$k$p").append(line);
      needsNewPage = false;
    } else {
      if (!skipLine) {
        if (formatted) {
          output.append("\\n").append(line);
        } else {
          output.append("\n").append(line);
        }
        needsNewPage = true;
      } else {
        output.append(line);
        skipLine = false;
      }
    }
  }

  /**
   * Processes the Text Color script command.
   */
  private void processTextColor() {
    output.append("$c");
    output.append(match.group(1)).append(",");
    output.append(match.group(2)).append(",");
    output.append(match.group(3)).append(",");
    output.append(match.group(4)).append("|");
  }

  /**
   * Processes the Type script command.
   */
  private void processType() {
    output.append(line.substring(0, line.indexOf("(")));
    if (match.group(1).equals(XMLManager.getText("Script_Command_Type_Zero"))) {
      type = 0;
      output.append("$t0");
    } else {
      type = 1;
      output.append("$t1");
    }
  }
  
  /**
   * Reloads the patterns use by the Script Converter for the current language.
   */
  public static void reloadPatterns() {
    SCRIPT_BGM_PATTERN = Pattern.compile("\\(" +
                                      XMLManager.getText("Script_Command_BGM") +
                                                                 // Match Name
                                                                 ":\\s*(\\w+)" +
                                                         // Match Offset
                                                         "(?:,\\s*(\\d+))?\\)");
    SCRIPT_SOUND_EFFECT_PATTERN = Pattern.compile(
                     "\\(" + XMLManager.getText("Script_Command_Sound_Effect") +
                                                           // Match Name
                                                           ":\\s*([\\w#]+)\\)");
    SCRIPT_EMOTION_PATTERN = Pattern.compile("\\(" + 
                            XMLManager.getText("Script_Command_Emotion_Shift") +
                                                // Match Name or primary Emotion
                                                             ":\\s*([^,\\)]+)" +
                                           // Match Emotions or Modifiers
                                           "(?:,\\s*([^,\\)]+)([^\\)]+)?)?\\)");
    SCRIPT_ENTER_PATTERN = Pattern.compile("\\(" + 
                                    XMLManager.getText("Script_Command_Enter") + 
                                                   // Match Direction
                    "\\s+(" + XMLManager.getText("Script_Command_Enter_Right") +
                         "|" + XMLManager.getText("Script_Command_Enter_Left") +
                          "|" + XMLManager.getText("Script_Command_Enter_Top") +
                       "|" + XMLManager.getText("Script_Command_Enter_Bottom") +
                                                              // Match Name
                                                            "):\\s*([^,\\)]+)" + 
                                           // Match Emotions
                                           "(?:,\\s*([^,\\)]+)([^\\)]+)?)?\\)");
    SCRIPT_EXIT_PATTERN = Pattern.compile("\\(" +
                                     XMLManager.getText("Script_Command_Exit") +
                                                      // Match Name
                                                      "(?::\\s*([^\\)]+))?\\)");
    SCRIPT_TEXT_COLOR_PATTERN = Pattern.compile("\\(" +
                               XMLManager.getText("Script_Command_Text_Color") +
                             // Match Color RGBA
                             ":\\s*(\\d+),\\s*(\\d+),\\s*(\\d+),\\s*(\\d+)\\)");
    SCRIPT_TYPE_PATTERN = Pattern.compile("\\(" +
                          XMLManager.getText("Script_Command_Type") + ":\\s*(" +
                          // Match Type
                          XMLManager.getText("Script_Command_Type_Zero") + "|" +
                        XMLManager.getText("Script_Command_Type_One") + ")\\)");
    SCRIPT_PERMANENTS_PATTERN = Pattern.compile("\\(" +
                           XMLManager.getText("Script_Command_Has_Permanents") +
                                                "\\)");
  }

  /**
   * Runs the script converter.
   * 
   * @return the script converted into game text
   */
  public String run() {
    if (!finished) {
      while (index < lines.length) {
        line = lines[index];
        parseLine();
        index++;
      }
      finished = true;
      // Add an input request with no following content to prevent the last
      // line of text from flashing by.
      output.append("$k");
      // Convert the script to a string and perform substitutions on known
      // pieces of problematic text.
      processedScript = output.toString();
      for (SubstitutionPair s : SCRIPT_SUBSTITUTIONS) {
        processedScript = s.replaceAll(processedScript);
      }
    }
    return processedScript;
  }
  
  /**
   * Returns whether or not the next line break should be skipped.
   * 
   * @return whether or not the next line break should be skipped
   */
  public boolean skipLine() {
    return skipLine;
  }
  
  /**
   * Sets whether or not the next BGM pattern will be the first one
   * 
   * @param value whether or not the next BGM pattern will be the first one
   */
  public void setFirstBGM(boolean value) {
    firstBGM = value;
  }
  
  /**
   * Sets whether or not the next entrance pattern will be the first one.
   * 
   * @param value whether or not the next entrance pattern will be the first one
   */
  public void setFirstEntrance(boolean value) {
    firstEntrance = value;
  }
  
  /**
   * Sets whether or not the script is formatted.
   * 
   * @param value whether or not the script is formatted
   */
  public void setFormatted(boolean value) {
    formatted = value;
  }
  
  /**
   * Returns the current processing index.
   * 
   * @param value the current processing index
   */
  public void setIndex(int value) {
    index = value;
  }
  
  /**
   * Returns the name of the last character to have been used in a script
   * command.
   * 
   * @param value the name of the last character to have been used in a script
   * command
   */
  public void setLastChar(String value) {
    lastChar = value;
  }
  
  /**
   * Sets whether or not the next request for a new page should be overridden
   * 
   * @param value whether or not the next request for a new page should be
   * overridden
   */
  public void setOverrideNewPage(boolean value) {
    overrideNewPage = value;
  }
  
  /**
   * Returns whether or not the output needs a new page.
   * 
   * @param value whether or not the output needs a new page
   */
  public void setNeedsNewPage(boolean value) {
    needsNewPage = value;
  }
  
  /**
   * Returns whether or not the next line break should be skipped.
   * 
   * @param value whether or not the next line break should be skipped
   */
  public void setSkipLine(boolean value) {
    skipLine = value;
  }
}
