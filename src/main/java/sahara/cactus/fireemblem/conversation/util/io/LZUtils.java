/*
 * Copyright (C) 2016 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.util.io;

import com.github.einstein95.dsdecmp.HexInputStream;
import com.github.einstein95.dsdecmp.JavaDSDecmp;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class contains a Java conversion of SciresM's message archive-related
 * methods from FEAT. It is used to unpack and repack Fire Emblem message
 * archives.
 * 
 * @author SciresM
 * @author Secretive Cactus
 */
public final class LZUtils {
  
  /**
   * Provides logging for the class.
   */
  private static final Logger LOGGER = Logger.getLogger(
                                                       LZUtils.class.getName());
  
  /**
   * Unpacks a Fire Emblem message archive and returns its content as a String.
   * 
   * @param file the file to unpack
   * @param save whether or not intermediate stages (.bin, .txt) should be saved
   * @return the unpacked content of the file
   */
  public static String unpackMessageArchive(File file, boolean save) {
    StringBuilder result = new StringBuilder();
    HexInputStream hexStream = null;
    int[] output;
    try {
      byte[] bytes = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
      if (bytes[0] == 0x13) {
        byte[] tmp = new byte[bytes.length - 4];
        System.arraycopy(bytes, 4, tmp, 0, tmp.length);
        bytes = tmp;
      }
      hexStream = new HexInputStream(new ByteArrayInputStream(bytes));
      output = JavaDSDecmp.Decompress(hexStream);
      bytes = new byte[output.length];
      for (int i = 0; i < output.length; i++) {
        bytes[i] = (byte) output[i];
      }
      String path = file.getAbsolutePath().replace(".lz", "");
      String[] lines;
      if (save) {
        Files.write(Paths.get(path), bytes);
        lines = extractMessageArchive(path.replace(".bin", ".txt"), bytes);
      } else {
        lines = extractMessageArchive(bytes);
      }
      for (String line : lines) {
        result.append(line).append(System.lineSeparator());
      }
    } catch (IOException ex) {
      LOGGER.log(Level.SEVERE, ex.toString(), ex);
    } finally {
      try {
        if (hexStream != null) {
          hexStream.close();
        }
      } catch (IOException ex) {
        LOGGER.log(Level.SEVERE, ex.toString(), ex);
      }
    }
    return result.toString();
  }
  
  /**
   * Unpacks a Fire Emblem message archive and returns its content as an array
   * of Strings.
   * 
   * @param file the file to unpack
   * @param save whether or not intermediate stages (.bin, .txt) should be saved
   * @return the unpacked content of the file
   */
  public static String[] unpackMessageArchiveAsArray(File file, boolean save) {
    HexInputStream hexStream = null;
    int[] output;
    try {
      byte[] bytes = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
      if (bytes[0] == 0x13) {
        byte[] tmp = new byte[bytes.length - 4];
        System.arraycopy(bytes, 4, tmp, 0, tmp.length);
        bytes = tmp;
      }
      hexStream = new HexInputStream(new ByteArrayInputStream(bytes));
      output = JavaDSDecmp.Decompress(hexStream);
      if (output != null) {
        bytes = new byte[output.length];
        for (int i = 0; i < output.length; i++) {
          bytes[i] = (byte) output[i];
        }
        String path = file.getAbsolutePath().replace(".lz", "");
        String[] lines;
        if (save) {
          Files.write(Paths.get(path), bytes);
          lines = extractMessageArchive(path.replace(".bin", ".txt"), bytes);
        } else {
          lines = extractMessageArchive(bytes);
        }
        return lines;
      } else {
        return null;
      }
    } catch (IOException ex) {
      LOGGER.log(Level.SEVERE, ex.toString(), ex);
    } finally {
      try {
        if (hexStream != null) {
          hexStream.close();
        }
      } catch (IOException ex) {
        LOGGER.log(Level.SEVERE, ex.toString(), ex);
      }
    }
    return new String[0];
  }
  
  /**
   * Unpacks a Fire Emblem message binary and returns its content as a String.
   * 
   * @param file the file to unpack
   * @return the content of the file
   */
  public static String unpackMessageBin(File file) {
    StringBuilder result = new StringBuilder();
    try {
      byte[] bytes = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
      String[] lines = extractMessageArchive(bytes);
      if (lines != null) {
        for (String line : lines) {
          result.append(line);
        }
      } else {
        return null;
      }
    } catch (IOException ex) {
      LOGGER.log(Level.SEVERE, ex.toString(), ex);
    }
    return result.toString();
  }
  
  /**
   * Unpacks a Fire Emblem message binary and returns its content as an array of
   * Strings.
   * 
   * @param file the file to unpack
   * @return the content of the file
   */
  public static String[] unpackMessageBinAsArray(File file) {
    try {
      byte[] bytes = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
      String[] lines = extractMessageArchive(bytes);
      return lines;
    } catch (IOException ex) {
      LOGGER.log(Level.SEVERE, ex.toString(), ex);
    }
    return new String[0];
  }
  
  /**
   * Packs the given content into a Fire Emblem message archive.
   * 
   * @param file the file into which the content should be packed
   * @param lines the content to pack
   * @return whether or not the packing was successful
   */
  public static boolean repackMessageArchive(File file, String[] lines) {
    try {
      byte[] cmp = compressLZ11(makeMessageArchive(lines));
      byte[] cmp2 = new byte[cmp.length + 4];
      cmp2[0] = 0x13;
      System.arraycopy(cmp, 0, cmp2, 4, cmp.length);
      System.arraycopy(cmp, 1, cmp2, 1, 3);
      Files.write(Paths.get(file.getAbsolutePath()), cmp2);
      return true;
    } catch (IOException ex) {
      LOGGER.log(Level.SEVERE, "Failed to repack message archive \"" + file +
                               "\".", ex);
      return false;
    }
  }
  
  /**
   * Packs the given content into a Fire Emblem message binary.
   * 
   * @param file the file into which the content should be packed
   * @param lines the content to pack
   * @return whether or not the packing was successful
   */
  public static boolean repackMessageBin(File file, String[] lines) {
    try {
      Files.write(Paths.get(file.getAbsolutePath()), makeMessageArchive(lines));
      return true;
    } catch (IOException ex) {
      LOGGER.log(Level.SEVERE, "Failed to repack message binary \"" + file +
                               "\".", ex);
      return false;
    }
  }
  
  /**
   * Extracts the passed bytes as a Fire Emblem message archive and returns
   * their content as an array of Strings.
   * 
   * @param archive the bytes to extract
   * @return the content of the passed bytes
   */
  public static String[] extractMessageArchive(byte[] archive) {
    return extractMessageArchive(null, archive, false);
  }
  
  /**
   * Extracts the passed bytes as a Fire Emblem message archive and returns
   * their content as an array of Strings. Saves the content in the given file.
   * 
   * @param outname the file to which output should be written
   * @param archive the bytes to extract
   * @return the content of the passed bytes
   */
  public static String[] extractMessageArchive(String outname, byte[] archive) {
    return extractMessageArchive(outname, archive, true);
  }
  
  /**
   * Extracts the passed bytes as a Fire Emblem message archive and returns
   * their content as an array of Strings. Can save the content in the given
   * file.
   * 
   * @param outname the file to which output should be written
   * @param archive the bytes to extract
   * @param save whether or not to save the output
   * @return the content of the passed bytes
   */
  public static String[] extractMessageArchive(String outname, byte[] archive,
                                               boolean save) {
    if (archive.length < 0x20) {
      return null;
    }
    try {
      ArrayList<Byte> bytes = new ArrayList<>();
      for (int i = 0x20; i < archive.length; i++) {
        if (archive[i] == 0) {
          break;
        } else {
          bytes.add(archive[i]);
        }
      }
      byte[] string = new byte[bytes.size()];
      for (int i = 0; i < bytes.size(); i++) {
        string[i] = bytes.get(i);
      }
      String archiveName = new String(string, "Shift_JIS");
      int textPartitionLen = ByteUtils.getUInt32(archive, 4);
      int stringCount = ByteUtils.getUInt32(archive, 0xC);
      int stringMetaOffset = 0x20 + textPartitionLen;
      int namesOffset = stringMetaOffset + 0x8 * stringCount;
      if (archive.length < (0x20 + (long) textPartitionLen +
                                   (0x8 * ((long) stringCount - 1)) + 4)) {
        return null;
      }
      String[] messageNames = new String[stringCount];
      String[] messages = new String[stringCount];
      for (int i = 0; i < stringCount; i++) {
        int messageOffset = 0x20 + ByteUtils.getUInt32(archive,
                                                    stringMetaOffset + 0x8 * i);
        int messageLen = 0;
        while (ByteUtils.getUInt16(archive, messageOffset + messageLen) != 0) {
          messageLen += 2;
        }
        byte[] message = new byte[messageLen];
        for (int j = 0; j < messageLen; j++) {
          message[j] = archive[messageOffset + j];
        }
        try {
          messages[i] = new String(message, "UTF-16LE").replace("\n","\\n")
                                                       .replace("\r","\\r");
        } catch (UnsupportedEncodingException ex) {
          LOGGER.log(Level.SEVERE, ex.toString(), ex);
        }
        int nameOffset = namesOffset + ByteUtils.getUInt32(archive,
                                              stringMetaOffset + (0x8 * i) + 4);
        bytes.clear();
        for (int j = nameOffset; j < archive.length; j++) {
          if (archive[j] == 0) {
            break;
          } else {
            bytes.add(archive[j]);
          }
        }
        string = new byte[bytes.size()];
        for (int j = 0; j < bytes.size(); j++) {
          string[j] = bytes.get(j);
        }
        messageNames[i] = new String(string, "Shift_JIS");
      }
      ArrayList<String> lines = new ArrayList<>();
      lines.add(archiveName);
      lines.add(System.getProperty("line.separator"));
      lines.add("Message Name: Message");
      lines.add(System.getProperty("line.separator"));
      for (int i = 0; i < stringCount; i++) {
        lines.add(messageNames[i] + ": " + messages[i]);
      }
      if (save) {
        try {
          Files.write(Paths.get(outname), lines);
        } catch (IOException ex) {
          LOGGER.log(Level.SEVERE, ex.toString(), ex);
        }
      }
      return lines.toArray(new String[lines.size()]);
    } catch (UnsupportedEncodingException ex) {
      LOGGER.log(Level.SEVERE, ex.toString(), ex);
    }
    return null;
  }
  
  /**
   * Creates an array of bytes containing a Fire Emblem message archive with the
   * passed content.
   * 
   * @param lines the content of the message archive
   * @return the byte data for the message archive
   */
  private static byte[] makeMessageArchive(String[] lines) {
    try {
      int stringCount = lines.length - 6;
      String[] messages = new String[stringCount];
      String[] names = new String[stringCount];
      int[] mPos = new int[stringCount];
      int[] nPos = new int[stringCount];
      for (int i = 6; i < lines.length; i++) {
        int ind = lines[i].indexOf(": ");
        names[i - 6] = lines[i].substring(0, ind);
        messages[i - 6] = lines[i].substring(ind + 2).replace("\\n", "\n")
                                                     .replace("\\r", "\r");
      }
      byte[] header = new byte[0x20];
      byte[] stringTable;
      byte[] metaTable = new byte[stringCount * 8];
      byte[] namesTable;
      try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
        try (DataOutputStream dos = new DataOutputStream(baos)) {
          byte[] bytes = lines[0].getBytes("Shift_JIS");
          dos.write(bytes, 0, bytes.length);
          dos.write((byte) 0);
          while (baos.size() % 4 != 0) {
            dos.write((byte) 0);
          }
          for (int i = 0; i < stringCount; i++) {
            mPos[i] = baos.size();
            bytes = messages[i].getBytes("UTF-16LE");
            dos.write(bytes, 0, bytes.length);
            dos.write((byte) 0);
            dos.write((byte) 0);
            while (baos.size() % 4 != 0) {
              dos.write((byte) 0);
            }
          }
          stringTable = baos.toByteArray();
        }
      }
      try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
        try (DataOutputStream dos = new DataOutputStream(baos)) {
          for (int i = 0; i < stringCount; i++) {
            nPos[i] = baos.size();
            byte[] bytes = names[i].getBytes("Shift_JIS");
            dos.write(bytes, 0, bytes.length);
            dos.write((byte) 0);
          }
          namesTable = baos.toByteArray();
        }
      }
      for (int i = 0; i < stringCount; i++) {
        System.arraycopy(ByteUtils.getBytes(mPos[i]), 0, metaTable, (i * 8), 4);
        System.arraycopy(ByteUtils.getBytes(nPos[i]), 0, metaTable, (i * 8) + 4,
                                            4);
      }
      byte[] archive = new byte[header.length + stringTable.length +
                                          metaTable.length + namesTable.length];
      System.arraycopy(ByteUtils.getBytes(archive.length), 0, header, 0, 4);
      System.arraycopy(ByteUtils.getBytes(stringTable.length), 0, header, 4, 4);
      System.arraycopy(ByteUtils.getBytes(stringCount), 0, header, 0xC, 4);
      System.arraycopy(header, 0, archive, 0, header.length);
      System.arraycopy(stringTable, 0, archive, header.length,
                       stringTable.length);
      System.arraycopy(metaTable, 0, archive, header.length +
                       stringTable.length, metaTable.length);
      System.arraycopy(namesTable, 0, archive, header.length +
                       stringTable.length + metaTable.length,
                       namesTable.length);
      return archive;
    } catch (IOException ex) {
      LOGGER.log(Level.SEVERE, ex.toString(), ex);
      return null;
    }
  }
  
  /**
   * Gets the length of a specific piece of data for compression in an LZ
   * archive.
   * 
   * @param bytes the bytes in which the data is found
   * @param newPtr a pointer to the new position of the data
   * @param newLength the new length of the data
   * @param oldPtr a pointer to the old position of the data
   * @param oldLength the old length of the data
   * @return the compressed length of the data
   */
  private static int[] getOccurrenceLength(byte[] bytes, int newPtr,
                                     int newLength, int oldPtr, int oldLength) {
    int disp = 0;
    if (newLength == 0) {
      return new int[] { 0, 0 };
    }
    int maxLength = 0;
    for (int i = 0; i < oldLength - 1; i++) {
      int currentOldStart = oldPtr + i;
      int currentLength = 0;
      for (int j = 0; j < newLength; j++) {
        if (bytes[currentOldStart + j] != bytes[newPtr + j]) {
          break;
        }
        currentLength++;
      }
      if (currentLength > maxLength) {
        maxLength = currentLength;
        disp = oldLength - i;
        if (maxLength == newLength) {
          break;
        }
      }
    }
    return new int[] { maxLength, disp };
  }
  
  /**
   * Compresses the passed data with the LZ11 algorithm.
   * 
   * @param decompressed the uncompressed data to compress
   * @return the compressed data
   */
  private static byte[] compressLZ11(byte[] decompressed) {
    if (decompressed.length > 0xFFFFFF) {
      throw new IllegalArgumentException("Input is too long.");
    }
    byte[] compressedBytes = null;
    try (ByteArrayInputStream inputStream =
                                       new ByteArrayInputStream(decompressed)) {
      try (ByteArrayOutputStream outStream = new ByteArrayOutputStream()) {
        int inLength = decompressed.length;
        byte[] inData = new byte[(int) inLength];
        int numReadBytes = inputStream.read(inData, 0, (int) inLength);
        if (numReadBytes != inLength) {
          throw new IOException("Stream is too short.");
        }
        outStream.write((byte) 0x11);
        outStream.write((byte) (inLength & 0xFF));
        outStream.write((byte) (inLength >> 8) & 0xFF);
        outStream.write((byte) (inLength >> 16) & 0xFF);
        byte[] outBuffer = new byte[8 * 4 + 1];
        outBuffer[0] = 0;
        int bufferLength = 1;
        int bufferedBlocks = 0;
        int readBytes = 0;
        int inStart = 0;
        int disp;
        while (readBytes < inLength) {
          if (bufferedBlocks == 8) {
            outStream.write(outBuffer, 0, bufferLength);
            outBuffer[0] = 0;
            bufferLength = 1;
            bufferedBlocks = 0;
          }
          int oldLength = Math.min(readBytes, 0x1000);
          int[] result = getOccurrenceLength(
            inData,
            inStart + readBytes,
            Math.min(inLength - readBytes, 0x10110),
            inStart + readBytes - oldLength,
            oldLength
          );
          int length = result[0];
          disp = result[1];
          if (length < 3) {
            outBuffer[bufferLength++] = inData[inStart + (readBytes++)];
          } else {
            readBytes += length;
            outBuffer[0] |= (byte) (1 << (7 - bufferedBlocks));
            if (length > 0x110) {
              outBuffer[bufferLength] = 0x10;
              outBuffer[bufferLength] |= (byte)(((length - 0x111) >> 12)
                                                & 0x0F);
              bufferLength++;
              outBuffer[bufferLength] = (byte)(((length - 0x111) >> 4) & 0xFF);
              bufferLength++;
              outBuffer[bufferLength] = (byte)(((length - 0x111) << 4) & 0xF0);
            } else if (length > 0x10) {
              outBuffer[bufferLength] = 0x00;
              outBuffer[bufferLength] |= (byte)(((length - 0x111) >> 4) & 0x0F);
              bufferLength++;
              outBuffer[bufferLength] = (byte)(((length - 0x111) << 4) & 0xF0);
            } else {
              outBuffer[bufferLength] = (byte)(((length - 1) << 4) & 0xF0);
            }
            outBuffer[bufferLength] |= (byte) (((disp - 1) >> 8) & 0x0F);
            bufferLength++;
            outBuffer[bufferLength] = (byte) ((disp - 1) & 0xFF);
            bufferLength++;
          }
          bufferedBlocks++;
        }
        if (bufferedBlocks > 0) {
          outStream.write(outBuffer, 0, bufferLength);
        }
        compressedBytes = outStream.toByteArray();
      }
    } catch (IOException ex) {
      LOGGER.log(Level.SEVERE, ex.toString(), ex);
    }
    return compressedBytes;
  }
}
