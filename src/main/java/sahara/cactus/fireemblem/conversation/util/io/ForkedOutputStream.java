/*
 * Copyright (C) 2017 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.util.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Allows output to be directed to both the terminal and a file.
 * 
 * @author Secretive Cactus
 */
public class ForkedOutputStream extends OutputStream {
  
  /**
   * The OutputStream pointing to the original System.out or another terminal.
   */
  OutputStream terminalOut;
  
  /**
   * The OutputStream pointing to the output file.
   */
  OutputStream fileOut;
	
  /**
   * Creates a new ForkedOutputStream pointing to the terminal and a file.
   * 
   * @param terminal an OutputStream pointing to a terminal
   * @param file the file to which output should be logged
   * @throws FileNotFoundException if the file cannot be found or created
   */
	public ForkedOutputStream(OutputStream terminal, File file)
                                                  throws FileNotFoundException {
		terminalOut = terminal;
    fileOut = new FileOutputStream(file);
	}
  
  /**
   * {@inheritDoc}
   */
  @Override
	public void close() throws IOException {
		terminalOut.close();
    fileOut.close();
	}
  
  /**
   * {@inheritDoc}
   */
  @Override
	public void flush() throws IOException {
		terminalOut.flush();
    fileOut.flush();
	}
	
  /**
   * {@inheritDoc}
   */
	@Override
	public void write(byte[] b) throws IOException {
		terminalOut.write(b);
    fileOut.write(b);
	}
  
  /**
   * {@inheritDoc}
   */
	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		terminalOut.write(b, off, len);
    fileOut.write(b, off, len);
	}
  
  /**
   * {@inheritDoc}
   */
  @Override
	public void write(int b) throws IOException {
		terminalOut.write(b);
    fileOut.write(b);
	}
}
