/*
 * Copyright (C) 2017 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.util.control;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.transform.Scale;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;

/**
 * Contains utility methods for working with Canvas controls.
 * 
 * @author Secretive Cactus
 */
public final class CanvasUtils {
  
  /**
   * Provides logging for the class.
   */
  private static final Logger LOGGER =
                                  Logger.getLogger(CanvasUtils.class.getName());
  
  /**
   * A transform used to flip a canvas horizontally.
   */
  private static final Scale MIRROR_TRANSFORM = new Scale(-1, 1);
  
  /**
   * A transform used to restore a canvas to its normal state.
   */
  private static final Scale REGULAR_TRANSFORM = new Scale(1, 1);
  
  /**
   * A set of parameters used to take snapshots with transparent backgrounds.
   */
  private static final SnapshotParameters SNAPSHOT_PARAMETERS;
  static {
    SNAPSHOT_PARAMETERS = new SnapshotParameters();
    SNAPSHOT_PARAMETERS.setFill(Color.TRANSPARENT);
  }
  
  /**
    * Clears the contents of the passed canvas.
    * 
    * @param canvas the canvas to clear
    */
  public static void clearCanvas(Canvas canvas) {
    canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(),
                                            canvas.getHeight());
  }
  
  /**
   * Mirrors the passed canvas horizontally.
   * 
   * @param canvas the canvas to mirror
   */
  public static void mirrorCanvas(Canvas canvas) {
    canvas.getGraphicsContext2D().setTransform(
      MIRROR_TRANSFORM.getMxx(), MIRROR_TRANSFORM.getMyx(),
      MIRROR_TRANSFORM.getMxy(), MIRROR_TRANSFORM.getMyy(),
      MIRROR_TRANSFORM.getTx(),  MIRROR_TRANSFORM.getTy()
    );
  }
  
  /**
   * Restores the passed canvas to its normal, untransformed appearance.
   * 
   * @param canvas the canvas to normalize
   */
  public static void normalizeCanvas(Canvas canvas) {
    canvas.getGraphicsContext2D().setTransform(
      REGULAR_TRANSFORM.getMxx(), REGULAR_TRANSFORM.getMyx(),
      REGULAR_TRANSFORM.getMxy(), REGULAR_TRANSFORM.getMyy(),
      REGULAR_TRANSFORM.getTx(),  REGULAR_TRANSFORM.getTy()
    );
  }
  
  /**
   * Saves a snapshot of the canvas to the chosen PNG file.
   * 
   * @param canvas the canvas that should be saved
   * @param defaultName the default output name, minus the file extension
   */
  public static void saveSnapshot(Canvas canvas, String defaultName) {
    FileChooser fileSelectionDialog = new FileChooser();
    fileSelectionDialog.setInitialFileName(defaultName + ".png");
    File outFile = fileSelectionDialog.showSaveDialog(null);
    WritableImage outImg = new WritableImage((int) canvas.getWidth(),
                                             (int) canvas.getHeight());
    canvas.snapshot(SNAPSHOT_PARAMETERS, outImg);
    if (outFile != null) {
      try {
        ImageIO.write(SwingFXUtils.fromFXImage(outImg, null), "png", outFile);
      } catch (IOException e) {
        LOGGER.log(Level.SEVERE, "Failed to write image to file \"" +
                                 outFile + "\".", e);
      }
    }
  }
}
