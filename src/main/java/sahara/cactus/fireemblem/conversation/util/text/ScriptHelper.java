/*
 * Copyright (C) 2017 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.util.text;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import sahara.cactus.fireemblem.conversation.manager.CharacterManager;
import sahara.cactus.fireemblem.conversation.manager.Config;
import sahara.cactus.fireemblem.conversation.manager.ResourceManager;
import sahara.cactus.fireemblem.conversation.manager.XMLManager;

/**
 * Contains methods and variables related to creating and parsing scripts.
 * 
 * @author Secretive Cactus
 */
public final class ScriptHelper {
  
  /**
   * ScriptHelper should never be instantiated.
   */
  private ScriptHelper() {};
  
  /**
   * Contains a mapping of restricted emotions to the Awakening characters
   * capable of using them.
   * <p>
   * <b>Awakening:</b><p>
   * Arara: Inigo<p>
   * Bashful: Gerome<p>
   * Eyes Shut: Tiki<p>
   * Fainted: Olivia<p>
   * Grimleal: Robin (used for the 'evil' alternate world Robin)<p>
   * Mirror Angry: Chrom (Flips Chrom due to his shoulder thing.)<p>
   * Mirror Pain: Chrom (See note above.)<p>
   * Mirror Posed: Chrom (See note above.)<p>
   * Mirror Sad: Chrom (See note above.)<p>
   * Mirror Smiling: Chrom (See note above.)<p>
   * Mirror Standard: Chrom (See note above.)<p>
   * Risen: All Generic NPC Enemies<p>
   * Sad: Lissa, Chrom<p>
   * Satisfied: Lissa<p>
   * Scheming: Severa<p>
   * Screaming: Maribelle (This is the portrait where her skin is completely red
   *     and she's grimacing while clutching her parasol with her eyes shut.)<p>
   * Shocked: Donnel<p>
   * Shy: Olivia<p>
   * Smiling Camera: Say'ri (See the note below.)<p>
   * Standard Camera: Olivia (This portrait has her looking at the actual
   *     player, not another character on the screen.)<p>
   * Sulky: Severa<p>
   * Surprised: Lissa<p>
   * Troubled: Lissa<p>
   * Worried: Lissa<p>
   * <br>
   * <b>Fates:</b><p>
   * Angry 2: Elise<p>
   * Arara: Inigo, Laslow<p>
   * Big Smile: Kaden<p>
   * Broad Grin: Niles<p>
   * Dejected: Elise<p>
   * Desperate: Sakura<p>
   * Difference: Selkie<p>
   * Extremely Angry: Charlotte<p>
   * Extremely Posed: Jakob<p>
   * Fainted: Elise, Takumi<p>
   * Impatient: Felicia, Chrom<p>
   * Pain 2: Lilith, Peri, Soleil, Shadow Lilith
   * Pain A: Layla<p>
   * Pain B: Layla<p>
   * Pensive: Jakob, Ike<p>
   * Possessed: Takumi, Scarlet, Faceless, All Generic NPC Enemies,
   *    Invisible Dragon<p>
   * Possessed 2: Takumi<p>
   * Scheming: Severa, Luna<p>
   * Singing: Azura, Shigure<p>
   * Smile: Shiro<p>
   * Smiling 2: Beruka, Azura, Inigo, Laslow, Hydra<p>
   * Smiling A: Layla<p>
   * Smiling B: Layla<p>
   * Standard 2: Percy, Kaden, Arthur, Laslow<p>
   * Sulky: Severa, Luna, Keaton<p>
   * Surprised: Elise, Lissa
   */
  public static final HashMap<String,List<String>> UNIQUE_EMOTIONS =
                                                                new HashMap<>();
   
  /**
   * Contains a mapping of English words to Japanese words for each possible
   * emotion that character portraits can display.
   */
  public static final HashMap<String, String> EMOTION_TR_AS_JA =
                                                                new HashMap<>();
  
  /**
   * Contains a mapping of Japanese words to English words for each possible
   * emotion that character portraits can display.
   */
  public static final HashMap<String, String> EMOTION_JA_AS_TR =
                                                                new HashMap<>();
  
  /**
   * Returns whether or not the passed emotion is part of the "standard" set of
   * emotions available to almost every character in the game, save for a
   * handful of enemies.
   * <p>
   * The standard emotions are "Angry", "Pain", "Posed", "Smiling", and
   * "Standard".
   * 
   * @param emotion the emotion whose standard status should be checked
   * @return whether or not the emotion is part of the standard set
   */
  public static boolean isStandardEmotion(String emotion) {
    if (emotion.equals(XMLManager.getText("Emotion_Angry")) ||
        emotion.equals(XMLManager.getText("Emotion_Pain")) ||
        emotion.equals(XMLManager.getText("Emotion_Posed")) ||
        emotion.equals(XMLManager.getText("Emotion_Smiling")) ||
        emotion.equals(XMLManager.getText("Emotion_Standard"))) {
        return true;
    }
    return false;
  }
  
  /**
   * Returns whether or not the passed emotion is valid for the given character
   * at the given index in a command to set the character's emotions.
   * <p>
   * Will always return true for standard emotions present in slot 0, even for
   * the handful of enemies that lack the full standard set. When in doubt, run
   * the simulator, as that will not display the portrait if the emotion is not
   * present.
   * <p>
   * If the emotion is not part of the standard set, it will be checked against
   * the unique emotion list for the current game.
   * <p>
   * Special handling is applied in Awakening for Chrom's Mirror emotions and
   * the My Unit's Grimleal expression.
   * <p>
   * Slots past 0 are only allowed to contain the modifiers "Blushing" and
   * "Sweat".
   * 
   * @param character the character to whom the emotion belongs
   * @param emotion the emotion that needs to be tested
   * @param index the command slot in which the emotion was found
   * @return whether or not the emotion is valid in the passed configuration
   */
  public static boolean isValidEmotion(String character, String emotion,
                                       int index) {
    if (index == 0 && EMOTION_TR_AS_JA.get(emotion) != null) {
      if (isStandardEmotion(emotion)) {
        return true;
      }
      if (Config.getGameMode().isAwakening()) {
        if (emotion.startsWith(XMLManager.getText("Emotion_Mirror"))) {
          return character.equals(CharacterManager.getTranslatedName("クロム"));
        } else if (emotion.equals(XMLManager.getText("Emotion_Grimleal"))) {
          return character.equals(CharacterManager.getMyUnit().getName()) ||
                 character.equals(XMLManager.getText("Default_My_Unit_Name")) ||
            character.equals(CharacterManager.getTranslatedName("username2")) ||
            character.equals(CharacterManager.getTranslatedName("username3")) ||
                 character.startsWith("username");
        }
      }
      List<String> unique = UNIQUE_EMOTIONS.get(emotion);
      if (unique == null) {
        return false;
      } else {
        return unique.contains(character);
      }
    } else {
      return index > 0 &&
             emotion.equals(XMLManager.getText("Emotion_Blushing")) ||
             emotion.equals(XMLManager.getText("Emotion_Sweat"));
    }
  }
  
  /**
   * Converts the passed text from a script to game-readable text.
   * <p>
   * If the text does not have a script header, the text will simply be returned
   * as-is.
   * 
   * @param script the text to convert
   * @return the converted text, or the text as-is if it was not a script
   */
  public static String convertFromScript(String script) {
    return new ScriptToTextConverter(script).run();
  }
  
  /**
   * Converts the passed game text to a formatted script.
   * 
   * @param text the text to convert to script format
   * @return a formatted script
   */
  public static String convertToScript(String text) {
    return new TextToScriptConverter(text).run();
  }
  
  /**
   * Reloads the language keys for emotions.
   */
  public static void reloadEmotions() {
    File[] files = {
      ResourceManager.getFileResource("txt/xml/lang/" + Config.getLanguage() +
                                      "/Script.xml"),
      ResourceManager.getFileResource("txt/xml/lang/" + Config.getLanguage() +
                                      "/Override.xml")
    };
    EMOTION_TR_AS_JA.clear();
    NodeList nodes;
    Node node;
    int len;
    for (File file : files) {
      if (file.exists()) {
        nodes = XMLManager.getNodesOfType(file, "emotion");
        len = nodes.getLength();
        for (int i = 0; i < len; i++) {
          node = nodes.item(i);
          EMOTION_TR_AS_JA.put(node.getTextContent(),
                               node.getAttributes().getNamedItem("japanese")
                               .getTextContent());
        }
      }
    }
    EMOTION_TR_AS_JA.forEach((tr, ja) -> EMOTION_JA_AS_TR.put(ja, tr));
  }
  
  /**
   * Reloads the mappings of characters to their unique emotions for use in
   * scripts. Used when the editor's language is changed.
   */
  public static void reloadUniqueEmotions() {
    UNIQUE_EMOTIONS.clear();
    NodeList uniqueEmotions = XMLManager.getNodesOfType(
                                                ResourceManager.getFileResource(
                                                 "txt/xml/system/Emotions.xml"),
                                                              "unique_emotion");
    Node emotion;
    Node character;
    NodeList characterList;
    ArrayList<String> list;
    int len = uniqueEmotions.getLength();
    int len2;
    for (int i = 0; i < len; i++) {
      emotion = uniqueEmotions.item(i);
      characterList = emotion.getChildNodes();
      len2 = characterList.getLength();
      list = new ArrayList<>();
      for (int j = 0; j < len2; j++) {
        character = characterList.item(j);
        if (character.getNodeName().equals("character")) {
          list.add(CharacterManager.getTranslatedName(
                                                   character.getTextContent()));
        }
      }
      UNIQUE_EMOTIONS.put(XMLManager.getText(
                                    emotion.getAttributes().getNamedItem("name")
                                    .getTextContent()), list);
    }
  }
}
