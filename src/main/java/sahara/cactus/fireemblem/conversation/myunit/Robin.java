/*
 * Copyright (C) 2017 Secretive Cactus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sahara.cactus.fireemblem.conversation.myunit;

import java.util.Set;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import jfxtras.labs.util.ColorUtils;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import sahara.cactus.fireemblem.conversation.manager.CharacterManager;
import sahara.cactus.fireemblem.conversation.manager.ResourceManager;
import sahara.cactus.fireemblem.conversation.util.io.ByteUtils;
import sahara.cactus.fireemblem.conversation.util.control.CanvasUtils;

/**
 * Contains Robin data that can be drawn onto a Canvas.
 * <p>
 * The drawing methods were drawn from SciresM's FEAT.
 * 
 * @author SciresM
 * @author Secretive Cactus
 */
public class Robin extends MyUnit {
  
  /**
   * The list of eye styles available to a Robin.
   */
  private static final String[] EYE_STYLES = { "A", "B", "C", "D", "E" };
  
  /**
   * The list of body styles available to a Robin.
   */
  private static final String[][] BODIES = {
               { "マイユニ_ロリ_顔立ち", "マイユニ_少女_顔立ち", "マイユニ_妙齢_顔立ち" },
               { "マイユニ_童顔_顔立ち", "マイユニ_青年_顔立ち", "マイユニ_ゴツイ_顔立ち" }
                                          };
  
  /**
   * Creates a Robin with default name for the current game mode.
   */
  public Robin() {
    super();
    body = 1;
  }
  
  /**
   * Creates a Robin with the given name.
   * 
   * @param name the Robin's name
   */
  public Robin(String name) {
    super(name);
    body = 1;
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public String getCharacterImageName() {
    return BODIES[gender.ordinal()][body] + EYE_STYLES[eyes];
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public String getDatId(String type) {
    return "FSID_" + type + "_" + getCharacterImageName();
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public String getHairName(String type) {
    return getCharacterImageName() + "_" + type + "_髪" + hairStyle + ".png";
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public void drawCharacterBustImage(Canvas canvas, int drawY) {
    String hairName = getHairName("bu");
    String datId = getDatId("BU");
    String name = getCharacterImageName();
    String[] emotions = emotion.split(",");
    String resName = name + "_bu_" + emotions[0] + ".png";
    CanvasUtils.mirrorCanvas(canvas);
    GraphicsContext g = canvas.getGraphicsContext2D();
    g.setGlobalBlendMode(BlendMode.SRC_OVER);
    if (ResourceManager.resourceExists("img/face/" + resName)) {
      int x;
      int y;
      int sx = ByteUtils.getUInt16(CharacterManager.getFaceData(datId), 0x10);
      int sy = ByteUtils.getUInt16(CharacterManager.getFaceData(datId), 0x12);
      int sw = ByteUtils.getUInt16(CharacterManager.getFaceData(datId), 0x14);
      int sh = ByteUtils.getUInt16(CharacterManager.getFaceData(datId), 0x16);
      Image res = ResourceManager.loadImage("face/" + resName);
      x = -12 - sw;
      if (flip) {
        y = 3;
      } else {
        // REVISIT
        y = 114 + sh;
      }
      g.drawImage(res, sx, sy, sw, sh, x, y, sw, sh);
      if (emotions.length > 1) {
        g.setGlobalBlendMode(BlendMode.MULTIPLY);
        String exResName;
        char mod;
        int emoX;
        int emoY;
        for (int i = 0; i < emotions[1].length(); i++) {
          mod = emotions[1].charAt(i);
          exResName = name + "_bu_" + mod + ".png";
          if (mod == '汗' &&
              ResourceManager.resourceExists("img/face/" + exResName)) {
            emoX = x - sx + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x20);
            emoY = y - sy + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x22);
            g.drawImage(ResourceManager.loadImage("face/" + exResName), emoX,
                        emoY);
          } else if (mod == '照' &&
                     ResourceManager.resourceExists("img/face/" + exResName)) {
            emoX = x - sx + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x18);
            emoY = y - sy + ByteUtils.getUInt16(
                                     CharacterManager.getFaceData(datId), 0x1A);
            g.drawImage(ResourceManager.loadImage("face/" + exResName), emoX,
                        emoY);
          }
        }
        g.setGlobalBlendMode(BlendMode.SRC_OVER);
      }
      if (ResourceManager.resourceExists("img/hair/" + hairName)) {
        res = ResourceManager.loadImage("hair/" + hairName);
        res = ResourceManager.colorImage(res, hairColor.get());
        g.drawImage(res, sx, sy, sw, sh, x, y, sw, sh);
      }
    }
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public void drawCharacterStageImage(Canvas canvas, boolean isActive) {
    String hairName = getHairName("st");
    String datId = getDatId("ST");
    String name = getCharacterImageName();
    String[] emotions = emotion.split(",");
    String resName = name + "_st_" + emotions[0] + ".png";
    CanvasUtils.normalizeCanvas(canvas);
    GraphicsContext g = canvas.getGraphicsContext2D();
    g.setGlobalBlendMode(BlendMode.SRC_OVER);
    if (ResourceManager.resourceExists("img/face/" + resName)) {
      int x = 0;
      int y = 0;
      Image res = ResourceManager.loadImage("face/" + resName);
      if (flip) {
        CanvasUtils.mirrorCanvas(canvas);
        x = (int) (28 - res.getWidth());
        y = (int) (canvas.getHeight() - res.getHeight() + 14);
        g.drawImage(isActive ? res : ResourceManager.fadeImage(res), x, y);
      } else {
        x = (int) (canvas.getWidth() - res.getWidth() + 28);
        y = (int) (canvas.getHeight() - res.getHeight() + 14);
        g.drawImage(isActive ? res : ResourceManager.fadeImage(res), x, y);
      }
      if (emotions.length > 1) {
        g.setGlobalBlendMode(BlendMode.MULTIPLY);
        String exResName;
        char mod;
        int emoX;
        int emoY;
        for (int i = 0; i < emotions[1].length(); i++) {
          mod = emotions[1].charAt(i);
          exResName = name + "_st_" + mod + ".png";
          if (mod == '汗' &&
              ResourceManager.resourceExists("img/face/" + exResName)) {
            emoX = x + ByteUtils.getUInt16(CharacterManager.getFaceData(datId),
                                           0x20);
            emoY = y + ByteUtils.getUInt16(CharacterManager.getFaceData(datId),
                                           0x22);
            g.drawImage(ResourceManager.loadImage("face/" + exResName), emoX,
                        emoY);
          } else if (mod == '照' &&
                     ResourceManager.resourceExists("img/face/" + exResName)) {
            emoX = x + ByteUtils.getUInt16(CharacterManager.getFaceData(datId),
                                           0x18);
            emoY = y + ByteUtils.getUInt16(CharacterManager.getFaceData(datId),
                                           0x1A);
            g.drawImage(ResourceManager.loadImage("face/" + exResName), emoX,
                        emoY);
          }
        }
        g.setGlobalBlendMode(BlendMode.SRC_OVER);
      }
      if (ResourceManager.resourceExists("img/hair/" + hairName)) {
        res = ResourceManager.loadImage("hair/" + hairName);
        res = ResourceManager.colorImage(res, hairColor.get());
        g.drawImage(isActive ? res : ResourceManager.fadeImage(res), x, y);
      }
    }
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public void drawCharacterStageImage(Canvas canvas) {
    String hairName = getHairName("st");
    String datId = getDatId("ST");
    String name = getCharacterImageName();
    String[] emotions = emotion.split(",");
    String resName = name + "_st_" + emotions[0] + ".png";
    CanvasUtils.normalizeCanvas(canvas);
    GraphicsContext g = canvas.getGraphicsContext2D();
    g.setGlobalBlendMode(BlendMode.SRC_OVER);
    if (ResourceManager.resourceExists("img/face/" + resName)) {
      Image res = ResourceManager.loadImage("face/" + resName);
      if (flip) {
        CanvasUtils.mirrorCanvas(canvas);
      }
      g.drawImage(res, 0, 0);
      if (emotions.length > 1) {
        g.setGlobalBlendMode(BlendMode.MULTIPLY);
        String exResName;
        char mod;
        int emoX;
        int emoY;
        for (int i = 0; i < emotions[1].length(); i++) {
          mod = emotions[1].charAt(i);
          exResName = name + "_st_" + mod + ".png";
          if (mod == '汗' &&
              ResourceManager.resourceExists("img/face/" + exResName)) {
            emoX = ByteUtils.getUInt16(CharacterManager.getFaceData(datId),
                                       0x20);
            emoY = ByteUtils.getUInt16(CharacterManager.getFaceData(datId),
                                       0x22);
            g.drawImage(ResourceManager.loadImage("face/" + exResName), emoX,
                        emoY);
          } else if (mod == '照' &&
                     ResourceManager.resourceExists("img/face/" + exResName)) {
            emoX = ByteUtils.getUInt16(CharacterManager.getFaceData(datId),
                                       0x18);
            emoY = ByteUtils.getUInt16(CharacterManager.getFaceData(datId),
                                       0x1A);
            g.drawImage(ResourceManager.loadImage("face/" + exResName), emoX,
                        emoY);
          }
        }
        g.setGlobalBlendMode(BlendMode.SRC_OVER);
      }
      if (ResourceManager.resourceExists("img/hair/" + hairName)) {
        res = ResourceManager.loadImage("hair/" + hairName);
        res = ResourceManager.colorImage(res, hairColor.get());
        g.drawImage(res, 0, 0);
      }
    }
  }
  
  /**
   * Converts the passed My Unit XML nodes to a Robin.
   * 
   * @param nodes the nodes containing the Robin's data
   * @return a Robin with data drawn from the passed nodes
   */
  public static Robin fromXML(NodeList nodes) {
    Robin myUnit = new Robin();
    int len = nodes.getLength();
    Node node;
    String name;
    for (int i = 0; i < len; i++) {
      node = nodes.item(i);
      switch (node.getNodeName()) {
        case "name":
          myUnit.name.set(node.getTextContent());
          break;
        case "voice":
          myUnit.voice = node.getTextContent();
          break;
        case "gender":
          myUnit.gender = Gender.valueOf(node.getTextContent().toLowerCase());
          break;
        case "body":
          myUnit.body = Integer.parseInt(node.getTextContent());
          break;
        case "hair_style":
          myUnit.hairStyle = Integer.parseInt(node.getTextContent());
          break;
        case "hair_color":
          myUnit.hairColor.set(ColorUtils.webColorToColor(
                                                        node.getTextContent()));
          break;
        case "eyes":
          myUnit.eyes = Integer.parseInt(node.getTextContent());
          break;
        case "child_hair_color":
          name = node.getAttributes().getNamedItem("target").getNodeValue();
          myUnit.childHairColors.put(name, node.getTextContent());
          break;
      }
    }
    return myUnit;
  }
  
  /**
   * {@inheritDoc}
   */
  @Override
  public Node toXML(Document document) {
    Element robin = document.createElement("myunit");
    Element attribute;
    
    attribute = document.createElement("name");
    attribute.appendChild(document.createTextNode(name.get()));
    robin.appendChild(attribute);
    
    attribute = document.createElement("gender");
    attribute.appendChild(document.createTextNode(gender.toString()));
    robin.appendChild(attribute);
    
    attribute = document.createElement("body");
    attribute.appendChild(document.createTextNode("" + body));
    robin.appendChild(attribute);
    
    attribute = document.createElement("hair_style");
    attribute.appendChild(document.createTextNode("" + hairStyle));
    robin.appendChild(attribute);
    
    attribute = document.createElement("hair_color");
    attribute.appendChild(document.createTextNode(hairColor.get().toString()
                                               .substring(2, 8).toUpperCase()));
    robin.appendChild(attribute);
    
    attribute = document.createElement("eyes");
    attribute.appendChild(document.createTextNode("" + eyes));
    robin.appendChild(attribute);
    
    attribute = document.createElement("voice");
    attribute.appendChild(document.createTextNode("" + voice));
    robin.appendChild(attribute);
    
    Set<String> keys = childHairColors.keySet();
    Attr target;
    for (String key : keys) {
      attribute = document.createElement("child_hair_color");
      attribute.appendChild(document.createTextNode(childHairColors.get(key)));
      target = document.createAttribute("target");
      target.setValue(key);
      attribute.getAttributes().setNamedItem(target);
      robin.appendChild(attribute);
    }
    return robin;
  }
}
